package org.lucares.collections;

import java.util.function.Function;

/**
 * Represents an operation that maps an int to an int. This is a specialization
 * of {@link Function} for a primitive integer.
 */
public interface UnaryIntOperator {
	/**
	 * Applies the operation to the integer
	 * 
	 * @param value
	 *            the input value
	 * @return the result of the operation
	 */
	int apply(int value);
}
