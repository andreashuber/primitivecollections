package org.lucares.collections;

import java.util.function.Supplier;

/**
 * A sparse 2-dimensional array of primitive longs.
 */
public class Sparse2DLongArray {

	public static final int MAX_SIZE = -1;

	private static final class MinMaxValue implements TripleLongConsumer {

		long minIndex1 = Long.MAX_VALUE;
		long maxIndex1 = Long.MIN_VALUE;

		long minIndex2 = Long.MAX_VALUE;
		long maxIndex2 = Long.MIN_VALUE;

		long minValue = Long.MAX_VALUE;
		long maxValue = Long.MIN_VALUE;

		@Override
		public void apply(long index1, long index2, long value) {

			minIndex1 = Math.min(minIndex1, index1);
			maxIndex1 = Math.max(maxIndex1, index1);

			minIndex2 = Math.min(minIndex2, index2);
			maxIndex2 = Math.max(maxIndex2, index2);

			minValue = Math.min(minValue, value);
			maxValue = Math.max(maxValue, value);
		}

		public long getMaxIndex1() {
			return maxIndex1;
		}

		public long getMaxIndex2() {
			return maxIndex2;
		}

		public long getMaxValue() {
			return maxValue;
		}

		public long getMinValue() {
			return minValue;
		}
	}

	private final LongObjHashMap<LongLongHashMap> matrix;
	private final Supplier<LongLongHashMap> initialValueSupplier;
	private long sizeX;
	private long sizeY;

	/**
	 * Create a new {@link Sparse2DLongArray} with arbitrary size. You can use all
	 * values in the interval 0 (inclusive) to {@link Long#MAX_VALUE} (inclusive) as
	 * index for the x/y axis.
	 */
	public Sparse2DLongArray() {
		this(MAX_SIZE, MAX_SIZE, 8, 0.75);
	}

	/**
	 * Create a new {@link Sparse2DLongArray} with specified size.
	 * 
	 * @param sizeX size of the x-axis, use {@link Sparse2DLongArray#MAX_SIZE} if
	 *              you want to be able to use {@link Long#MAX_VALUE} as index for
	 *              the x-axis
	 * @param sizeY size of the y-axis, use {@link Sparse2DLongArray#MAX_SIZE} if
	 *              you want to be able to use {@link Long#MAX_VALUE} as index for
	 *              the y-axis
	 */
	public Sparse2DLongArray(long sizeX, long sizeY) {
		this(sizeX, sizeY, 8, 0.75);
	}

	/**
	 * Create a new {@link Sparse2DLongArray} with the given initial capacity and
	 * load factor.
	 * 
	 * @param sizeX           size of the x-axis, use
	 *                        {@link Sparse2DLongArray#MAX_SIZE} if you want to be
	 *                        able to use {@link Long#MAX_VALUE} as index for the
	 *                        x-axis
	 * @param sizeY           size of the y-axis, use
	 *                        {@link Sparse2DLongArray#MAX_SIZE} if you want to be
	 *                        able to use {@link Long#MAX_VALUE} as index for the
	 *                        y-axis
	 * @param initialCapacity the initial capacity
	 * @param loadFactor      the load factor
	 */
	public Sparse2DLongArray(long sizeX, long sizeY, int initialCapacity, double loadFactor) {
		if (sizeX <= 0 && sizeX != MAX_SIZE || sizeY <= 0 & sizeY != MAX_SIZE) {
			throw new IllegalArgumentException("size in x and y axis must be positive");
		}

		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.matrix = new LongObjHashMap<LongLongHashMap>(initialCapacity, loadFactor);
		initialValueSupplier = () -> new LongLongHashMap(initialCapacity, loadFactor);
	}

	/**
	 * Add or overwrite a value at position {@code x}&times;{@code y} in the array.
	 * 
	 * @param x     index of the first dimension, must not be negative
	 * @param y     index of the second dimension, must not be negative
	 * @param value the value
	 */
	public void put(long x, long y, long value) {

		if (x < 0 || y < 0) {
			throw new IllegalArgumentException("indexes must be non-negative");
		}
		if (sizeX != MAX_SIZE && x >= sizeX) {
			throw new IndexOutOfBoundsException("x-axis out of range: " + x);
		}
		if (sizeY != MAX_SIZE && y >= sizeY) {
			throw new IndexOutOfBoundsException("y-axis out of range: " + y);
		}
		matrix.compute(x, initialValueSupplier, (key, oldValue) -> {
			oldValue.put(y, value);
			return oldValue;
		});
	}

	/**
	 * Get value from position {@code x}&times;{@code y} of the array
	 * 
	 * @param x index of the first dimension, must not be negative
	 * @param y index of the second dimension, must not be negative
	 * @return the value, or 0 if no value exists
	 */
	public long get(long x, long y) {
		if (x < 0 || y < 0) {
			throw new IllegalArgumentException("indexes must be non-negative");
		}

		LongLongHashMap longLongHashMap = matrix.get(x);
		if (longLongHashMap != null) {
			return longLongHashMap.get(y, 0);
		}
		return 0;
	}

	/**
	 * Calls the {@link TripleLongConsumer#apply(long, long, long)} method for all
	 * entries in this map. The order is not deterministic. Don't rely on the order!
	 * 
	 * @param consumer the consumer
	 */
	public void forEach(TripleLongConsumer consumer) {
		matrix.forEach((x, yDimension) -> {
			yDimension.forEach((y, value) -> consumer.apply(x, y, value));
		});
	}

	@Override
	public String toString() {
		return toString(120);
	}

	/**
	 * Prints (parts) of the matrix as 2-dimensional table with
	 * {@code maxWidthInCharacters}
	 * 
	 * @param maxWidthInCharacters maximum number of characters (horizontally)
	 * @return the matrix
	 */
	public String toString(long maxWidthInCharacters) {

		final MinMaxValue minMaxValue = new MinMaxValue();
		forEach(minMaxValue);

		final long maxIndex1 = minMaxValue.getMaxIndex1();

		final long maxIndex2 = minMaxValue.getMaxIndex2();

		final long lengthOfMaxValue = Math.max(Long.toString(minMaxValue.getMaxValue()).length(),
				Long.toString(minMaxValue.getMinValue()).length());

		final StringBuilder s = new StringBuilder();
		if (maxIndex2 < maxWidthInCharacters / (lengthOfMaxValue + 1)) {
			String format = "%" + (lengthOfMaxValue + 1) + "d";

			for (int i = 0; i <= maxIndex1; i++) {
				for (int j = 0; j <= maxIndex2; j++) {
					s.append(String.format(format, get(i, j)));
				}
				s.append("\n");
			}
			return s.toString();
		} else {
			forEach((index1, index2, value) -> s.append(String.format("(%d, %d) = %d\n", index1, index2, value)));
		}
		return s.toString();
	}
}
