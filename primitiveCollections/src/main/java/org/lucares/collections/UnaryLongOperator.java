package org.lucares.collections;

import java.util.function.Function;

/**
 * Represents an operation that maps a long to a long. This is a specialization
 * of {@link Function} for a primitive long.
 */
public interface UnaryLongOperator {
	/**
	 * Applies the operation to the integer
	 *
	 * @param value
	 *            the input value
	 * @return the result of the operation
	 */
	long apply(long value);
}
