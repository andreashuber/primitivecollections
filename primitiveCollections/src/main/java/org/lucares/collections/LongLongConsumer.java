package org.lucares.collections;

public interface LongLongConsumer {
	public void accept(long key, long value);
}
