package org.lucares.collections;

/**
 * Represents a boolean with three values: true, false and unknown
 */
enum TriValueBool {
	TRUE, FALSE, UNKNOWN
}
