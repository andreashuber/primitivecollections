package org.lucares.collections;

/**
 * Consumer for iterating over {@link Sparse2DLongArray}s.
 */
public interface TripleLongConsumer {
	
	/**
	 * Performs this operation on the given arguments
	 * @param x the index in the x-dimension
	 * @param y the index in the y-dimension
	 * @param value the value at {@code x}&times;{@code y}
	 */
	public void apply(long x, long y, long value);
}
