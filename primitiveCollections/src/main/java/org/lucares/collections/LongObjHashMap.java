package org.lucares.collections;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

/**
 * A hash map where the key is a long and the value is a generic object.
 * 
 * @see LongLongHashMap
 */
public class LongObjHashMap<V> {

	// There is no equivalent to null for primitive values. Therefore we have to add
	// special handling for one long value. Otherwise we couldn't tell if a key is
	// in the map or not. We chose 0L, because LongList is initially all 0L.
	static final long NULL_KEY = 0L;
	
	// Needed when checking for the existence of a key. Without it we would be forced to
	// iterate over all keys. This is caused by the fact that we search for the next free
	// slot when adding new keys. 
	static final long REMOVED_KEY = -1L;

	private static final long EMPTY_SLOT = 0L;

	/**
	 * The maximum size of an array.
	 */
	private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

	private final double fillFactor;

	private long[] keys;
	private V[] values;
	private int size = 0;

	private V zeroValue = null;
	private V removedValue = null;

	/**
	 * Create a new {@link LongLongHashMap} with the given initial capacity and load
	 * factor.
	 *
	 * @param initialCapacity the initial capacity
	 * @param loadFactor      the load factor
	 */
	@SuppressWarnings("unchecked")
	public LongObjHashMap(final int initialCapacity, final double loadFactor) {

		if (initialCapacity < 0) {
			throw new IllegalArgumentException("initial capacity must be non-negative");
		}
		if (initialCapacity > MAX_ARRAY_SIZE) {
			throw new IllegalArgumentException("initial capacity must be smaller or equal to " + MAX_ARRAY_SIZE);
		}
		if (loadFactor <= 0 || Double.isNaN(loadFactor) || loadFactor >= 1.0)
			throw new IllegalArgumentException("Illegal load factor: " + loadFactor);

		this.fillFactor = loadFactor;
		keys = new long[initialCapacity];
		values = (V[]) new Object[initialCapacity];
	}

	/**
	 * Create a new {@link LongLongHashMap} with initial capacity 8 and load factor
	 * 0.75.
	 */
	public LongObjHashMap() {
		this(8, 0.75);
	}

	/**
	 * The number of entries in this map.
	 *
	 * @return the size
	 */
	public int size() {
		return size;
	}

	/**
	 * The capacity of this map.
	 *
	 * @return the capacity
	 */
	int getCapacity() {
		return keys.length;
	}

	/**
	 * Add the given key and value to the map.
	 *
	 * @param key   the key
	 * @param value the value
	 */
	public void put(final long key, final V value) {

		if (key == NULL_KEY) {
			size += zeroValue == null ? 1 : 0;
			zeroValue = value;
			return;
		}
		if (key == REMOVED_KEY) {
			size += removedValue == null ? 1 : 0;
			removedValue = value;
			return;
		}

		if ((keys.length * fillFactor) < size) {
			growAndRehash();
		}

		final boolean added = putInternal(key, value);
		if (added) {
			size++;
		}
	}

	private boolean putInternal(final long key, final V value) {
		final int searchStart = spread(key);
		int currentPosition = searchStart;

		do {
			// found a free place, insert the value
			if (keys[currentPosition] == EMPTY_SLOT) {
				keys[currentPosition] = key;
				values[currentPosition] = value;
				return true;
			}
			// value exists, update it
			if (keys[currentPosition] == key) {
				keys[currentPosition] = key;
				values[currentPosition] = value;
				return false;
			}
			currentPosition = (currentPosition + 1) % keys.length;
		} while (currentPosition != searchStart);

		// Can happen when all slots where occupied at some time in the past.
		// Easy to reproduce by adding and immediately removing all keys from 1 to n.
		// All slots will be marked with REMOVED_KEY.
		// We fix this by calling rehash(), which will effectively replace all REMOVED_KEY 
		// with EMPTY_SLOT.
		rehash();
		return putInternal(key, value);
	}

	/**
	 * Returns the value for the given key if it exists. This method throws a
	 * {@link NoSuchElementException} if the key does not exist. Use
	 * {@link #containsKey(long)} to check before calling {@link #get(long)}.
	 *
	 * @param key the key
	 * @return the value if it exists, or {@code null} if the value does not exist
	 */
	public V get(final long key) {

		if (key == NULL_KEY) {
			if (zeroValue != null) {
				return zeroValue;
			}
			return null;
		}
		
		if (key == REMOVED_KEY) {
			if (removedValue != null) {
				return removedValue;
			}
			return null;
		}

		final int searchStart = spread(key);
		int currentPosition = searchStart;
		do {
			if (keys[currentPosition] == key) {
				return values[currentPosition];
			}
			currentPosition = (currentPosition + 1) % keys.length;
		} while (currentPosition != searchStart);
		return null;
	}

	/**
	 * Check if the map contains the given key.
	 *
	 * @param key the key
	 * @return true iff the map contains the key
	 */
	public boolean containsKey(final long key) {

		if (key == NULL_KEY) {
			return zeroValue != null;
		}
		
		if (key == REMOVED_KEY) {
			return zeroValue != null;
		}

		final int searchStart = spread(key);
		int currentPosition = searchStart;
		do {
			if (keys[currentPosition] == key) {
				return true;
			}
			currentPosition = (currentPosition + 1) % keys.length;
		} while (currentPosition != searchStart);
		
		// Can happen when all slots where occupied at some time in the past.
		// Easy to reproduce by adding and immediately removing all keys from 1 to n.
		// All slots will be marked with REMOVED_KEY.
		// We fix this by calling rehash(), which will effectively replace all REMOVED_KEY 
		// with EMPTY_SLOT.
		rehash();
		
		return false;
	}

	/**
	 * Remove the given key and its value from the map.
	 *
	 * @param key the key
	 */
	public void remove(final long key) {

		if (key == NULL_KEY) {
			size -= zeroValue != null ? 1 : 0;
			zeroValue = null;
			return;
		}
		
		if (key == REMOVED_KEY) {
			size -= removedValue != null ? 1 : 0;
			removedValue = null;
			return;
		}

		final int searchStart = spread(key);
		int currentPosition = searchStart;
		do {
			if (keys[currentPosition] == key) {
				keys[currentPosition] = REMOVED_KEY;
				size--;
				return;
			}
			
			if (keys[currentPosition] == EMPTY_SLOT) {
				// key does not exist
				return;
			}
			
			currentPosition = (currentPosition + 1) % keys.length;
		} while (currentPosition != searchStart);

		// Can happen when all slots where occupied at some time in the past.
		// Easy to reproduce by adding and immediately removing all keys from 1 to n.
		// All slots will be marked with REMOVED_KEY.
		// We fix this by calling rehash(), which will effectively replace all REMOVED_KEY 
		// with EMPTY_SLOT.
		rehash();
	}

	/**
	 * Computes a mapping for the given key and its current value.
	 * <p>
	 * The mapping for given key is updated by calling {@code function} with the old
	 * value. The return value will be set as new value. If the map does not contain
	 * a mapping for the key, then {@code function} is called with 
	 * {@code initialValueIfAbsent}.
	 *
	 * @param key                  the key
	 * @param initialValueIfAbsent a {@link Supplier} returning the value used if there is no current mapping for the
	 *                             key
	 * @param function             called to update an existing value
	 */
	public void compute(final long key, final Supplier<V> initialValueIfAbsent, final BiLongObjectFunction<V> function) {
		if (key == NULL_KEY) {
			if (zeroValue != null) {
				zeroValue = function.apply(NULL_KEY, zeroValue);
				return;
			}
			zeroValue = function.apply(NULL_KEY, initialValueIfAbsent.get());
			return;
		}
		
		if (key == REMOVED_KEY) {
			if (removedValue != null) {
				removedValue = function.apply(REMOVED_KEY, removedValue);
				return;
			}
			removedValue = function.apply(REMOVED_KEY, initialValueIfAbsent.get());
			return;
		}

		final int searchStart = spread(key);
		int currentPosition = searchStart;
		do {
			if (keys[currentPosition] == key) {
				final V updatedValue = function.apply(key, values[currentPosition]);
				values[currentPosition] = updatedValue;
				return;
			}
			if (keys[currentPosition] == EMPTY_SLOT) {
				// key does not exist
				break;
			}
			currentPosition = (currentPosition + 1) % keys.length;
		} while (currentPosition != searchStart);

		// key not found -> add it
		final V newZeroValue = function.apply(key, initialValueIfAbsent.get());
		put(key, newZeroValue);
	}

	/**
	 * Calls the {@link LongObjConsumer#accept(long, Object)} method for all entries
	 * in this map. The order is based on the hash value and is therefore not
	 * deterministic. Don't rely on the order!
	 *
	 * @param consumer the consumer
	 */
	public void forEach(final LongObjConsumer<V> consumer) {

		if (zeroValue != null) {
			consumer.accept(NULL_KEY, zeroValue);
		}
		
		if (removedValue != null) {
			consumer.accept(REMOVED_KEY, removedValue);
		}

		for (int i = 0; i < keys.length; i++) {
			if (keys[i] != EMPTY_SLOT && keys[i] != REMOVED_KEY) {
				consumer.accept(keys[i], values[i]);
			}
		}
	}

	/**
	 * Calls the {@link LongObjConsumer#accept(long, Object)} method for all entries
	 * in this map. This method iterates over the keys in ascending order.
	 * <p>
	 * Note: this method is slower than {@link #forEach(LongLongConsumer)}.
	 *
	 * @param consumer the consumer
	 */
	public void forEachOrdered(final LongObjConsumer<V> consumer) {
		final long[] sortedKeys = Arrays.copyOf(keys, keys.length);
		Arrays.parallelSort(sortedKeys);

		// handle negative keys
		for (int i = 0; i < sortedKeys.length; i++) {
			final long key = sortedKeys[i];
			if (key < REMOVED_KEY) {
				consumer.accept(key, get(key));
			} else {
				break;
			}
		}
		
		// handle the special keys
		if (removedValue != null) {
			consumer.accept(REMOVED_KEY, removedValue);
		}
		if (zeroValue != null) {
			consumer.accept(NULL_KEY, zeroValue);
		}
		
		// handle positive keys
		final int posFirstKey = findPosOfFirstPositiveKey(sortedKeys);
		if (posFirstKey < 0) {
			return;
		}
		for (int i = posFirstKey; i < sortedKeys.length; i++) {
			final long key = sortedKeys[i];
			consumer.accept(key, get(key));
		}
	}

	static int findPosOfFirstPositiveKey(final long[] sortedKeys) {

		if (sortedKeys.length == 0) {
			return -1;
		}
		if (sortedKeys.length == 1) {
			return sortedKeys[0] > EMPTY_SLOT ? 0 : -1;
		}

		int low = 0;
		int high = sortedKeys.length - 1;
		int pos = -1;

		while (low <= high) {
			pos = (low + high) / 2;
			if (sortedKeys[pos] <= EMPTY_SLOT) {
				low = pos + 1;
			} else {
				high = pos - 1;
			}
		}

		if (low < sortedKeys.length && sortedKeys[low] <= EMPTY_SLOT) {
			low++;
		}

		return low < sortedKeys.length && sortedKeys[low] > EMPTY_SLOT ? low : -1;
	}
	
	/**
	 * Rehashes all elements of this map.
	 * <p>
	 * This is a maintenance operation that should be executed periodically after removing elements.
	 */
	public void rehash() {
		rehash(keys.length);
	} 

	private void growAndRehash() {
		final int newSize = Math.min(keys.length * 2, MAX_ARRAY_SIZE);
		rehash(newSize);
	}
	
	@SuppressWarnings("unchecked")
	private void rehash(int newSize) {
		final long[] oldKeys = keys;
		final V[] oldValues = values;

		keys = new long[newSize];
		values = (V[]) new Object[newSize];

		for (int i = 0; i < oldKeys.length; i++) {
			final long key = oldKeys[i];
			if (key != EMPTY_SLOT && key != REMOVED_KEY) {
				final V value = oldValues[i];
				putInternal(key, value);
			}
		}
	}

	// visible for test
	int spread(final long key) {
		return hash(key) % keys.length;
	}

	private int hash(final long l) {
		return Math.abs(Long.hashCode(l));
	}

}
