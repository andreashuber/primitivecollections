package org.lucares.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

class MultiwayLongMerger {
	private static final long UNSET = Long.MAX_VALUE;

	static class LongQueue {
		private static final LongQueue EMPTY = new LongQueue(LongList.of());

		final long[] wrapped;

		int offset = 0;

		private int size;

		public LongQueue(LongList wrapped) {
			this.wrapped = wrapped.getArrayInternal();
			this.size = wrapped.size();
		}

		boolean isEmpty() {
			return offset >= size;
		}

		long pop() {
			assert offset < size;
			final long result = wrapped[offset];
			offset++;
			return result;
		}

		public long peek() {
			return wrapped[offset];
		}

		public long peekLast() {
			return wrapped[size - 1];
		}

		@Override
		public String toString() {
			return Arrays.toString(Arrays.copyOfRange(wrapped, offset, size));
		}

		public int size() {
			return size - offset;
		}
	}

	static LongList unionSorted(Collection<LongList> longLists) {

		assertAllListsAreSorted(longLists);

		final List<LongQueue> queues = new ArrayList<LongQueue>();
		boolean hasValueUNSET = initQueues(longLists, queues);

		final LongList result = new LongList();

		if (!queues.isEmpty()) {
			mergeQueues(queues, result);
		}
		if (hasValueUNSET) {
			result.add(UNSET);
		}

		return result;
	}

	private static void mergeQueues(final List<LongQueue> queues, final LongList result) {
		final MinValuePriorityQueue selectionTree = new MinValuePriorityQueue(queues);

		long previousValue = UNSET;
		long val;
		while ((val = selectionTree.pop()) != UNSET) {
			if (val != previousValue) {
				result.add(val);
				previousValue = val;
			}
		}
	}

	private static boolean initQueues(Collection<LongList> longLists, final List<LongQueue> queues) {
		boolean hasValueUNSET = false;
		for (final LongList longList : longLists) {
			if (!longList.isEmpty()) {
				final LongQueue queue = new LongQueue(longList);
				if (!queue.isEmpty() && queue.peekLast() == UNSET) {
					hasValueUNSET = true;
				}

				queues.add(queue);
			}
		}
		return hasValueUNSET;
	}

	private static void assertAllListsAreSorted(Collection<LongList> longLists) {
		for (LongList longList : longLists) {
			if (!longList.isSorted()) {
				throw new IllegalArgumentException("lists must be sorted");
			}
		}
	}

	private static int nextPowOfTwo(int i) {
		return Integer.highestOneBit(i - 1) << 1;
	}

	private static class MinValuePriorityQueue {

		private static final LongQueue[] EMPTY_QUEUE = new LongQueue[0];
		
		private LongQueue[] mLongQueues;

		/*
		 * a classic heap where the nodes are layed out in breath first order. First the
		 * root, then the nodes of level 1, then the nodes of level 2, ...
		 */
		private final long[] mHeap;

		private final int size;

		private final int firstLeafIndex;

		public MinValuePriorityQueue(final Collection<LongQueue> longQueues) {
			final List<LongQueue> tmpQueues = new ArrayList<>(longQueues);
			size = longQueues.size();
			mHeap = new long[2 * nextPowOfTwo(size) - 1];

			firstLeafIndex = mHeap.length / 2;

			Arrays.fill(mHeap, UNSET);
			
			// fill the longQueues list with empty queues, so that we
			// have a queue for every leaf in the heap. This makes fillWithMinOfChildren()
			// easier, because it removes special cases.
			for (int i = size; i < nextPowOfTwo(size); i++) {
				tmpQueues.add(LongQueue.EMPTY);
			}
			this.mLongQueues = tmpQueues.toArray(EMPTY_QUEUE);

			init();
		}

		/**
		 * Returns the smallest value of the heap. Returns
		 * {@link MultiwayLongMerger#UNSET}={@value MultiwayLongMerger#UNSET} if the
		 * heap is empty.
		 * 
		 * @return the smallest value or
		 *         {@link MultiwayLongMerger#UNSET}={@value MultiwayLongMerger#UNSET} if
		 *         heap is empty
		 */
		public long pop() {
			long result = mHeap[0];
			if (result != UNSET) {
				fillWithMinOfChildren(0);
			}
			return result;
		}

		/**
		 * <pre>
		 *               7
		 *           3  
		 *               8
		 *       1   
		 *               9
		 *           4
		 *               10
		 *   0
		 *               11
		 *           5
		 *               12
		 *       2
		 *               13
		 *           6 
		 *               14
		 * </pre>
		 */
		private void init() {
			// fill leaf nodes
			int offset = firstLeafIndex;
			for (int j = 0; j < size; j++) {
				final LongQueue q = mLongQueues[j];
				mHeap[offset + j] = q.isEmpty() ? UNSET : q.pop();
			}

			// fill the non-leaf layers (from the leafs up to the root)
			while (offset > 0) {
				offset /= 2; //
				for (int i = offset; i <= offset * 2; i++) {
					fillWithMinOfChildren(i);
				}
			}
		}

		private int leftChildIndex(int i) {
			return i * 2 + 1;
		}

		private int rightChildIndex(int i) {
			return i * 2 + 2;
		}

		private boolean isLeaf(int i) {
			return i >= firstLeafIndex;
		}

		private int leafIndexToListIndex(int i) {
			assert isLeaf(i) : "index " + i + " is not a leaf";
			return i - firstLeafIndex;
		}

		private void fillWithMinOfChildren(int index) {

			final int firstLeafOffset = firstLeafIndex;
			final long[] heap = mHeap;
			final LongQueue[] longQueues = mLongQueues;
			
			int currentIndex = index;
			while (true) {
				final int leftChildIndex = currentIndex * 2 + 1; // leftChildIndex(index);
				final int rightChildIndex = leftChildIndex + 1;// rightChildIndex(index);

				final long valueOfLeftChild = heap[leftChildIndex];
				final long valueOfRightChild = heap[rightChildIndex];

				if (valueOfLeftChild < valueOfRightChild) {
					// left < right
					heap[currentIndex] = valueOfLeftChild;
					currentIndex = leftChildIndex;
				} else {
					// left >= right
					heap[currentIndex] = valueOfRightChild;
					currentIndex = rightChildIndex;
				}

				if (currentIndex >= firstLeafOffset) {
					final int listIndex = currentIndex - firstLeafOffset; // leafIndexToListIndex(index);
					final LongQueue queue = longQueues[listIndex];
					heap[currentIndex] = queue.isEmpty() ? UNSET : queue.pop();
					return;
				}
			}
		}

	}
}
