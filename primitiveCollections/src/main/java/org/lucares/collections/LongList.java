package org.lucares.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Spliterator.OfLong;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.LongStream;
import java.util.stream.StreamSupport;

/**
 * A list for primitive longs.
 * <p>
 * This class does not (yet) implements all methods a java.util {@link List}
 * would have.
 */
public final class LongList implements Serializable, Cloneable {

	private static final long serialVersionUID = 2622570032686034909L;

	private static final int DEFAULT_CAPACITY = 10;

	/**
	 * The maximum size of an array.
	 */
	private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

	private static final long[] EMPTY_ARRAY = {};

	/**
	 * If the average length of the lists is longer than this value, then we'll
	 * first try to concatenate non-overlapping lists before the union is computed.
	 */
	public static int FLAGS_UNION_CONCATENATE_NON_OVERLAPPING_AVG_MIN = 500;

	/**
	 * The array containing the values. It is transient, so that we can implement
	 * our own serialization.
	 */
	transient private long[] data;

	private int size = 0;

	/**
	 * Keeps track of whether or not the list is sorted. This allows us to use
	 * binary search for {@link #indexOf(int)} and efficient algorithms for
	 * {@link #intersection(LongList, LongList)} /
	 * {@link #unionInternal(LongList, LongList)} / {@link #uniq()} /
	 * {@link #removeAll(int, int)}. An empty list is sorted.
	 */
	private boolean sorted = true;

	/**
	 * Create a new {@link LongList} with initial capacity 10.
	 */
	public LongList() {
		this(DEFAULT_CAPACITY);
	}

	/**
	 * Create a new {@link LongList}.
	 *
	 * @param initialCapacity initial capacity
	 * @throws IllegalArgumentException if initial capacity is negative
	 */
	public LongList(final int initialCapacity) {
		if (initialCapacity < 0 || initialCapacity > MAX_ARRAY_SIZE) {
			throw new IllegalArgumentException(
					"initial capacity must not be negative and not larger than " + MAX_ARRAY_SIZE);
		}

		data = initialCapacity > 0 ? new long[initialCapacity] : EMPTY_ARRAY;
	}

	/**
	 * Create a new {@link LongList} with a copy of the elements of
	 * {@code longList}.
	 *
	 * @param longList the list to copy
	 * @throws NullPointerException if the specified {@link LongList} is null
	 */
	public LongList(final LongList longList) {
		data = EMPTY_ARRAY;
		addAll(longList);
	}

	/**
	 * Create a new {@link LongList} with a copy of the given elements.
	 *
	 * @param values the values
	 * @return the list
	 * @throws NullPointerException if the specified array is null
	 */
	public static LongList of(final long... values) {
		final LongList result = new LongList(values.length);
		result.addAll(values);
		return result;
	}

	/**
	 * Returns an {@link LongList} with values from {@code startInclusive} to
	 * {@code endExclusive}-1.
	 *
	 * @param startInclusive the lower bound (inclusive)
	 * @param endExclusive   the upper bound (exclusive)
	 * @return the {@link IntList}
	 */
	public static LongList range(final long startInclusive, final long endExclusive) {
		if (startInclusive >= endExclusive) {
			return new LongList(0);
		} else if (endExclusive - startInclusive > MAX_ARRAY_SIZE) {
			throw new IllegalArgumentException("Range of more than " + MAX_ARRAY_SIZE + " is not supported.");
		} else {
			final LongList result = new LongList((int) (endExclusive - startInclusive));
			for (long i = startInclusive; i < endExclusive; i++) {
				result.add(i);
			}
			return result;
		}
	}

	/**
	 * Returns an {@link LongList} with values from {@code startInclusive} to
	 * {@code endInclusive}.
	 *
	 * @param startInclusive the lower bound (inclusive)
	 * @param endInclusive   the upper bound (inclusive)
	 * @return the {@link LongList}
	 */
	public static LongList rangeClosed(final long startInclusive, final long endInclusive) {
		if (startInclusive > endInclusive) {
			return new LongList(0);
		} else if (endInclusive - startInclusive + 1 > MAX_ARRAY_SIZE) {
			throw new IllegalArgumentException("Range of more than " + MAX_ARRAY_SIZE + " is not supported.");
		} else {
			final LongList result = new LongList((int) (endInclusive - startInclusive + 1));
			for (long i = startInclusive; i <= endInclusive; i++) {
				result.add(i);
			}
			return result;
		}
	}

	/**
	 * Returns a new list with the values from the given start index to the end of
	 * the list.
	 * 
	 * @param startInclusive start index
	 * @return {@code LongList}
	 */
	public LongList sublist(final int startInclusive) {
		return sublist(startInclusive, size);
	}

	/**
	 * Returns a new list with the values of the given range.
	 * 
	 * @param startInclusive the start index
	 * @param endExclusive   the end index (exclusive)
	 * @return {@link LongList}
	 */
	public LongList sublist(final int startInclusive, int endExclusive) {
		final LongList result = new LongList(endExclusive - startInclusive);
		result.data = Arrays.copyOfRange(data, startInclusive, endExclusive);
		result.size = result.data.length;
		result.sorted = sorted;
		return result;
	}

	/**
	 * Returns {@code true} if this list contains no elements.
	 *
	 * @return {@code true} if this list contains no elements
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns the number of elements in this list.
	 *
	 * @return the number of elements in this list
	 */
	public int size() {
		return size;
	}

	/**
	 * Returns the number of elements this list can hold before a resize is
	 * necessary, this is the size of the internal backing array.
	 *
	 * @return the number of elements this list can hold before a resize is
	 *         necessary
	 */
	public int getCapacity() {
		return data.length;
	}

	/**
	 * Returns whether or not the list is sorted.
	 * <p>
	 * An empty list is sorted.
	 *
	 * @return true iff the list is sorted.
	 */
	public boolean isSorted() {
		return sorted;
	}

	/**
	 * Adds {@code value} to the list.
	 *
	 * @param value the value to add
	 */
	public void add(final long value) {
		ensureCapacity(1);

		data[size] = value;

		if (sorted) {
			sorted = size == 0 ? sorted : data[size - 1] <= data[size];
		}
		size++;
	}

	/**
	 * Unsafe version of {@link #add(long)} that does not call
	 * {@link #ensureCapacity(long)}. The caller has to make sure that the list has
	 * capacity.
	 *
	 * @param value the value to add
	 */
	private void addUnsafe(final long value) {
		data[size] = value;

		if (sorted) {
			sorted = size == 0 ? sorted : data[size - 1] <= data[size];
		}
		size++;
	}

	/**
	 * Inserts {@code values} at position {@code pos} into the list.
	 *
	 * @param destPos the position to insert the elements
	 * @param values  the elements to insert
	 * @throws IndexOutOfBoundsException if destPos is out of bounds
	 * @throws NullPointerException      if the given array is null
	 */
	public void insert(final int destPos, final LongList values) {
		final TriValueBool sourceIsSorted = values.isSorted() ? TriValueBool.TRUE : TriValueBool.FALSE;
		insert(destPos, values.data, 0, values.size(), sourceIsSorted);
	}

	/**
	 * Inserts {@code values} at position {@code pos} into the list.
	 *
	 * @param destPos   the position to insert the elements
	 * @param source    the elements to insert
	 * @param sourcePos starting position in the source array
	 * @param length    number of elements to insert
	 * @throws IndexOutOfBoundsException if destPos is out of bounds, if sourcePos
	 *                                   is out of bounds or if sourcePos + length
	 *                                   is out of bounds
	 * @throws NullPointerException      if the given array is null
	 */
	public void insert(final int destPos, final LongList source, final int sourcePos, final int length) {

		// note: the sublist might be sorted even if the complete list is not
		final TriValueBool sourceIsSorted = source.isSorted() ? TriValueBool.TRUE : TriValueBool.UNKNOWN;
		insert(destPos, source.data, sourcePos, length, sourceIsSorted);
	}

	/**
	 * Inserts {@code values} at position {@code pos} into the list.
	 *
	 * @param destPos the position to insert the elements
	 * @param source  the elements to insert
	 * @throws IndexOutOfBoundsException if destPos is out of bounds
	 * @throws NullPointerException      if the given array is null
	 */
	public void insert(final int destPos, final long... source) {
		insert(destPos, source, 0, source.length, TriValueBool.UNKNOWN);
	}

	/**
	 * Inserts {@code values} at position {@code pos} into the list.
	 *
	 * @param destPos   the position to insert the elements
	 * @param source    the elements to insert
	 * @param sourcePos starting position in the source array
	 * @param length    number of elements to insert
	 * @throws IndexOutOfBoundsException if destPos is out of bounds, if sourcePos
	 *                                   is out of bounds or if sourcePos + length
	 *                                   is out of bounds
	 * @throws NullPointerException      if the given array is null
	 */
	public void insert(final int destPos, final long[] source, final int sourcePos, final int length) {
		insert(destPos, source, sourcePos, length, TriValueBool.UNKNOWN);
	}

	private void insert(final int destPos, final long[] source, final int sourcePos, final int length,
			final TriValueBool sourceIsSorted) {

		if (length == 0) {
			return;
		}
		if (destPos < 0) {
			throw new IndexOutOfBoundsException("destPos must not be negative, but was: " + destPos);
		}
		if (destPos > size) {
			throw new IndexOutOfBoundsException("destPos must not be larger than size(), but was: " + sourcePos);
		}
		if (sourcePos >= source.length) {
			throw new IndexOutOfBoundsException("sourcePos must be smaller than source.length, but was: " + destPos);
		}
		if (sourcePos + length > source.length) {
			throw new IndexOutOfBoundsException(
					"sourcePos + length must not be larger than source.length, but was: " + (sourcePos + length));
		}

		ensureCapacity(length);

		// move everything after the insert position to make room for the new values
		System.arraycopy(data, destPos, data, destPos + length, size - destPos);
		// insert the new values
		System.arraycopy(source, sourcePos, data, destPos, length);
		size += length;

		if (sorted) {
			// compare with element before the insertion
			sorted = destPos == 0 ? sorted : data[destPos - 1] <= data[destPos];

			// check if the inserted values are sorted
			switch (sourceIsSorted) {
			case TRUE:
				// nothing to do, keep the current value of 'sorted'
				break;
			case FALSE:
				sorted = false;
				break;
			case UNKNOWN:
				for (int i = 1; i < length && sorted; i++) {
					sorted = data[destPos + i - 1] <= data[destPos + i];
				}
				break;
			default:
				throw new IllegalStateException("unhandled enum value: " + sourceIsSorted);
			}
			if (sorted) {
				// compare last inserted element with next element in the list
				sorted = destPos + length < size()//
						? data[destPos + length - 1] <= data[destPos + length]//
						: sorted;
			}
		}
	}

	/**
	 * Set the value {@code value} at position {@code pos}.
	 *
	 * @param pos   the position to overwrite
	 * @param value the new value
	 * @throws IndexOutOfBoundsException if pos is out of bounds
	 *                                   {@code pos < 0 || pos >= size()}
	 */
	public void set(final int pos, final long value) {

		if (pos < 0) {
			throw new IndexOutOfBoundsException("pos must not be negative, but was: " + pos);
		}

		if (pos >= size) {
			throw new IndexOutOfBoundsException("pos must not smaller than size(), but was: " + pos);
		}

		data[pos] = value;

		if (sorted) {
			sorted = pos <= 0 ? sorted : data[pos - 1] <= data[pos];
			sorted = pos + 1 >= size() ? sorted : data[pos] <= data[pos + 1];
		}
	}

	/**
	 * Add {@code values} to the list.
	 *
	 * @param values the values to add
	 * @throws NullPointerException if the given array is null
	 */
	public void addAll(final long... values) {
		ensureCapacity(values.length);

		System.arraycopy(values, 0, data, size, values.length);

		if (sorted) {
			for (int i = 0; i < values.length && sorted; i++) {
				sorted = size + i - 1 < 0 ? sorted : data[size + i - 1] <= data[size + i];
			}
		}

		size += values.length;
	}

	/**
	 * Add all value of the given list.
	 *
	 * @param list the list
	 * @throws NullPointerException if the given list is null
	 */
	public void addAll(final LongList list) {
		ensureCapacity(list.size());

		System.arraycopy(list.data, 0, data, size, list.size());

		if (sorted) {
			sorted = list.isSorted();
			if (list.isSorted() // new list is sorted
					&& !isEmpty() // if old list is empty, then the new list's sorted value is equal to that of
									// the new list
					&& !list.isEmpty()) // if new list is empty, then old list stays sorted
			{
				// check that the first new value is bigger than the highest old value
				final long highestOldValue = data[size - 1]; // size has still the old value
				final long lowestNewValue = list.get(0);
				sorted = highestOldValue <= lowestNewValue;
			}
		}

		size += list.size();
	}

	/**
	 * Add all values of the given lists.
	 *
	 * @param longLists collection of the lists
	 * @throws NullPointerException if the collection of {@link LongList} is null or
	 *                              contains null
	 */
	public void addAll(final Collection<LongList> longLists) {
		final int sizeNewElements = longLists.stream().mapToInt(LongList::size).sum();
		ensureCapacity(sizeNewElements);
		for (final LongList longList : longLists) {
			addAll(longList);
		}
	}

	/**
	 * Removes elements from the list.
	 * <p>
	 * This method does not release any memory. Call {@link #trim()} to free unused
	 * memory.
	 *
	 * @param fromIndex index of the first element to remove
	 * @param toIndex   the index of the last element to remove (exclusive)
	 * @throws IndexOutOfBoundsException if {@code fromIndex} or {@code toIndex} is
	 *                                   negative, or if the range defined by
	 *                                   {@code fromIndex} and {@code toIndex} is
	 *                                   out of bounds
	 * @see #trim()
	 */
	public void remove(final int fromIndex, final int toIndex) {

		if (fromIndex < 0) {
			throw new IndexOutOfBoundsException("from must not be negative, but was: " + fromIndex);
		}
		if (toIndex < fromIndex) {
			throw new IndexOutOfBoundsException("toIndex must not be smaller than fromIndex, but was: " + toIndex);
		}
		if (toIndex > size) {
			throw new IndexOutOfBoundsException(
					"toIndex must not be larger than the size of this list, but was: " + toIndex);
		}

		final int numRemoved = size - toIndex;
		System.arraycopy(data, toIndex, data, fromIndex, numRemoved);

		size = size - (toIndex - fromIndex);

		if (!sorted) {
			checkIfSorted();
		}
	}

	/**
	 * Remove all elements that contained in the specified list.
	 * <p>
	 * This method does not release any memory. Call {@link #trim()} to free unused
	 * memory.
	 * <p>
	 * If both lists are sorted, then the algorithms has time complexity of O(n+m),
	 * where n is the length of {@code this} and m the length of {@code remove}. If
	 * only {@code remove} is sorted, then the algorithm has a complexity of
	 * O(n*log(m)). If {@code remove} is not sorted, then the complexity is O(n*m).
	 * The space complexity is constant in all cases.
	 *
	 * @param remove the elements to remove
	 * @throws NullPointerException if the specified {@link LongList} is null
	 * @see #trim()
	 */
	public void removeAll(final LongList remove) {

		if (isSorted() && remove.isSorted()) {
			removeSorted(remove);
		} else {
			removeIf((val, index) -> remove.indexOf(val) >= 0);
		}
	}

	private void removeSorted(final LongList remove) {
		int posInRemoveList = 0;
		int insertPosition = 0;
		for (int i = 0; i < size; i++) {
			final long current = data[i];

			while (posInRemoveList < remove.size() && remove.get(posInRemoveList) < current) {
				posInRemoveList++;
			}

			if (posInRemoveList >= remove.size() || remove.get(posInRemoveList) != current) {
				// keep current element
				data[insertPosition] = current;
				insertPosition++;
			}
		}
		size = insertPosition;
	}

	/**
	 * Remove all elements that match the given predicate.
	 * <p>
	 * This method does not release any memory. Call {@link #trim()} to free unused
	 * memory.
	 *
	 *
	 * @param predicate the predicate
	 * @throws NullPointerException if the specified predicate is null
	 * @see #trim()
	 */
	public void removeIf(final LongPredicate predicate) {

		int insertPosition = 0;
		for (int i = 0; i < size; i++) {
			final long current = data[i];
			if (!predicate.test(current, i)) {
				// keep current element
				data[insertPosition] = current;
				insertPosition++;
			}
		}
		size = insertPosition;

		if (!sorted) {
			checkIfSorted();
		}
	}

	/**
	 * Retains all elements contained in the specified list.
	 * <p>
	 * This method does not release any memory. Call {@link #trim()} to free unused
	 * memory.
	 * <p>
	 * For a method that computes the intersection of two lists and also removes
	 * duplicate values, see {@link #intersection(LongList, LongList)}.
	 * <p>
	 * If {@code retain} is sorted, then the algorithm has a complexity of
	 * O(n*log(m)), where n is the length of {@code this} and m the length of
	 * {@code retain}. If {@code retain} is not sorted, then the complexity is
	 * O(n*m).
	 *
	 * @param retain the elements to retain
	 * @throws NullPointerException if the specified {@link LongList} is null
	 * @see #trim()
	 * @see #intersection(LongList, LongList)
	 */
	public void retainAll(final LongList retain) {
		removeIf((val, index) -> retain.indexOf(val) < 0);
	}

	/**
	 * Replaces all values in the list by applying {@code operator}.
	 *
	 * @param operator the operator
	 * @throws NullPointerException if the specified {@link UnaryLongOperator} is
	 *                              null
	 */
	public void replaceAll(final UnaryLongOperator operator) {

		for (int i = 0; i < size; i++) {
			final long newValue = operator.apply(data[i]);
			set(i, newValue);
		}
	}

	/**
	 * Returns the element at position {@code pos}.
	 *
	 * @param pos position of the element to return
	 * @return the element at position {@code pos}
	 * @throws IndexOutOfBoundsException if {@code pos} is out of bounds
	 *                                   {@code index < 0 || index >= size()}
	 */
	public long get(final int pos) {
		if (pos < 0 || pos >= size) {
			throw new IndexOutOfBoundsException();
		}
		return data[pos];
	}

	public long first() {
		return get(0);
	}

	public long last() {
		return get(size() - 1);
	}

	/**
	 * Unsafe version of {@link #get(long)} that does not check for out of bounds
	 * access if assertions are disabled. The caller has to make sure that pos is
	 * not negative and smaller than {@link #size()}.
	 *
	 * @param pos position of the element to return
	 * @return the element at position {@code pos}
	 */
	private long getUnsafe(final int pos) {
		assert pos >= 0 && pos < size : "index out of bounds at " + pos;
		return data[pos];
	}

	/**
	 * Returns the {@code length} elements starting at {@code from}.
	 *
	 * @param from   position of the first element
	 * @param length number of elements
	 * @return the {@code length} elements starting at {@code from}
	 * @throws IndexOutOfBoundsException if {@code from} or {@code length} is
	 *                                   negative, or if the range defined by
	 *                                   {@code from} and {@code length} is out of
	 *                                   bounds
	 */
	public long[] get(final int from, final int length) {
		if (from < 0) {
			throw new IndexOutOfBoundsException("from must not be negative, but was: " + from);
		}
		if (length < 0) {
			throw new IndexOutOfBoundsException("length must not be negative, but was: " + length);
		}

		if (from + length > size) {
			throw new IndexOutOfBoundsException("from: " + from + " length: " + length);
		}
		return Arrays.copyOfRange(data, from, from + length);
	}

	/**
	 * Returns an array containing all elements of this list.
	 *
	 * @return an array containing all elements of this list
	 */
	public long[] toArray() {
		return get(0, size);
	}

	/**
	 * Fills the given array with the elements of this list if the array can hold
	 * all elements. A new array is returned otherwise.
	 *
	 * @param input
	 * @throws NullPointerException if the specified array is null
	 * @return an array containing all elements of this list
	 */
	public long[] toArray(final long[] input) {

		if (input.length < size) {
			return toArray();
		}
		System.arraycopy(data, 0, input, 0, size);
		return input;
	}

	long[] getArrayInternal() {
		return data;
	}

	/**
	 * Sorts the list into ascending order.
	 */
	public void sort() {
		Arrays.sort(data, 0, size);
		sorted = true;
	}

	/**
	 * Sorts the list into ascending order using an algorithm than can use
	 * parallelism.
	 */
	public void parallelSort() {
		Arrays.parallelSort(data, 0, size);
		sorted = true;
	}

	/**
	 * Shuffles the list. The permutation is uniformly distributed.
	 */
	public void shuffle() {
		shuffle(ThreadLocalRandom.current());
	}

	/**
	 * Shuffles the list. The permutation is uniformly distributed.
	 *
	 * @param random the random number generator used
	 */
	public void shuffle(final Random random) {

		/*
		 * See Knuth, Donald E. Seminumerical algorithms. (1998). The Art of Computer
		 * Programming. 2. Addison–Wesley pp. 145–146. Algorithm P and exercise 18.
		 */
		for (int i = size() - 1; i > 0; i--) {
			final int other = random.nextInt(i);

			final long tmp = data[other];
			data[other] = data[i];
			data[i] = tmp;
		}

		checkIfSorted();
	}

	/**
	 * Removes all duplicate values from the list. The list will be sorted if it is
	 * not already sorted.
	 *
	 * The list can have unused capacity. Consider using {@link #trim()}.
	 */
	public void uniq() {
		if (size <= 1) {
			// nothing to do
			return;
		}
		if (!sorted) {
			sort();
		}
		uniqSorted();
	}

	private void uniqSorted() {
		int insertPosition = 1;
		long prev = data[0];
		for (int i = 1; i < size; i++) {
			final long current = data[i];

			if (prev != current) {
				data[insertPosition] = current;
				insertPosition++;
				prev = current;
			}
		}
		size = insertPosition;
	}

	private void ensureCapacity(final int newElements) {

		final int requiredCapacity = size + newElements;
		if (requiredCapacity < 0 || requiredCapacity > MAX_ARRAY_SIZE) {
			// overflow, or too big
			throw new OutOfMemoryError();
		}
		if (requiredCapacity > data.length) {

			int newCapacity = data.length + data.length / 2;
			newCapacity = Math.max(requiredCapacity, newCapacity);
			newCapacity = Math.min(MAX_ARRAY_SIZE, newCapacity);

			data = Arrays.copyOf(data, newCapacity);
		}
	}

	/**
	 * Reduces the capacity to the size of the list.
	 * <p>
	 * Call this method to reduce the memory consumption of this list.
	 */
	public void trim() {
		if (size == 0) {
			data = EMPTY_ARRAY;
		} else if (size != data.length) {
			data = Arrays.copyOf(data, size);
		}
	}

	/**
	 * Removes all elements from the list.
	 * <p>
	 * This method does not free any memory associated with this list. Call
	 * {@link #clear()} + {@link #trim()} to free associated memory.
	 */
	public void clear() {
		size = 0;
		sorted = true;
	}

	/**
	 * Returns a sequential {@link LongStream} with this collection as its source.
	 *
	 * @return a sequential {@link LongStream}
	 */
	public LongStream stream() {

		return Arrays.stream(data, 0, size);
	}

	/**
	 * Returns a parallel {@link LongStream} with this collection as its source.
	 *
	 * @return a parallel {@link LongStream}
	 */
	public LongStream parallelStream() {
		final OfLong spliterator = Arrays.spliterator(data, 0, size);
		return StreamSupport.longStream(spliterator, true);
	}

	/**
	 * Returns the index of the first occurrence of {@code value}, or -1 if it does
	 * not exist.
	 * <p>
	 * This method uses a binary search algorithm if the list is sorted.
	 *
	 * @param value the value
	 * @return the index, or -1
	 * @see #isSorted()
	 */
	public int indexOf(final long value) {
		return indexOf(value, 0);
	}

	/**
	 * Returns the index of the first occurrence of {@code value}, or -1 if it does
	 * not exist.
	 * <p>
	 * This method uses a binary search algorithm if the list is sorted.
	 *
	 * @param value  the value
	 * @param offset the offset (inclusive). There is no invalid value. If the
	 *               offset is negative, then it behaves as if it was 0. If it is
	 *               &gt;= {@link #size()}, then -1 is returned.
	 * @return the index, or -1
	 * @throws ArrayIndexOutOfBoundsException if offset is negative, or larger than
	 *                                        the size of the list
	 * @throws IllegalArgumentException       if offset is negative
	 * @see #isSorted()
	 */
	public int indexOf(final long value, final int offset) {

		int result = -1;
		if (sorted && offset >= 0 && offset < size()) {
			int insertionPoint = Arrays.binarySearch(data, offset, size(), value);
			while (insertionPoint > offset && data[insertionPoint - 1] == value) {
				insertionPoint--;
			}
			result = insertionPoint < 0 ? -1 : insertionPoint;
		} else {
			final int start = Math.max(offset, 0);
			for (int i = start; i < size; i++) {
				if (data[i] == value) {
					result = i;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Returns the index of the last occurrence of {@code value} searching
	 * backwards, or -1 if it does not exist.
	 * <p>
	 * This method uses a binary search algorithm if the list is sorted.
	 *
	 * @param value the value
	 * @return the index, or -1
	 * @see #isSorted()
	 */
	public int lastIndexOf(final long value) {
		return lastIndexOf(value, size() - 1);
	}

	/**
	 * Returns the index of the last occurrence of {@code value} searching backwards
	 * from {@code fromIndex}, or -1 if it does not exist.
	 * <p>
	 * This method uses a binary search algorithm if the list is sorted.
	 *
	 * @param value     the value
	 * @param fromIndex the index the start the search from (inclusive). There is no
	 *                  invalid input. If {@code fromIndex} is &lt; 0, then -1 is
	 *                  returned. If it is larger than the length of the list, then
	 *                  it behaves as if it were {@link #size()}-1.
	 * @return the index, or -1
	 * @see #isSorted()
	 */
	public int lastIndexOf(final long value, final int fromIndex) {
		int result = -1;

		if (sorted) {
			final int toIndex = Math.min(size - 1, fromIndex + 1); // toIndex is exclusive in binarySearch, but
																	// fromIndex is inclusive
			if (toIndex > 0) {
				int insertionPoint = Arrays.binarySearch(data, 0, toIndex, value);

				// if value exists more than once, then binarySearch can find any one of them,
				// but we are looking for the last occurrence
				while (insertionPoint >= 0 && insertionPoint < fromIndex && data[insertionPoint + 1] == value) {
					insertionPoint++;
				}
				result = insertionPoint < 0 ? -1 : insertionPoint;
			}
		} else {
			final int startIndex = Math.min(size - 1, fromIndex);
			for (int i = startIndex; i >= 0; i--) {
				if (data[i] == value) {
					result = i;
					break;
				}
			}
		}
		return result;
	}

	@Override
	public String toString() {

		assert data != null : "data cannot be null";
		final int iMax = size - 1;
		if (iMax == -1)
			return "[]";

		final StringBuilder result = new StringBuilder();
		result.append('[');
		for (int i = 0; i < size; i++) {
			if (i > 0) {
				result.append(", ");
			}
			result.append(data[i]);
		}
		return result.append(']').toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + size;

		/*
		 * only consider values in the range of 0 to size
		 */
		for (int i = 0; i < size; i++) {
			result = 31 * result + (int) data[i]; // low bits
			result = 31 * result + (int) (data[i] >> 32); // high bits
		}

		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final LongList other = (LongList) obj;
		if (size != other.size)
			return false;
		if (sorted != other.sorted)
			return false;

		/*
		 * only consider values in the range of 0 to size
		 */
		for (int i = 0; i < size; i++) {
			if (data[i] != other.data[i]) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Serialize the {@link LongList}.
	 * <p>
	 * This only serializes the used part of the data array.
	 *
	 * @serialData The length of the array containing the values, followed by the
	 *             values.
	 */
	private void writeObject(final java.io.ObjectOutputStream s) throws java.io.IOException {
		s.defaultWriteObject();

		// write out the array length. According to the implementation in ArrayList this
		// is needed to be compatible with clone.
		s.writeInt(size);

		for (int i = 0; i < size; i++) {
			s.writeLong(data[i]);
		}
	}

	/**
	 * Deserialize the {@link LongList}
	 */
	private void readObject(final java.io.ObjectInputStream s) throws java.io.IOException, ClassNotFoundException {
		data = EMPTY_ARRAY;

		s.defaultReadObject();

		// Read in capacity
		s.readInt(); // ignored

		if (size > 0) {
			final long[] local_data = new long[size];

			for (int i = 0; i < size; i++) {
				local_data[i] = s.readLong();
			}
			data = local_data;
		}
	}

	@Override
	public LongList clone() {
		try {
			final LongList result = (LongList) super.clone();
			result.data = size == 0 ? EMPTY_ARRAY : Arrays.copyOf(data, size);
			result.sorted = sorted;
			return result;
		} catch (final CloneNotSupportedException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Returns a list with all elements that are in {@code a} and {@code b}.
	 * <p>
	 * The cardinality of each element will be equal to one (like in a set). This
	 * method returns a new list. The list can have unused capacity. Consider using
	 * {@link #trim()}.
	 * <p>
	 * If both lists were sorted, then the output list will also be sorted.
	 * <p>
	 * See {@link #retainAll(LongList)} for a method that modifies the list and
	 * keeps duplicate values.
	 * <p>
	 * If both lists are sorted, then the time complexity is O(n+m), where n is the
	 * length of the first list and m the length of the second list. If at least one
	 * list is not sorted, then the time complexity is O(n*m), where n is the length
	 * of the shorter list and m the length of the longer list.
	 *
	 * @param a a sorted {@link LongList}
	 * @param b a sorted {@link LongList}
	 * @return {@link LongList} containing all elements that are in {@code a} and
	 *         {@code b}
	 * @throws NullPointerException if {@code a} or {@code b} is null
	 * @see #retainAll(LongList)
	 * @see #trim()
	 */
	public static LongList intersection(final LongList a, final LongList b) {
		final LongList result;

		if (a.isSorted() && b.isSorted()) {
			result = intersectionSorted(a, b);
		} else {
			result = intersectionUnsorted(a, b);
		}
		return result;
	}

	/**
	 * Implements an intersection algorithm with O(n+m), where n is the length of
	 * the first list and m the length of the second list.
	 *
	 * @param a first list
	 * @param b second list
	 * @return the intersection
	 */
	private static LongList intersectionSorted(final LongList a, final LongList b) {
		final int aSize = a.size();
		final int bSize = b.size();
		final LongList result = new LongList(Math.min(aSize, bSize));

		assert a.isSorted() : "first list must be sorted";
		assert b.isSorted() : "second list must be sorted";

		int l = 0;
		int r = 0;

		while (l < aSize && r < bSize) {

			final long lv = a.getUnsafe(l);
			final long rv = b.getUnsafe(r);

			if (lv < rv) {
				l++;
			} else if (lv > rv) {
				r++;
			} else {
				result.addUnsafe(lv);
				do {
					l++;
				} while (l < aSize && lv == a.getUnsafe(l));
				do {
					r++;
				} while (r < bSize && rv == b.getUnsafe(r));
			}
		}
		return result;
	}

	/**
	 * Implements an algorithm with O(n*m), where n is the length of the shorter
	 * list and m the length of the longer list.
	 *
	 * @param a first list
	 * @param b second list
	 * @return the intersection
	 */
	private static LongList intersectionUnsorted(final LongList a, final LongList b) {
		final int aSize = a.size();
		final int bSize = b.size();
		final LongList result;

		if (aSize <= bSize) {
			result = new LongList(Math.min(aSize, bSize));

			for (int l = 0; l < aSize; l++) {
				final long lv = a.getUnsafe(l);

				if (b.indexOf(lv) >= 0 && result.indexOf(lv) < 0) {
					result.addUnsafe(lv);
				}

				while (l + 1 < aSize && lv == a.getUnsafe(l + 1)) {
					l++;
				}
			}
		} else {
			result = intersectionUnsorted(b, a);
		}

		return result;
	}

	/**
	 * Returns a list with all elements that are in list {@code a} or {@code b} or
	 * ... or {@code n} (logical or).
	 * <p>
	 * The result does not contain duplicate values.
	 * <p>
	 * If all lists were sorted, then the output list will also be sorted. If at
	 * least one list is unsorted, then the order is undefined.
	 * <p>
	 * TODO check time complexity If all lists are sorted, then the time complexity
	 * is O(n+m), where n is the length of the first list and m the length of the
	 * second list. If at least one list is not sorted, then the time complexity is
	 * O(m*log(m)), where m is the length of the longest list.
	 *
	 * @param longLists the lists
	 * @return the union of both lists
	 */
	public static LongList union(final LongList... longLists) {
		return union(List.of(longLists));
	}

	private static LongList unionInternal(final LongList a, final LongList b) {
		final LongList result;

		if (a.isEmpty() && b.isEmpty()) {
			result = new LongList();
		} else if (a.isSorted() && b.isSorted()) {
			result = unionSorted(a, b);
		} else {
			result = unionUnsorted(a, b);
		}
		return result;
	}

	public static LongList union(final Collection<LongList> longLists) {
		switch (longLists.size()) {
		case 0:
			return new LongList();
		case 1:
			// remove duplicate values
			return unionInternal(longLists.iterator().next(), LongList.of());
		case 2:
			final Iterator<LongList> it = longLists.iterator();
			final LongList a = it.next();
			final LongList b = it.next();
			return unionInternal(a, b);
		default:
			final List<LongList> sortedLists = toSortedLists(longLists);

			final double averageLength = totalLength(longLists) / (double) longLists.size();

			final List<LongList> sortedConcatenatedLists;
			// benchmarks showed that concatenation is beneficial for longer lists
			if (averageLength > FLAGS_UNION_CONCATENATE_NON_OVERLAPPING_AVG_MIN) 
			{
				final ListConcatenater listConcatenater = new ListConcatenater(sortedLists);
				sortedConcatenatedLists = listConcatenater.concatenateNonOverlapping();
			} else {
				sortedConcatenatedLists = sortedLists;
			}

			switch (sortedConcatenatedLists.size()) {
			case 0:
				return new LongList();
			case 1:
				// remove duplicate values
				return unionInternal(sortedConcatenatedLists.get(0), LongList.of());
			case 2:
			case 3:
			case 4:
			case 5:
				// benchmarks have shown that the trivial merge is faster when merging only a
				// few lists
				return unionRepeatedTwowayMerge(sortedConcatenatedLists);
			default:
				final LongList multiwayMerged = MultiwayLongMerger.unionSorted(sortedConcatenatedLists);
				return multiwayMerged;
			}
		}
	}

	private static int totalLength(Collection<LongList> longLists) {
		int totalLength = 0;
		for (LongList longList : longLists) {
			totalLength += longList.size();
		}
		return totalLength;
	}

	private static LongList unionRepeatedTwowayMerge(final List<LongList> sortedLongLists) {

		LongList result = sortedLongLists.get(0);
		for (int i = 1; i < sortedLongLists.size(); i++) {
			result = LongList.unionSorted(result, sortedLongLists.get(i));
		}

		return result;
	}

	private static class ListConcatenater {

		private static class ListLongList {
			private final List<LongList> list = new ArrayList<>();

			public ListLongList(LongList longList) {
				list.add(longList);
			}

			public void add(ListLongList listLongList) {
				list.addAll(listLongList.list);
			}

			public LongList toLongList() {
				switch (list.size()) {
				case 0:
					return new LongList(0);
				case 1:
					return list.get(0);
				default:
					int capacity = Math.toIntExact(list.stream().mapToLong(LongList::size).sum());
					final LongList result = new LongList(capacity);
					result.addAll(list);
					return result;
				}
			}

			public long first() {
				return list.get(0).first();
			}

			public long last() {
				return list.get(list.size() - 1).last();
			}
		}

		final TreeMap<Long, List<ListLongList>> lowestValueMap = new TreeMap<>();
		final TreeMap<Long, List<ListLongList>> highestValueMap = new TreeMap<>();

		public ListConcatenater(final Collection<LongList> sortedLongLists) {
			sortedLongLists.stream().map(ListLongList::new).forEach(this::index);
		}

		private void index(ListLongList listLongList) {
			final long lowestValue = listLongList.first();
			final long highestValue = listLongList.last();
			lowestValueMap.computeIfAbsent(lowestValue, k -> new ArrayList<>()).add(listLongList);
			highestValueMap.computeIfAbsent(highestValue, k -> new ArrayList<>()).add(listLongList);
		}

		private void removeFromIndex(ListLongList listLongList) {
			lowestValueMap.get(listLongList.first()).remove(listLongList);
			highestValueMap.get(listLongList.last()).remove(listLongList);
		}

		public List<LongList> concatenateNonOverlapping() {
			for (Entry<Long, List<ListLongList>> e : highestValueMap.entrySet()) {
				final long highestValue = e.getKey();
				if (highestValue == Long.MAX_VALUE) {
					continue;
				}

				final Iterator<ListLongList> it = e.getValue().iterator();
				while (it.hasNext()) {
					final ListLongList lowList = it.next();
					final Entry<Long, List<ListLongList>> ceilingEntry = lowestValueMap.ceilingEntry(highestValue + 1);
					if (ceilingEntry != null && !ceilingEntry.getValue().isEmpty()) {
						final ListLongList highList = ceilingEntry.getValue().get(0);
						removeFromIndex(highList);

						it.remove(); // prevents concurrent modification that would happen in removeFromIndex()
						removeFromIndex(lowList);

						lowList.add(highList);

						index(lowList);
					}
				}
			}

			final List<LongList> result = new ArrayList<>();
			for (List<ListLongList> l : highestValueMap.values()) {
				for (ListLongList listLongList : l) {
					result.add(listLongList.toLongList());
				}
			}
			return result;
		}

	}

	private static List<LongList> toSortedLists(final Collection<LongList> longLists) {
		final List<LongList> result = new ArrayList<>();

		for (LongList longList : longLists) {
			if (longList.isEmpty()) {
				// skip, no need to merge an empty list
			} else if (longList.isSorted()) {
				result.add(longList);
			} else {
				final LongList copy = longList.clone();
				copy.sort();
				result.add(copy);
			}
		}

		return result;
	}

	private static LongList unionSorted(final LongList a, final LongList b) {

		final int aSize = a.size();
		final int bSize = b.size();

		final LongList result = new LongList(aSize + bSize);

		int l = 0;
		int r = 0;

		while (l < aSize && r < bSize) {

			final long lv = a.getUnsafe(l);
			final long rv = b.getUnsafe(r);

			if (lv < rv) {
				result.addUnsafe(lv);
				l++;
				while (l < aSize && lv == a.getUnsafe(l)) {
					l++;
				}
			} else if (lv > rv) {
				result.addUnsafe(rv);
				r++;
				while (r < bSize && rv == b.getUnsafe(r)) {
					r++;
				}
			} else {
				result.addUnsafe(lv);
				l++;
				r++;

				while (l < aSize && lv == a.getUnsafe(l)) {
					l++;
				}
				while (r < bSize && rv == b.getUnsafe(r)) {
					r++;
				}
			}
		}

		// add remaining values from list a, if any exist
		for (; l < aSize; l++) {
			final long valueAtPosL = a.getUnsafe(l);
			if (l == 0 || valueAtPosL != a.getUnsafe(l - 1)) {
				result.addUnsafe(valueAtPosL);
			}
		}

		// add remaining values from list b, if any exist
		for (; r < bSize; r++) {
			final long valueAtPosR = b.getUnsafe(r);
			if (r == 0 || valueAtPosR != b.getUnsafe(r - 1)) {
				result.addUnsafe(valueAtPosR);
			}
		}

		return result;
	}

	private static LongList unionUnsorted(final LongList a, final LongList b) {
		final LongList aSorted = new LongList(a);
		if (!aSorted.isSorted()) {
			aSorted.parallelSort();
		}
		final LongList bSorted = new LongList(b);
		if (!bSorted.isSorted()) {
			bSorted.parallelSort();
		}

		return unionSorted(aSorted, bSorted);
	}

	private void checkIfSorted() {
		sorted = true;
		for (int i = 1; i < size && sorted; i++) {
			sorted = data[i - 1] <= data[i];
		}
	}
}
