package org.lucares.collections;

import java.util.function.Predicate;

/**
 * A {@link Predicate} for primitive integers.
 */
@FunctionalInterface
public interface IntPredicate {

	/**
	 * Evaluates the predicate.
	 *
	 * @param value
	 *            the value
	 * @param index
	 *            the index in the list
	 * @return {@code true} iff the input argument matches the predicate
	 */
	boolean test(int value, int index);

	/**
	 * Returns a predicate that represents the logical AND of {@code this} and
	 * another predicate. The and operation is short-circuiting.
	 *
	 * @param other
	 *            the other predicate
	 * @return the result of combining both predicates with a logical AND operation
	 * @throws NullPointerException
	 *             if {@code other} is null
	 */
	default IntPredicate and(final IntPredicate other) {
		return (value, index) -> test(value, index) && other.test(value, index);
	}

	/**
	 * Returns a predicate that represents the logical OR of {@code this} and
	 * another predicate. The and operation is short-circuiting.
	 *
	 * @param other
	 *            the other predicate
	 * @return the result of combining both predicates with a logical OR operation
	 * @throws NullPointerException
	 *             if {@code other} is null
	 */
	default IntPredicate or(final IntPredicate other) {
		return (value, index) -> test(value, index) || other.test(value, index);
	}

	/**
	 * Returns a predicate that negates the result of this predicate.
	 *
	 * @return the negation of {@code this}
	 */
	default IntPredicate negate() {
		return (value, index) -> !test(value, index);
	}
}
