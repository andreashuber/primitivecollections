package org.lucares.collections;

public interface BiLongObjectFunction<O> {
	O apply(long key, O value);
}
