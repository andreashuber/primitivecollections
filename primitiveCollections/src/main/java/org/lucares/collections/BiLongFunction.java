package org.lucares.collections;

@FunctionalInterface
public interface BiLongFunction {
	long apply(long key, long value);
}
