package org.lucares.collections;

public interface LongObjConsumer<O> {
	void accept(long key, O value);
}
