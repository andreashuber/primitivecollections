package org.lucares.collections;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.LongStream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
public class BenchmarkLongLongMapForEach {

	@Param({ "1000", "10000", "50000" })
	private int values;

	private LongLongHashMap unsortedMap = new LongLongHashMap();
	private LongLongHashMap unsortedMapLowValues = new LongLongHashMap();

	private org.eclipse.collections.impl.map.mutable.primitive.LongLongHashMap eclipseUnsortedMap = new org.eclipse.collections.impl.map.mutable.primitive.LongLongHashMap();

	@Setup
	public void setup() throws Exception {
		final int seed = 6789;
		final Random rand = new Random(seed);
		LongStream.generate(rand::nextLong).limit(values).forEach(l -> unsortedMap.put(l, 2 * l));

		rand.setSeed(seed);
		final LongList lowValues = LongList.range(0, values);
		lowValues.shuffle(rand);
		lowValues.stream().forEach(l -> unsortedMapLowValues.put(l, 2 * l));

		rand.setSeed(seed);
		LongStream.generate(rand::nextLong).limit(values).forEach(l -> eclipseUnsortedMap.put(l, 2 * l));
	}

	@TearDown
	public void tearDown() {
		unsortedMap = null;
		unsortedMapLowValues = null;
		eclipseUnsortedMap = null;
	}

	@Benchmark
	public void testLongLongMapForEach(final Blackhole blackhole) throws Exception {
		final long[] tmp = new long[] { 0 };
		unsortedMap.forEach((k, v) -> tmp[0] += k);
		blackhole.consume(tmp[0]);
	}

	@Benchmark
	public void testLongLongMapForEachLowValues(final Blackhole blackhole) throws Exception {
		final long[] tmp = new long[] { 0 };
		unsortedMapLowValues.forEach((k, v) -> tmp[0] += k);
		blackhole.consume(tmp[0]);
	}

	@Benchmark
	public void testLongLongMapForEachOrdered(final Blackhole blackhole) throws Exception {
		final long[] tmp = new long[] { 0 };
		unsortedMap.forEachOrdered((k, v) -> tmp[0] += k);
		blackhole.consume(tmp[0]);
	}

	@Benchmark
	public void testLongLongMapForEachOrderedLowValues(final Blackhole blackhole) throws Exception {
		final long[] tmp = new long[] { 0 };
		unsortedMapLowValues.forEachOrdered((k, v) -> tmp[0] += k);
		blackhole.consume(tmp[0]);
	}

	@Benchmark
	public void testEclipseLongLongMapForEach(final Blackhole blackhole) throws Exception {
		final long[] tmp = new long[] { 0 };
		eclipseUnsortedMap.forEachKeyValue((k, v) -> tmp[0] += k);
		blackhole.consume(tmp[0]);
	}
}
