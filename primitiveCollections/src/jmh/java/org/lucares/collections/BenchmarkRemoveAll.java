package org.lucares.collections;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
public class BenchmarkRemoveAll {

	private IntList base = null;
	private IntList remove = null;

	@Setup
	public void setup() throws Exception {

		final int values = 10_000;
		base = IntList.of();
		IntStream.range(0, values).forEachOrdered(i -> base.add(i));
		base.sort();

		remove = IntList.of();
		IntStream.range(0, 50).forEachOrdered(i -> remove.add(i));
		remove.sort();
	}

	@TearDown
	public void tearDown() {
		base = null;
		remove = null;
	}

	@Benchmark
	public void testRemoveAll() throws Exception {

		final IntList tmp = base.clone();
		tmp.removeAll(remove);
	}

	@Benchmark
	public void testRemoveAll_withRemoveIf() throws Exception {

		final IntList tmp = base.clone();
		tmp.removeIf((val, index) -> remove.indexOf(val) >= 0);
	}
}
