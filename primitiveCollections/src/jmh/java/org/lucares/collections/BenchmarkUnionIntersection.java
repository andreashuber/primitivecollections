package org.lucares.collections;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
public class BenchmarkUnionIntersection {

	@Param({ "10000", "20000" })
	private int values;

	private IntList aIntSorted = null;
	private IntList bIntSorted = null;
	private IntList cIntUnsorted = null;
	private IntList dIntUnsorted = null;

	private LongList aLongSorted = null;
	private LongList bLongSorted = null;
	private LongList cLongUnsorted = null;
	private LongList dLongUnsorted = null;

	@Setup
	public void setup() throws Exception {

		aIntSorted = IntList.of();
		IntStream.range(0, values).forEachOrdered(aIntSorted::add);
		aIntSorted.sort();

		bIntSorted = IntList.of();
		IntStream.range(0, values).forEachOrdered(bIntSorted::add);
		bIntSorted.sort();

		cIntUnsorted = IntList.of();
		IntStream.range(0, values).forEachOrdered(cIntUnsorted::add);
		cIntUnsorted.shuffle();

		dIntUnsorted = IntList.of();
		IntStream.range(0, values).forEachOrdered(dIntUnsorted::add);
		dIntUnsorted.shuffle();

		// -----------------------

		aLongSorted = LongList.of();
		LongStream.range(0, values).forEachOrdered(aLongSorted::add);
		aLongSorted.sort();

		bLongSorted = LongList.of();
		LongStream.range(0, values).forEachOrdered(bLongSorted::add);
		bLongSorted.sort();

		cLongUnsorted = LongList.of();
		LongStream.range(0, values).forEachOrdered(cLongUnsorted::add);
		cLongUnsorted.shuffle();

		dLongUnsorted = LongList.of();
		LongStream.range(0, values).forEachOrdered(dLongUnsorted::add);
		dLongUnsorted.shuffle();

	}

	@TearDown
	public void tearDown() {
		aIntSorted = null;
		bIntSorted = null;
		cIntUnsorted = null;
		dIntUnsorted = null;
		aLongSorted = null;
		bLongSorted = null;
		cLongUnsorted = null;
		dLongUnsorted = null;
	}

	@Benchmark
	public void testIntUnionSortedLists() throws Exception {

		IntList.union(aIntSorted, bIntSorted);
	}

	@Benchmark
	public void testIntIntersectionSortedLists() throws Exception {

		IntList.intersection(aIntSorted, bIntSorted);
	}

	@Benchmark
	public void testIntUnionUnsortedLists() throws Exception {

		IntList.union(cIntUnsorted, dIntUnsorted);
	}

	@Benchmark
	public void testIntIntersectionUnsortedLists() throws Exception {

		IntList.intersection(cIntUnsorted, dIntUnsorted);
	}

	@Benchmark
	public void testLongUnionSortedLists() throws Exception {

		LongList.union(aLongSorted, bLongSorted);
	}

	@Benchmark
	public void testLongIntersectionSortedLists() throws Exception {

		LongList.intersection(aLongSorted, bLongSorted);
	}

	@Benchmark
	public void testLongUnionUnsortedLists() throws Exception {

		LongList.union(cLongUnsorted, dLongUnsorted);
	}

	@Benchmark
	public void testLongIntersectionUnsortedLists() throws Exception {

		LongList.intersection(cLongUnsorted, dLongUnsorted);
	}
}
