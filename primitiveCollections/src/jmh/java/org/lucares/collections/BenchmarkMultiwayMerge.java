package org.lucares.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.LongStream;

import org.lucares.collections.MultiwayLongMerger.LongQueue;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 5, time = 500, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 3, time = 500, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
public class BenchmarkMultiwayMerge {

	private static final String OPTION_RANDOM = "random";

	private static final String OPTION_NON_OVERLAP = "non-overlap";

	// @Param({ "3","5", "10","20", "1000" })
	@Param({ "1000" })
	private int anumLists;

	// @Param({ "10", "1000" , "20000" })
	@Param({ "1000" })
	private int bvalues;

	@Param({ OPTION_RANDOM, OPTION_NON_OVERLAP })
	// @Param({ OPTION_NON_OVERLAP})
	private String ctype;

	@Param({ "0", "500" })
	private int dconcatNonOverlap;

	private List<LongList> longSorted = null;

	@Setup
	public void setup() throws Exception {
		ThreadLocalRandom rng = ThreadLocalRandom.current();
		longSorted = new ArrayList<>();
		if (ctype.equalsIgnoreCase(OPTION_NON_OVERLAP)) {

			final LongList list = randomList(bvalues * anumLists, rng);
			list.sort();
			for (int i = 0; i < anumLists; i++) {
				longSorted.add(list.sublist(i * bvalues, (i + 1) * bvalues));
			}

		} else {
			for (int i = 0; i < anumLists; i++) {
				final LongList list;
				if (ctype.equalsIgnoreCase(OPTION_RANDOM)) {
					list = randomList(bvalues, rng);
					list.sort();
				} else {
					list = new LongList(bvalues);
					LongStream.range(0, bvalues).forEachOrdered(list::add);
				}
				longSorted.add(list);
			}
		}

		LongList.FLAGS_UNION_CONCATENATE_NON_OVERLAPPING_AVG_MIN = dconcatNonOverlap;
	}

	private LongList randomList(int values, ThreadLocalRandom rng) {
		final LongList list = new LongList(values);
		for (int j = 0; j < values; j++) {
			list.add(rng.nextLong());
		}
		return list;
	}

	@TearDown
	public void tearDown() {
		longSorted = null;
	}

	@Benchmark
	public void testUnionSortedLists_MultiwayMerge() throws Exception {

		LongList.union(longSorted);
	}

	// @Benchmark
	public void testUnionSortedLists_TwowayMergeImplementation() throws Exception {

		twowayMerge(longSorted);
	}

	private void twowayMerge(List<LongList> longLists) {
		LongList result = longLists.get(0);
		for (int i = 1; i < longLists.size(); i++) {
			result = LongList.union(result, longLists.get(i));
		}
	}

	public static void main(String[] args) throws Exception {
		System.out.println("\n\n----------------\nstart");

		for (int i = 0; i < 80; i++) {
			BenchmarkMultiwayMerge benchmark = new BenchmarkMultiwayMerge();
			benchmark.anumLists = 1000;
			benchmark.bvalues = 1000;
			benchmark.ctype = "non-overlapping";
			benchmark.dconcatNonOverlap = 500;
			benchmark.setup();
			long start = System.nanoTime();
			benchmark.testUnionSortedLists_MultiwayMerge();
			System.out.println("total: " + (System.nanoTime() - start) / 1_000_000.0 + " ms");
		}
		System.out.println("done");
	}
}
