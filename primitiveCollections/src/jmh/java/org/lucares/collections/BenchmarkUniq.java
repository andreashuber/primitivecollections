package org.lucares.collections;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
public class BenchmarkUniq {
	private IntList aIntSorted = null;
	private IntList bIntUnsorted = null;

	private LongList aLongSorted = null;
	private LongList bLongUnsorted = null;

	@Setup
	public void setup() throws Exception {

		final int values = 10_000;
		aIntSorted = IntList.of();
		IntStream.range(0, values).forEachOrdered(aIntSorted::add);
		aIntSorted.sort();

		bIntUnsorted = IntList.of();
		IntStream.range(0, values).forEachOrdered(bIntUnsorted::add);
		bIntUnsorted.shuffle();

		// -----------------------

		aLongSorted = LongList.of();
		LongStream.range(0, values).forEachOrdered(aLongSorted::add);
		aLongSorted.sort();

		bLongUnsorted = LongList.of();
		LongStream.range(0, values).forEachOrdered(bLongUnsorted::add);
		bLongUnsorted.shuffle();
	}

	@TearDown
	public void tearDown() {
		aIntSorted = null;
		bIntUnsorted = null;

		aLongSorted = null;
		bLongUnsorted = null;
	}

	@Benchmark
	public void testIntUniqSorted() throws Exception {
		aIntSorted.clone().uniq();
	}

	@Benchmark
	public void testIntUniqUnsorted() throws Exception {
		bIntUnsorted.clone().uniq();
	}

	@Benchmark
	public void testLongUniqSorted() throws Exception {
		aLongSorted.clone().uniq();
	}

	@Benchmark
	public void testLongUniqUnsorted() throws Exception {
		bLongUnsorted.clone().uniq();
	}
}
