package org.lucares.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Sparse2DLongArrayTest {
	@Test
	public void testPutValues() {
		final Sparse2DLongArray matrix2d = new Sparse2DLongArray();

		// put values
		for (long i = 1; i < 5; i++) {
			for (long j = 1; j < 5; j++) {
				matrix2d.put(i, j, i * j);
			}
		}
		Assertions.assertEquals("  0  0  0  0  0\n" + //
				"  0  1  2  3  4\n" + //
				"  0  2  4  6  8\n" + //
				"  0  3  6  9 12\n" + //
				"  0  4  8 12 16\n", //
				matrix2d.toString(120));

		// update values
		for (long i = 1; i < 5; i++) {
			for (long j = 1; j < 5; j++) {
				long value = matrix2d.get(i, j);
				matrix2d.put(i, j, value * 2);
			}
		}

		// get values
		for (long i = 1; i < 5; i++) {
			for (long j = 1; j < 5; j++) {
				final long expectedValue = i * j * 2;
				final long actualValue = matrix2d.get(i, j);
				Assertions.assertEquals(expectedValue, actualValue, "value at position " + i + "x" + j);
			}
		}
		
		Assertions.assertEquals("  0  0  0  0  0\n" + //
				"  0  2  4  6  8\n" + //
				"  0  4  8 12 16\n" + //
				"  0  6 12 18 24\n" + //
				"  0  8 16 24 32\n", //
				matrix2d.toString(120));
	}

	@Test
	public void testPutGetWithLargeIndexes() {
		final Sparse2DLongArray matrix2d = new Sparse2DLongArray();
		final long value = 1;
		final long x = Long.MAX_VALUE;
		final long y = Long.MAX_VALUE;
		matrix2d.put(x, y, value);
		
		long actualValue = matrix2d.get(x, y);
		Assertions.assertEquals(value, actualValue, "value at position " + x + "x" + y);
		
		Assertions.assertEquals("(9223372036854775807, 9223372036854775807) = 1\n", //
				matrix2d.toString(120));
	}
	
	@Test
	public void testWithLimitedSize() {
		final Sparse2DLongArray matrix2d = new Sparse2DLongArray(10, 10);
		final long value = 1;
		matrix2d.put(9, 9, value);
		
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, ()-> {matrix2d.put(10, 9, value);});
		Assertions.assertThrows(IndexOutOfBoundsException.class, ()-> {matrix2d.put(9, 10, value);});
		
	}

	@Test
	public void testToStringWhenNotEnougthSpace() {
		final Sparse2DLongArray matrix2d = new Sparse2DLongArray();

		for (long i = 1; i < 5; i++) {
			for (long j = 1; j < 5; j++) {
				matrix2d.put(i, j, i * j);
			}
		}

		// NOTE: this expectation relies on a deterministic order of forEach
		// (which is used in toString(), even though the order is not deterministic.
		// We can to this here, because we have very small indices and their hash
		// value is identical to the index. But that could change with a different
		// hash algorithm.
		Assertions.assertEquals("(1, 1) = 1\n" + //
				"(1, 2) = 2\n" + //
				"(1, 3) = 3\n" + //
				"(1, 4) = 4\n" + //
				"(2, 1) = 2\n" + //
				"(2, 2) = 4\n" + //
				"(2, 3) = 6\n" + //
				"(2, 4) = 8\n" + //
				"(3, 1) = 3\n" + //
				"(3, 2) = 6\n" + //
				"(3, 3) = 9\n" + //
				"(3, 4) = 12\n" + //
				"(4, 1) = 4\n" + //
				"(4, 2) = 8\n" + //
				"(4, 3) = 12\n" + //
				"(4, 4) = 16\n", //
				matrix2d.toString(12));
	}
}
