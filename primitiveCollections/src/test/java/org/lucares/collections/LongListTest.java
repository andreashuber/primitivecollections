package org.lucares.collections;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LongListTest {

	@Test
	public void testEmptyList() {
		final LongList list = new LongList();

		Assertions.assertEquals(true, list.isEmpty());
	}

	@Test
	public void testCopyConstructor() {
		final LongList list = new LongList();
		list.addAll(1, 2, 3, 4, 5);

		final LongList copy = new LongList(list);

		Assertions.assertArrayEquals(copy.toArray(), list.toArray());
	}

	@Test
	public void testInvalidInitialCapacity() {
		try {
			new LongList(-1);
			Assertions.fail();
		} catch (final IllegalArgumentException e) {
			// expected
		}
	}

	@Test
	public void testRange() {
		final LongList expected = LongList.of(1, 2, 3, 4, 5);
		final LongList list = LongList.range(1, 6);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(5, list.getCapacity());
	}

	@Test
	public void testRangeWithNegativeValues() {
		final LongList expected = LongList.of(Long.MIN_VALUE, Long.MIN_VALUE + 1, Long.MIN_VALUE + 2);
		final LongList list = LongList.range(Long.MIN_VALUE, Long.MIN_VALUE + 3);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(3, list.getCapacity());
	}

	@Test
	public void testEmptyRange() {
		final LongList expected = LongList.of();
		final LongList list = LongList.range(1, 1);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(0, list.getCapacity());
	}

	@Test
	public void testNegativeRange() {
		final LongList expected = LongList.of();
		final LongList list = LongList.range(1, 0);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(0, list.getCapacity());
	}

	@Test
	public void testRangeClosed() {
		final LongList expected = LongList.of(1, 2, 3, 4, 5);
		final LongList list = LongList.rangeClosed(1, 5);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(5, list.getCapacity());
	}

	@Test
	public void testRangeClosedWithNegativeValues() {
		final LongList expected = LongList.of(Long.MIN_VALUE, Long.MIN_VALUE + 1, Long.MIN_VALUE + 2);
		final LongList list = LongList.rangeClosed(Long.MIN_VALUE, Long.MIN_VALUE + 2);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(3, list.getCapacity());
	}

	@Test
	public void testOneElementRangeClosed() {
		final LongList expected = LongList.of(1);
		final LongList list = LongList.rangeClosed(1, 1);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(1, list.getCapacity());
	}

	@Test
	public void testEmptyRangeClosed() {
		final LongList expected = LongList.of();
		final LongList list = LongList.rangeClosed(1, 0);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(0, list.getCapacity());
	}

	@Test
	public void testRangeClosedTooBig() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			LongList.range(0, Integer.MAX_VALUE + 1L);
		});
	}

	@Test
	public void testAdd() {
		// setting initial size to one, so that the first add does not need to resize,
		// but the second add must
		final LongList list = new LongList(1);

		list.add(1);

		Assertions.assertFalse(list.isEmpty());
		Assertions.assertEquals(1, list.size());

		list.add(2);
		Assertions.assertEquals(2, list.size());

		Assertions.assertEquals(1, list.get(0));
		Assertions.assertEquals(2, list.get(1));
	}

	@Test
	public void testInsert() {
		final LongList list = new LongList();

		list.insert(0);
		Assertions.assertArrayEquals(new long[] {}, list.toArray());

		list.insert(0, 1);
		Assertions.assertArrayEquals(new long[] { 1 }, list.toArray());

		list.insert(1, 2, 2, 2);
		Assertions.assertArrayEquals(new long[] { 1, 2, 2, 2 }, list.toArray());

		list.insert(1, 3);
		Assertions.assertArrayEquals(new long[] { 1, 3, 2, 2, 2 }, list.toArray());

		list.insert(2, 4, 4, 4);
		Assertions.assertArrayEquals(new long[] { 1, 3, 4, 4, 4, 2, 2, 2 }, list.toArray());

		list.insert(2);
		Assertions.assertArrayEquals(new long[] { 1, 3, 4, 4, 4, 2, 2, 2 }, list.toArray());

		list.insert(8, 5, 5);
		Assertions.assertArrayEquals(new long[] { 1, 3, 4, 4, 4, 2, 2, 2, 5, 5 }, list.toArray());
	}

	@Test
	public void testInsertWithFlexibleApi() {
		final LongList list = new LongList();

		list.insert(0, new long[] {}, 0, 0);
		Assertions.assertArrayEquals(new long[] {}, list.toArray());

		list.insert(0, new long[] { 1, 3 }, 0, 2);
		Assertions.assertArrayEquals(new long[] { 1, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, new long[] { -1, 2, -1 }, 1, 1); // only the 2 will be inserted
		Assertions.assertArrayEquals(new long[] { 1, 2, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(2, new long[] { 7, 8 }, 0, 2);
		Assertions.assertArrayEquals(new long[] { 1, 2, 7, 8, 3 }, list.toArray());
		Assertions.assertFalse(list.isSorted());
	}

	@Test
	public void testInsertLongListWithFlexibleApi() {
		final LongList list = new LongList();

		list.insert(0, LongList.of(), 0, 0);
		Assertions.assertArrayEquals(new long[] {}, list.toArray());

		list.insert(0, LongList.of(1, 3), 0, 2);
		Assertions.assertArrayEquals(new long[] { 1, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, LongList.of(-1, 2, -1), 1, 1); // only the 2 will be inserted
		Assertions.assertArrayEquals(new long[] { 1, 2, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(2, LongList.of(7, 8), 0, 2);
		Assertions.assertArrayEquals(new long[] { 1, 2, 7, 8, 3 }, list.toArray());
		Assertions.assertFalse(list.isSorted());
	}

	@Test
	public void testInsertLongListWithSimpleApi() {
		final LongList list = new LongList();

		list.insert(0, LongList.of());
		Assertions.assertArrayEquals(new long[] {}, list.toArray());

		list.insert(0, LongList.of(1, 3));
		Assertions.assertArrayEquals(new long[] { 1, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, LongList.of(2));
		Assertions.assertArrayEquals(new long[] { 1, 2, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(3, LongList.of(6, 5));
		Assertions.assertArrayEquals(new long[] { 1, 2, 3, 6, 5 }, list.toArray());
		Assertions.assertFalse(list.isSorted());
	}

	@Test
	public void testInsertOutOfBounds() {
		final LongList list = new LongList();
		try {
			list.insert(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		try {
			list.insert(1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		try {
			list.add(1);
			list.insert(2, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			// sourcePos is out of range
			list.insert(1, LongList.of(1), 1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			// length is out of range
			list.insert(1, LongList.of(1), 0, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			// sourcePod+length is out of range
			list.insert(1, LongList.of(1, 2, 3), 2, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testInsertNull() {
		final LongList list = new LongList();
		try {
			list.insert(0, (long[]) null);
			Assertions.fail();
		} catch (final NullPointerException e) {
			// expected
		}
	}

	@Test
	public void testSet() {
		final LongList list = new LongList();
		list.addAll(0, 1, 2, 3, 4, 5);

		list.set(0, 10);
		Assertions.assertArrayEquals(new long[] { 10, 1, 2, 3, 4, 5 }, list.toArray());

		list.set(1, 11);
		Assertions.assertArrayEquals(new long[] { 10, 11, 2, 3, 4, 5 }, list.toArray());

		list.set(5, 55);
		Assertions.assertArrayEquals(new long[] { 10, 11, 2, 3, 4, 55 }, list.toArray());
	}

	@Test
	public void testSetOutOfBounds() {
		final LongList list = new LongList();
		try {
			list.set(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		try {
			list.set(1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		list.addAll(0, 1, 2, 3, 4, 5);

		try {
			list.set(6, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testGrow() {
		final LongList list = new LongList(1);

		final int size = 100;
		for (int i = 0; i < size; i++) {
			list.add(i);
		}
		Assertions.assertEquals(size, list.size());

		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(i, list.get(i));
		}
	}

	@Test
	public void testGrowLongListOfCapacityNull() {
		final LongList list = new LongList(0);

		final int size = 100;
		for (int i = 0; i < size; i++) {
			list.add(i);
		}
		Assertions.assertEquals(size, list.size());

		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(i, list.get(i));
		}
	}

	@Test
	public void testAddArray() {
		final LongList list = new LongList();

		list.addAll();
		Assertions.assertTrue(list.isEmpty());
		Assertions.assertEquals(0, list.size());

		final int size = 100;
		final long[] longs = ThreadLocalRandom.current().longs(size).toArray();

		list.addAll(longs);
		Assertions.assertEquals(size, list.size());

		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(longs[i], list.get(i));
		}

		final long[] anotherLongs = ThreadLocalRandom.current().longs(size).toArray();
		list.addAll(anotherLongs);
		Assertions.assertEquals(size * 2, list.size());
		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(anotherLongs[i], list.get(size + i));
		}
	}

	@Test
	public void testAddList() {
		final LongList list = new LongList();

		// adding empty list with no capacity
		list.addAll(LongList.of());
		Assertions.assertEquals(new LongList(), list);
		Assertions.assertTrue(list.isSorted());

		// adding empty list with capacity 2
		list.addAll(new LongList(2));
		Assertions.assertEquals(new LongList(), list);
		Assertions.assertTrue(list.isSorted());

		// adding sorted list to an empty list
		list.addAll(LongList.of(1, 2, 3));
		Assertions.assertEquals(LongList.of(1, 2, 3), list);
		Assertions.assertTrue(list.isSorted());

		// add empty list to a sorted list
		list.addAll(LongList.of());
		Assertions.assertEquals(LongList.of(1, 2, 3), list);
		Assertions.assertTrue(list.isSorted());

		// adding sorted list to a sorted list so that the list stays sorted
		list.addAll(LongList.of(3, 4, 5));
		Assertions.assertEquals(LongList.of(1, 2, 3, 3, 4, 5), list);
		Assertions.assertTrue(list.isSorted());

		// adding sorted list to a sorted list, but the new list is not sorted
		list.clear();
		list.addAll(LongList.of(1, 2, 3));
		list.addAll(LongList.of(0));
		Assertions.assertEquals(LongList.of(1, 2, 3, 0), list);
		Assertions.assertFalse(list.isSorted());

		// adding unsorted list to a sorted list
		list.clear();
		list.addAll(LongList.of(1, 2, 3));
		list.addAll(LongList.of(6, 5, 4));
		Assertions.assertEquals(LongList.of(1, 2, 3, 6, 5, 4), list);
		Assertions.assertFalse(list.isSorted());

		// adding unsorted list to an empty list
		list.clear();
		list.addAll(LongList.of(3, 2, 1));
		Assertions.assertEquals(LongList.of(3, 2, 1), list);
		Assertions.assertFalse(list.isSorted());

		// adding sorted list to an unsorted list
		list.clear();
		list.addAll(LongList.of(3, 2, 1));
		list.addAll(LongList.of(1, 2, 3));
		Assertions.assertEquals(LongList.of(3, 2, 1, 1, 2, 3), list);
		Assertions.assertFalse(list.isSorted());

	}

	@Test
	public void testGetArray() {
		final LongList list = new LongList();

		final int size = 100;
		final long[] longs = ThreadLocalRandom.current().longs(size).toArray();

		list.addAll(longs);

		final int length = 20;
		final int from = 10;
		final long[] actualLongs = list.get(from, length);

		for (int i = 0; i < length; i++) {
			Assertions.assertEquals(actualLongs[i], list.get(from + i));
			Assertions.assertEquals(actualLongs[i], longs[from + i]);
		}
	}

	@Test
	public void testGetOutOfBounds() {
		final LongList list = new LongList();

		list.addAll(0, 1, 2, 3);

		try {
			list.get(-1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(4);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testGetArrayOutOfBounds() {
		final LongList list = new LongList();

		list.addAll(0, 1, 2, 3);

		try {
			list.get(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(1, -1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(3, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(4, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testToArrayEmptyLIst() {
		final LongList list = new LongList();

		final long[] actual = list.toArray();
		Assertions.assertArrayEquals(actual, new long[0]);
	}

	@Test
	public void testToArray() {
		final LongList list = new LongList();
		list.addAll(1, 2, 3, 4, 5, 6);

		{
			final long[] input = new long[1];
			final long[] actual = list.toArray(input);
			// input is too short -> new array returned
			Assertions.assertNotSame(input, actual);
			Assertions.assertArrayEquals(list.toArray(), actual);
		}

		{
			final long[] input = new long[list.size()];
			final long[] actual = list.toArray(input);
			// input fits exactly -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(list.toArray(), actual);
		}

		{
			final long[] input = new long[list.size() + 1];
			final long[] expected = { 1, 2, 3, 4, 5, 6, 0 };
			final long[] actual = list.toArray(input);
			// input too big -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(expected, actual);
		}
	}

	@Test
	public void testToArrayWithEmptyList() {
		final LongList list = new LongList();

		{
			final long[] input = new long[0];
			final long[] actual = list.toArray(input);
			// input fits exactly -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(list.toArray(), actual);
		}

		{
			final long[] input = new long[list.size() + 1];
			final long[] expected = { 0 };
			final long[] actual = list.toArray(input);
			// input too big -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(expected, actual);
		}
	}

	@Test
	public void testRemove() {
		final LongList list = new LongList();

		final int size = 10;
		final long[] ints = new long[size];
		for (int i = 0; i < size; i++) {
			ints[i] = i;
		}

		list.addAll(ints);

		// remove nothing
		list.remove(2, 2);
		Assertions.assertArrayEquals(ints, list.toArray());

		// remove the last element
		list.remove(9, 10);
		final long[] expectedA = removeElements(ints, 9);
		Assertions.assertArrayEquals(expectedA, list.toArray());

		// remove the first element
		list.remove(0, 1);
		final long[] expectedB = removeElements(expectedA, 0);
		Assertions.assertArrayEquals(expectedB, list.toArray());

		// remove single element in the middle
		list.remove(3, 4);
		final long[] expectedC = removeElements(expectedB, 3);
		Assertions.assertArrayEquals(expectedC, list.toArray());

		// remove several elements in the middle
		list.remove(2, 5);
		final long[] expectedD = removeElements(expectedC, 2, 3, 4);
		Assertions.assertArrayEquals(expectedD, list.toArray());

		// remove all elements
		list.remove(0, 4);
		final long[] expectedE = removeElements(expectedD, 0, 1, 2, 3);
		Assertions.assertArrayEquals(expectedE, list.toArray());
	}

	@Test
	public void testRemoveOutOfRange() {
		final LongList list = new LongList();

		list.addAll(0, 1, 2, 3);

		try {
			list.remove(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.remove(1, 0);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.remove(3, 5);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.remove(4, 5);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testTrim() {
		final LongList list = new LongList();

		list.addAll(0, 1, 2, 3);

		list.trim();
		Assertions.assertEquals(4, list.getCapacity());
	}

	@Test
	public void testTrimOnEmptyList() {
		final LongList list = new LongList();

		list.trim();
		Assertions.assertEquals(0, list.getCapacity());
	}

	@Test
	public void testClear() {
		final LongList list = LongList.of(2, 0, 1); // unsorted list
		final int capacityBeforeClear = list.getCapacity();
		list.clear();

		Assertions.assertEquals(0, list.size(), "the list is empty after clear");
		Assertions.assertEquals(capacityBeforeClear, capacityBeforeClear, "capacity does not change");
		Assertions.assertTrue(list.isSorted(), "empty lists are sorted");
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testHashCodeEquals() {
		final LongList a = new LongList();
		final LongList b = new LongList();

		// on empty lists
		Assertions.assertTrue(a.equals(b));
		Assertions.assertEquals(a.hashCode(), b.hashCode());

		// on equal lists
		a.addAll(1, 2, 3, 4);
		b.addAll(1, 2, 3, 4);
		Assertions.assertTrue(a.equals(b));
		Assertions.assertEquals(a.hashCode(), b.hashCode());

		// trim only one of them
		// we want to compare the actual content
		a.trim();
		Assertions.assertTrue(a.equals(b));
		Assertions.assertEquals(a.hashCode(), b.hashCode());

		// change one value
		a.remove(2, 3);
		a.insert(2, 99);
		Assertions.assertFalse(a.equals(b));
		Assertions.assertNotEquals(a.hashCode(), b.hashCode());

		// have different length
		a.add(66);
		Assertions.assertFalse(a.equals(b));
		Assertions.assertNotEquals(a.hashCode(), b.hashCode());

		// same object
		Assertions.assertTrue(a.equals(a));

		// other is null
		Assertions.assertFalse(a.equals(null));

		// equals with a different class
		Assertions.assertFalse(a.equals("not an LongList"));

		// sorted and unsorted lists with same length
		final LongList sorted = LongList.of(1, 2, 3, 4);
		final LongList unsorted = LongList.of(4, 3, 2, 1);
		Assertions.assertFalse(sorted.equals(unsorted));
		Assertions.assertNotEquals(sorted.hashCode(), unsorted.hashCode());
	}

	@Test
	public void testSort() {
		final LongList list = new LongList();

		list.addAll(4, 2, 3, 1);
		list.sort();

		final long[] expected = new long[] { 1, 2, 3, 4 };
		Assertions.assertArrayEquals(expected, list.toArray());
	}

	@Test
	public void testSortParallel() {
		final LongList list = new LongList();

		list.addAll(4, 2, 3, 1);
		list.parallelSort();

		final long[] expected = new long[] { 1, 2, 3, 4 };
		Assertions.assertArrayEquals(expected, list.toArray());
	}

	@Test
	public void testShuffle() {
		final LongList list = LongList.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		final LongList original = list.clone();
		final LongList original2 = list.clone();

		final Random random = new Random(1);
		list.shuffle(random);

		Assertions.assertEquals(original.size(), list.size());

		original.removeAll(list);
		Assertions.assertTrue(original.isEmpty(), "original minus shuffled list: " + original);

		list.removeAll(original2);
		Assertions.assertTrue(list.isEmpty(), "shuffled list minus original: " + list);
	}

	@Test
	public void testShuffle_sortOfEmptyElementList() {
		final LongList list = LongList.of();
		list.shuffle();

		Assertions.assertTrue(list.isSorted(), "an empty list is sorted");
	}

	@Test
	public void testShuffle_sortOfOneElementList() {
		final LongList list = LongList.of(1);
		list.shuffle();

		Assertions.assertTrue(list.isSorted(), "a shuffled list of size 1 is sorted");
	}

	@Test
	public void testShuffle_sortOfListWithIdenticalElements() {
		final LongList list = LongList.of(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
		list.shuffle();

		Assertions.assertTrue(list.isSorted(), "a shuffled list with identical elements is sorted");
	}

	@Test
	public void testShuffle_sortOfThreeElementList() {
		final LongList list = LongList.of(2, 3, 1);

		int sorted = 0;
		int unsorted = 0;
		for (int i = 0; i < 20; i++) {
			final Random random = new Random(i);
			list.shuffle(random);

			sorted += LongList.of(1, 2, 3).equals(list) ? 1 : 0;
			unsorted += LongList.of(1, 2, 3).equals(list) ? 0 : 1;

			Assertions.assertEquals(LongList.of(1, 2, 3).equals(list), list.isSorted());
		}

		Assertions.assertTrue(sorted > 0);
		Assertions.assertTrue(unsorted > 0);
	}

	private long[] removeElements(final long[] data, final int... removedIndices) {
		final long[] result = new long[data.length - removedIndices.length];
		final List<Integer> blacklist = Arrays.stream(removedIndices).boxed().collect(Collectors.toList());

		int j = 0;
		for (int i = 0; i < data.length; i++) {
			if (!blacklist.contains(i)) {
				result[j] = data[i];
				j++;
			}
		}

		return result;
	}

	@Test
	public void testSerialize() throws IOException, ClassNotFoundException {
		final LongList emptyList = new LongList(0);
		internalSerializeTest(emptyList);

		final LongList emptyListWithCapacity = new LongList(10);
		internalSerializeTest(emptyListWithCapacity);

		final LongList threeElements = new LongList(10000);
		threeElements.addAll(1, 2, 3);
		internalSerializeTest(threeElements);

		final LongList trimmedList = new LongList(10000);
		trimmedList.addAll(1, 2, 3);
		trimmedList.trim();
		internalSerializeTest(trimmedList);
	}

	private void internalSerializeTest(final LongList list) throws IOException, ClassNotFoundException {
		final ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		try (final ObjectOutputStream out = new ObjectOutputStream(byteBuffer)) {
			out.writeObject(list);
		}

		try (ByteArrayInputStream byteInputStream = new ByteArrayInputStream(byteBuffer.toByteArray());
				ObjectInputStream in = new ObjectInputStream(byteInputStream)) {
			final LongList actual = (LongList) in.readObject();

			Assertions.assertEquals(list, actual);

			Assertions.assertEquals(list.size(), actual.getCapacity(),
					"The capacity of the deserialized list. Should be equal to the number of inserted "
							+ "values, because we don't want to serialize the unused part of the array");
		}
	}

	@Test
	public void testClone() {
		final LongList list = new LongList();
		list.addAll(1, 2);

		final LongList clone = list.clone();

		Assertions.assertEquals(list, clone);

		list.set(1, 0);
		Assertions.assertNotEquals(list, clone);
	}

	@Test
	public void testCloneEmptyList() {
		final LongList list = new LongList();

		final LongList clone = list.clone();
		Assertions.assertEquals(list, clone);
	}

	@Test
	public void testClonePreservesSortedFlagOnUnsortedList() {
		final LongList list = LongList.of(3, 2, 1);

		final LongList clone = list.clone();
		Assertions.assertEquals(list.isSorted(), clone.isSorted());
	}

	@Test
	public void testClonePreservesSortedFlagOnSortedList() {
		final LongList list = LongList.of(1, 2, 3);

		final LongList clone = list.clone();
		Assertions.assertEquals(list.isSorted(), clone.isSorted());
	}

	@Test
	public void testToString() {
		Assertions.assertEquals("[]", new LongList().toString());

		final LongList list = new LongList();
		list.addAll(-2, -1, 0, 1, 2);
		Assertions.assertEquals("[-2, -1, 0, 1, 2]", list.toString());
		Assertions.assertEquals(Arrays.toString(list.toArray()), list.toString(), "same result as Arrays.toString()");
	}

	@Test
	public void testSequentialStream() {

		{
			final LongList list = new LongList();
			list.addAll(0, 1, 2, 3, 4, 5, 6);
			final LongStream stream = list.stream();
			Assertions.assertEquals(list.size(), stream.count());
		}
		{
			final LongList list = new LongList();
			list.addAll(0, 1, 2, 3, 4, 5);
			final LongStream stream = list.stream();
			Assertions.assertEquals(15, stream.sum());
		}
		{
			final LongList emptyList = new LongList();
			Assertions.assertEquals(0, emptyList.stream().count());
		}
	}

	@Test
	public void testParallelStream() {
		final LongList list = new LongList();

		final int size = 1000;
		final long[] ints = new long[size];
		for (int i = 0; i < size; i++) {
			ints[i] = i;
		}

		list.addAll(ints);

		final ConcurrentLinkedQueue<Long> processingOrder = new ConcurrentLinkedQueue<>();
		final List<Long> actualList = list.parallelStream()//
				.peek(e -> processingOrder.add(e))//
				.boxed()//
				.collect(Collectors.toList());

		Assertions.assertEquals(actualList.toString(), list.toString(), "should be sequential, when using collect");
		Assertions.assertNotEquals(processingOrder.toString(), list.toString(),
				"should use parallelism during computation");
	}

	@Test
	public void testIndexOfOnSortedList() {
		final LongList list = new LongList();
		Assertions.assertEquals(-1, list.indexOf(0));

		list.add(1);
		Assertions.assertEquals(-1, list.indexOf(0));
		Assertions.assertEquals(0, list.indexOf(1));

		list.add(2);
		Assertions.assertEquals(-1, list.indexOf(0));
		Assertions.assertEquals(0, list.indexOf(1));
		Assertions.assertEquals(1, list.indexOf(2));
	}

	@Test
	public void testIndexOfOnSortedListReturnsFirstMatch() {
		final LongList list = LongList.of(0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4);
		Assertions.assertEquals(2, list.indexOf(2));
		Assertions.assertEquals(4, list.indexOf(2, 4));
	}

	@Test
	public void testIndexOfOnUnsortedList() {
		final LongList list = LongList.of(2, 0, 1);
		Assertions.assertEquals(1, list.indexOf(0));

		Assertions.assertEquals(0, list.indexOf(2));
		Assertions.assertEquals(2, list.indexOf(1));
		Assertions.assertEquals(-1, list.indexOf(3));
	}

	@Test
	public void testIndexOfWithOffsetOnSortedList() {
		final LongList list = new LongList();
		list.addAll(1, 1, 2, 3, 3);
		Assertions.assertEquals(0, list.indexOf(1, 0));
		Assertions.assertEquals(1, list.indexOf(1, 1));
		Assertions.assertEquals(2, list.indexOf(2, 2));
		Assertions.assertEquals(-1, list.indexOf(2, 3));
		Assertions.assertEquals(3, list.indexOf(3, 2));
	}

	@Test
	public void testIndexOfWithOffsetOnSortedListWithOffsetOutOfRange() {
		final LongList list = new LongList();
		list.addAll(1);
		Assertions.assertEquals(0, list.indexOf(1, -1));
		Assertions.assertEquals(-1, list.indexOf(1, list.size()));
	}

	@Test
	public void testIndexOfWithOffsetOnUnsortedList() {
		final LongList list = new LongList();
		list.addAll(0, 2, 0, 2);
		Assertions.assertEquals(1, list.indexOf(2, 0));
		Assertions.assertEquals(1, list.indexOf(2, 1));
		Assertions.assertEquals(3, list.indexOf(2, 2));
		Assertions.assertEquals(3, list.indexOf(2, 3));
		Assertions.assertEquals(-1, list.indexOf(2, 4));

		// indexed returned by indexOf() are consistent with get()
		Assertions.assertEquals(list.get(list.indexOf(2, 0)), 2);
		Assertions.assertEquals(list.get(list.indexOf(2, 1)), 2);
		Assertions.assertEquals(list.get(list.indexOf(2, 2)), 2);
		Assertions.assertEquals(list.get(list.indexOf(2, 3)), 2);
	}

	@Test
	public void testLastIndexOfWithOffsetOnEmptyList() {
		final LongList list = LongList.of();

		Assertions.assertEquals(-1, list.lastIndexOf(3, list.size()));
	}

	@Test
	public void testLastIndexOfWithOffsetOnSortedList() {
		final LongList list = LongList.of(1, 2, 2, 3, 4, 5, 6, 7, 8, 9);

		Assertions.assertEquals(3, list.lastIndexOf(3, list.size()));
		Assertions.assertEquals(2, list.lastIndexOf(2, list.size()));
		Assertions.assertEquals(0, list.lastIndexOf(1, list.size()));

		Assertions.assertEquals(2, list.lastIndexOf(2, 2)); // fromIndex == result
		Assertions.assertEquals(1, list.lastIndexOf(2, 1)); // fromIndex == result && the next element would be a hit

		Assertions.assertEquals(0, list.lastIndexOf(1, 0)); // fromIndex 0; found
		Assertions.assertEquals(-1, list.lastIndexOf(99, 0)); // fromIndex 0; not found
		Assertions.assertEquals(-1, list.lastIndexOf(99, list.size())); // fromIndex larger than list && not found
	}

	@Test
	public void testLastIndexOfOnSortedListReturnsFirstMatch() {
		final LongList list = LongList.of(0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4);
		Assertions.assertEquals(13, list.lastIndexOf(2));
		Assertions.assertEquals(10, list.lastIndexOf(2, 10));
	}

	@Test
	public void testLastIndexOfWithOffsetOnUnsortedList() {
		final LongList list = LongList.of(1, 2, 3, 1, 2, 3);

		Assertions.assertEquals(5, list.lastIndexOf(3, list.size()));
		Assertions.assertEquals(4, list.lastIndexOf(2, list.size()));
		Assertions.assertEquals(3, list.lastIndexOf(1, list.size()));
		Assertions.assertEquals(-1, list.lastIndexOf(99, list.size()));
		Assertions.assertEquals(-1, list.lastIndexOf(99, 0));

		Assertions.assertEquals(2, list.lastIndexOf(3, list.size() - 2));
		Assertions.assertEquals(4, list.lastIndexOf(2, list.size() - 1));
		Assertions.assertEquals(1, list.lastIndexOf(2, list.lastIndexOf(2) - 1));
	}

	@Test
	public void replaceAll() {
		final LongList list = new LongList();
		list.addAll(1, 2, 3);

		list.replaceAll(i -> i * 2);
		Assertions.assertArrayEquals(new long[] { 2, 4, 6 }, list.toArray());
	}

	@Test
	public void testReplaceAllOnEmptyList() {
		final LongList list = new LongList();

		list.replaceAll(i -> i * 3);
		Assertions.assertArrayEquals(new long[0], list.toArray());
	}

	@Test
	public void testRemoveAll() {
		final LongList list = LongList.of(-2, -1, 0, 1, 2, 3, 4, 5, 6);
		final LongList remove = LongList.of(-1, 2, 4, 5);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new long[] { -2, 0, 1, 3, 6 }, list.toArray());
		Assertions.assertArrayEquals(new long[] { -1, 2, 4, 5 }, remove.toArray());
	}

	@Test
	public void testRemoveEmptyList() {
		final LongList list = LongList.of(1, 2, 3, 4, 5, 6);
		final LongList remove = new LongList();

		list.removeAll(remove);
		Assertions.assertArrayEquals(new long[] { 1, 2, 3, 4, 5, 6 }, list.toArray());
		Assertions.assertArrayEquals(new long[] {}, remove.toArray());
	}

	@Test
	public void testRemoveAllFromEmptyList() {
		final LongList list = new LongList();
		final LongList remove = LongList.of(1);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new long[] {}, list.toArray());
		Assertions.assertEquals(0, list.size());
	}

	@Test
	public void testRemoveAllFromSortedList() {
		final LongList list = LongList.of(1, 2, 3, 4, 5);
		final LongList remove = LongList.of(1, 3);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new long[] { 2, 4, 5 }, list.toArray());
		Assertions.assertEquals(3, list.size());
		Assertions.assertTrue(list.isSorted());
	}

	@Test
	public void testRemoveAllFromSortedList2() {
		final LongList list = LongList.of(2, 4, 4, 6, 8);
		final LongList remove = LongList.of(1, 4, 9);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new long[] { 2, 6, 8 }, list.toArray());
		Assertions.assertEquals(3, list.size());
		Assertions.assertTrue(list.isSorted());
	}

	@Test
	public void testRetainAll() {
		final LongList list = LongList.of(-2, -1, 0, 1, 2, 3, 4, 5, 6);
		final LongList retain = LongList.of(-1, 2, 4, 5, 99);

		list.retainAll(retain);
		Assertions.assertArrayEquals(new long[] { -1, 2, 4, 5 }, list.toArray());
		Assertions.assertArrayEquals(new long[] { -1, 2, 4, 5, 99 }, retain.toArray());
	}

	@Test
	public void testRetainAllOnEmptyList() {
		final LongList list = LongList.of();
		final LongList retain = LongList.of(-1, 2, 4, 5, 99);

		list.retainAll(retain);
		Assertions.assertArrayEquals(new long[] {}, list.toArray());
		Assertions.assertArrayEquals(new long[] { -1, 2, 4, 5, 99 }, retain.toArray());
	}

	@Test
	public void testRetainAllFromEmptyList() {
		final LongList list = LongList.of(-2, -1, 0, 1, 2);
		final LongList retain = LongList.of();

		list.retainAll(retain);
		Assertions.assertArrayEquals(new long[] {}, list.toArray());
		Assertions.assertArrayEquals(new long[] {}, retain.toArray());
	}

	@Test
	public void testRemoveIf() {
		final LongList list = LongList.of(1, 2, 3, 4, 5, 6);

		list.removeIf((value, index) -> value % 2 == 0);
		Assertions.assertArrayEquals(new long[] { 1, 3, 5 }, list.toArray());
	}

	@Test
	public void testRemoveIfNegationOfPredicate() {
		final LongList list = LongList.of(1, 2, 3, 4, 5, 6);

		final LongPredicate predicate = (value, index) -> value % 2 == 0;
		list.removeIf(predicate.negate());
		Assertions.assertArrayEquals(new long[] { 2, 4, 6 }, list.toArray());
	}

	@Test
	public void testRemoveIfWithAndCombinedPredicates() {
		final LongList list = LongList.of(1, 2, 3, 4, 5, 6);

		final LongPredicate predicateA = (value, index) -> value % 2 == 0;
		final LongPredicate predicateB = (value, index) -> value == 3 || value == 4;
		list.removeIf(predicateA.and(predicateB));
		Assertions.assertArrayEquals(new long[] { 1, 2, 3, 5, 6 }, list.toArray());
	}

	@Test
	public void testRemoveIfWithOrCombinedPredicates() {
		final LongList list = LongList.of(1, 2, 3, 4, 5, 6);

		final LongPredicate predicateA = (value, index) -> value % 2 == 0;
		final LongPredicate predicateB = (value, index) -> value == 3 || value == 4;
		list.removeIf(predicateA.or(predicateB));
		Assertions.assertArrayEquals(new long[] { 1, 5 }, list.toArray());
	}

	@Test
	public void testRemoveIfOnEmptyList() {
		final LongList list = LongList.of();

		list.removeIf((value, index) -> false);
		Assertions.assertArrayEquals(new long[] {}, list.toArray());
	}

	@Test
	public void testSortedFlagSort() {

		Assertions.assertTrue(new LongList().isSorted(), "empty list is sorted");

		final LongList list = new LongList();
		list.addAll(2, 0, 1);
		list.sort();
		Assertions.assertTrue(list.isSorted(), "is sorted after calling sort");
	}

	@Test
	public void testSortedFlagEmptyList() {

		Assertions.assertTrue(new LongList().isSorted(), "empty list is sorted");

		final LongList list = new LongList();
		list.addAll(2, 0, 1);
		list.remove(0, list.size());
		Assertions.assertTrue(list.isSorted(), "unsorted list initialized by addAll");
	}

	@Test
	public void testSortedFlagAdd() {

		final LongList list = new LongList();
		list.add(1);
		Assertions.assertTrue(list.isSorted(), "[1]");
		list.add(2);
		Assertions.assertTrue(list.isSorted(), "[1,2]");
		list.add(2);
		Assertions.assertTrue(list.isSorted(), "[1,2,2]");
		list.add(1);
		Assertions.assertFalse(list.isSorted(), "[1,2,2,1]");
		list.add(1);
		Assertions.assertFalse(list.isSorted(), "[1,2,2,1,1]");
	}

	@Test
	public void testSortedFlagAddAll() {

		{
			final LongList list = new LongList();
			list.addAll(2, 0);
			Assertions.assertFalse(list.isSorted(), "unsorted list initialized by addAll");
		}

		{
			final LongList list = new LongList();
			list.addAll(1);
			Assertions.assertTrue(list.isSorted(), "list with one element is sorted");
		}

		{
			final LongList list = new LongList();
			list.addAll(1, 1);
			Assertions.assertTrue(list.isSorted(), "list with all the same elements is sorted");
		}
	}

	@Test
	public void testSortedFlagInsert() {

		/*
		 * tests that result in a sorted list
		 */
		{
			final LongList list = new LongList();
			list.insert(0, 1, 2, 3);
			Assertions.assertTrue(list.isSorted(), "insert sorted values into empty list");
		}

		{
			final LongList list = LongList.of(1, 1);
			list.insert(0, 2, 2); // -> [2,2,1,1]
			Assertions.assertFalse(list.isSorted(), "insert before: unsorted");
		}
		{
			final LongList list = LongList.of(3, 3);
			list.insert(0, 2, 2); // -> [2,2,3,3]
			Assertions.assertTrue(list.isSorted(), "insert sorted values before: sorted");
		}

		{
			final LongList list = LongList.of(4, 4);
			list.insert(2, 2, 2); // -> [4,4,2,2]
			Assertions.assertFalse(list.isSorted(), "insert sorted values at end: unsorted");
		}
		{
			final LongList list = LongList.of(1, 1);
			list.insert(2, 2, 2); // -> [1,1,2,2]
			Assertions.assertTrue(list.isSorted(), "insert sorted values at end: sorted");
		}

		{
			final LongList list = LongList.of(1, 4);
			list.insert(1, 2, 2); // -> [1,2,2,4]
			Assertions.assertTrue(list.isSorted(), "insert sorted values in middle: sorted");
		}

		/*
		 * tests that result in an unsorted list
		 */
		{
			final LongList list = LongList.of(1, 4);
			list.insert(1, 5); // -> [1,5,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}
		{
			final LongList list = LongList.of(1, 4);
			list.insert(1, 6, 6); // -> [1,6,6,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}

		{
			final LongList list = LongList.of(1, 4);
			list.insert(1, 0); // -> [1,0,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}
		{
			final LongList list = LongList.of(1, 4);
			list.insert(1, 0, 0); // -> [1,0,0,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}

		{
			final LongList list = LongList.of(3, 4);
			list.insert(0, 2, 1); // -> [2,1,3,4]
			Assertions.assertFalse(list.isSorted(), "insert unsorted at begin: unsorted");
		}
		{
			final LongList list = LongList.of(1, 4);
			list.insert(1, 2, 1); // -> [1,2,1,4]
			Assertions.assertFalse(list.isSorted(), "insert unsorted middle: unsorted");
		}
		{
			final LongList list = LongList.of(3, 4);
			list.insert(2, 6, 5); // -> [13,4,6,5]
			Assertions.assertFalse(list.isSorted(), "insert unsorted at end: unsorted");
		}

	}

	@Test
	public void testSortedFlagSetByIndex() {

		{
			final LongList list = LongList.of(0, 1, 2);
			list.set(0, -1);
			Assertions.assertTrue(list.isSorted(), "set first element: [-1,1,2] sorted");
			list.set(0, 1);
			Assertions.assertTrue(list.isSorted(), "set first element: [1,1,2] sorted");
			list.set(0, 2);
			Assertions.assertFalse(list.isSorted(), "set first element: [2,1,2] not sorted");
		}

		{
			final LongList sortedList = LongList.of(0, 2, 4);
			sortedList.set(1, 3);
			Assertions.assertTrue(sortedList.isSorted(), "set middle element: [0,3,4] sorteed");
			sortedList.set(1, 4);
			Assertions.assertTrue(sortedList.isSorted(), "set middle element: [0,4,4] sorteed");
			sortedList.set(1, 0);
			Assertions.assertTrue(sortedList.isSorted(), "set middle element: [0,0,4] sorteed");
			sortedList.set(1, 5);
			Assertions.assertFalse(sortedList.isSorted(), "set middle element: [0,5,4] not sorteed");
		}

		{
			final LongList sortedList = LongList.of(0, 1, 2);
			sortedList.set(2, 3);
			Assertions.assertTrue(sortedList.isSorted(), "set last element: [0,1,3] sorteed");
			sortedList.set(2, 1);
			Assertions.assertTrue(sortedList.isSorted(), "set last element: [0,1,1] sorteed");
			sortedList.set(2, 0);
			Assertions.assertFalse(sortedList.isSorted(), "set last element: [0,1,0] not sorteed");
		}

		{
			final LongList sortedList = LongList.of(0, 1, 2);
			sortedList.set(2, 0);
			sortedList.set(2, 2);
			Assertions.assertFalse(sortedList.isSorted(), "unsorted lists stay unsorted");
		}
	}

	@Test
	public void testSortedFlagRemove() {

		final LongList list = LongList.of(4, 3, 2, 1);
		list.remove(0, 2); // removes 4,3
		Assertions.assertFalse(list.isSorted(), "unsorted list with two elements is not sorted");

		list.remove(0, 1);
		Assertions.assertTrue(list.isSorted(), "unsorted list with one element becomes sorted");

		list.add(-1); // make list unsorted again
		list.remove(0, 2); // remove both elements
		Assertions.assertTrue(list.isSorted(), "unsorted list with no elements becomes sorted");
	}

	@Test
	public void testSortedFlagRemove_unsortedBecomesSorted_emptyList() {

		final LongList list = LongList.of(4, 3, 2, 1);
		list.remove(0, 4); // removes all
		Assertions.assertTrue(list.isSorted(), "empty list is sorted");
	}

	@Test
	public void testSortedFlagRemove_unsortedBecomesSorted_oneElement() {

		final LongList list = LongList.of(4, 3, 2, 1);
		list.remove(1, 4); // removes 3,2,1
		Assertions.assertTrue(list.isSorted(), "list with one element is");
	}

	@Test
	public void testSortedFlagRemove_unsortedBecomesSorted() {

		final LongList list = LongList.of(1, 2, 777, 4, 5);
		list.remove(2, 3); // removes 777
		Assertions.assertTrue(list.isSorted(), "list is sorted after remove");
	}

	@Test
	public void testSortedFlagRemoveAll() {

		final LongList list = LongList.of(4, 3, 2, 1);
		list.removeAll(LongList.of(4, 3));
		Assertions.assertFalse(list.isSorted(), "unsorted list with two elements is not sorted");

		list.removeAll(LongList.of(2));
		Assertions.assertTrue(list.isSorted(), "unsorted list with one element becomes sorted");

		list.add(-1); // make list unsorted again
		list.removeAll(LongList.of(-1, 2)); // remove both elements
		Assertions.assertTrue(list.isSorted(), "unsorted list with no elements becomes sorted");
	}

	@Test
	public void testSortedFlagRemoveIf() {

		final LongList list = LongList.of(4, 3, 2, 1);
		list.removeIf((value, index) -> value >= 3); // removes 3 and 4
		Assertions.assertFalse(list.isSorted(), "unsorted list with two elements is not sorted");

		list.removeIf((value, index) -> value >= 2); // removes 2
		Assertions.assertTrue(list.isSorted(), "unsorted list with one element becomes sorted");

		list.add(-1); // make list unsorted again
		list.removeIf((value, index) -> true); // remove both elements
		Assertions.assertTrue(list.isSorted(), "unsorted list with no elements becomes sorted");
	}

	@Test
	public void testSortedFlagRemoveIf_unsortedBecomesSorted_emptyAfterRemove() {

		final LongList list = LongList.of(1, 3, 2);
		list.removeIf((value, index) -> true); // makes the list sorted
		Assertions.assertTrue(list.isEmpty(), "list is empty");
		Assertions.assertTrue(list.isSorted(), "empty list is sorted");
	}

	@Test
	public void testSortedFlagRemoveIf_unsortedBecomesSorted_oneElementAfterRemove() {

		final LongList list = LongList.of(1, 3, 2);
		list.removeIf((value, index) -> value > 1); // makes the list sorted
		Assertions.assertTrue(list.isSorted(), "list with one element is sorted");
	}

	@Test
	public void testSortedFlagRemoveIf_unsortedBecomesSorted() {

		final LongList list = LongList.of(1, 2, 3, 777, 4, 5);
		list.removeIf((value, index) -> value == 777); // makes the list sorted
		Assertions.assertTrue(list.isSorted(), "unsorted list becomes sorted");
	}

	@Test
	public void testSortedFlagReplace() {

		final LongList list = LongList.of(1, 2, 3, 4);
		list.replaceAll(v -> v >= 3 ? 2 : v); // replace 3 and 4 with 2 -> [1,2,2,2]
		Assertions.assertTrue(list.isSorted(), "sorted list still sorted after replace");

		list.replaceAll(v -> v == 1 ? 3 : v); // replace 1 with 3 -> [3,2,2,2]
		Assertions.assertFalse(list.isSorted(), "sorted list becomes unsorted after replace");

		list.replaceAll(v -> 2); // replace all with 2 -> [2,2,2,2]
		Assertions.assertFalse(list.isSorted(), "unsorted list stays unsorted");
	}

	@Test
	public void testIntersectionSortedLists() {
		{
			final LongList a = LongList.of(0, 1, 2, 3, 4);
			final LongList b = LongList.of(2, 4, 5);
			final LongList actual = LongList.intersection(a, b);
			Assertions.assertEquals(LongList.of(2, 4), actual);
		}

		{
			final LongList a = LongList.of(0, 2, 4, 6);
			final LongList b = LongList.of(3, 5);
			final LongList actual = LongList.intersection(a, b);
			Assertions.assertEquals(LongList.of(), actual);
		}

		/*
		 * duplicate elements are removed in the result
		 */
		{
			final LongList a = LongList.of(3, 3, 3);
			final LongList b = LongList.of(3, 3);
			final LongList actual = LongList.intersection(a, b);
			Assertions.assertEquals(LongList.of(3), actual);
		}
		{
			final LongList a = LongList.of(4, 4);
			final LongList b = LongList.of(4, 4, 4);
			final LongList actual = LongList.intersection(a, b);
			Assertions.assertEquals(LongList.of(4), actual);
		}
	}

	@Test
	public void testIntersectionUnsortedLists() {
		{
			final LongList a = LongList.of(0, 1, 2, 3, 4);
			final LongList b = LongList.of(2, 4, 5);
			a.shuffle();
			b.shuffle();
			final LongList actual = LongList.intersection(a, b);
			actual.sort();
			Assertions.assertEquals(LongList.of(2, 4), actual);
		}

		/*
		 * duplicate elements are removed in the result
		 */
		{
			final LongList a = LongList.of(3, 5, 3, 3, 1);
			final LongList b = LongList.of(2, 3, 3);
			final LongList actual = LongList.intersection(a, b);
			Assertions.assertEquals(LongList.of(3), actual);
		}
		{
			final LongList a = LongList.of(1, 4);
			final LongList b = LongList.of(4, 3, 4, 4, 2);
			final LongList actual = LongList.intersection(a, b);
			Assertions.assertEquals(LongList.of(4), actual);
		}

		{
			final LongList a = LongList.of(1, 4, 3, 2, 4);
			final LongList b = LongList.of(4, 3, 4, 4, 2);
			final LongList actual = LongList.intersection(a, b);
			actual.sort();
			Assertions.assertEquals(LongList.of(2, 3, 4), actual);
		}
	}

	@Test
	public void testUnionSortedLists_emptyLists() {
		final LongList a = LongList.of();
		final LongList b = LongList.of();

		Assertions.assertEquals(LongList.of(), LongList.union(a, b));
		Assertions.assertEquals(LongList.union(a, b), LongList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_uniqueValues() {
		final LongList a = LongList.of(0, 1, 3, 4);
		final LongList b = LongList.of(2, 4, 5);

		final LongList actual = LongList.union(a, b);
		Assertions.assertEquals(LongList.of(0, 1, 2, 3, 4, 5), actual);
		Assertions.assertEquals(LongList.union(a, b), LongList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_duplicateValues_inMiddleOfListA() {
		final LongList a = LongList.of(1, 2, 2, 3);
		final LongList b = LongList.of(1, 3);

		final LongList actual = LongList.union(a, b);
		Assertions.assertEquals(LongList.of(1, 2, 3), actual);
		Assertions.assertEquals(LongList.union(a, b), LongList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_duplicateValues_inMiddleOfBothLists() {
		final LongList a = LongList.of(1, 2, 2, 3);
		final LongList b = LongList.of(1, 2, 2, 4);

		final LongList actual = LongList.union(a, b);
		Assertions.assertEquals(LongList.of(1, 2, 3, 4), actual);
		Assertions.assertEquals(LongList.union(a, b), LongList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_duplicateValues_atEndOfListA_whenHighestValueInBIsSmaller() {
		final LongList a = LongList.of();
		final LongList b = LongList.of(2, 2);

		final LongList actual = LongList.union(a, b);
		Assertions.assertEquals(LongList.of(2), actual);
		Assertions.assertEquals(LongList.union(a, b), LongList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_three() {
		final LongList a = LongList.of(1, 2, 3);
		final LongList b = LongList.of(2, 4, 6);
		final LongList c = LongList.of(3, 5, 7);

		final LongList actual = LongList.union(a, b, c);
		Assertions.assertEquals(LongList.of(1, 2, 3, 4, 5, 6, 7), actual);
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union(b, c, a));
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union(b, a, c));
	}

	@Test
	public void testUnionSortedLists_four_LongMinValue() {
		final LongList a = LongList.of(Long.MIN_VALUE, 2, 3, Long.MAX_VALUE);
		final LongList b = LongList.of(2, 4, 6, Long.MAX_VALUE);
		final LongList c = LongList.of(Long.MIN_VALUE, 5, 7);
		final LongList d = LongList.of(Long.MIN_VALUE, Long.MIN_VALUE);

		final LongList actual = LongList.union(a, b, c, d);
		Assertions.assertEquals(LongList.of(Long.MIN_VALUE, 2, 3, 4, 5, 6, 7, Long.MAX_VALUE), actual);
		Assertions.assertEquals(LongList.union(a, b, c, d), LongList.union(b, c, a, d));
		Assertions.assertEquals(LongList.union(a, b, c, d), LongList.union(d, b, a, c));
	}

	@Test
	public void testUnionSortedLists_Concatenating_with_empty_list() {
		// aims to use the ListConcatenater in LongList.union()
		// that means we need at least three lists and they must all be non-overlapping
		final LongList a = LongList.of(1, 2, 3);
		final LongList b = LongList.of();
		final LongList c = LongList.of(10,11);

		final LongList actual = LongList.union(a, b, c);
		Assertions.assertEquals(LongList.of(1,2,3,10,11), actual);
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union(b, c, a));
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union( b, a, c));
	}
	
	@Test
	public void testUnionSortedLists_Concatenating_results_in_one_list() {
		// aims to use the ListConcatenater in LongList.union()
		// that means we need at least three lists and they must all be non-overlapping
		final LongList a = LongList.of(1, 2, 3);
		final LongList b = LongList.of(4);
		final LongList c = LongList.of(10,11);

		final LongList actual = LongList.union(a, b, c);
		Assertions.assertEquals(LongList.of(1,2,3,4,10,11), actual);
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union(b, c, a));
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union( b, a, c));
	}

	@Test
	public void testUnionSortedLists_Concatenating_results_in_two_lists() {
		// aims to use the ListConcatenater in LongList.union()
		// that means we need at least three lists
		final LongList a = LongList.of(1, 2, 3);
		final LongList b = LongList.of(3, 4);
		final LongList c = LongList.of(4, 10,11); // can be concatenated to a

		final LongList actual = LongList.union(a, b, c);
		Assertions.assertEquals(LongList.of(1,2,3,4,10,11), actual);
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union(b, c, a));
		Assertions.assertEquals(LongList.union(a, b, c), LongList.union( b, a, c));
	}
	
	@Test
	public void testUnionSortedLists_Multiway_merge () {
		// aims to use the MultiwayLongMerge 
		// that means we need overlapping lists so that ListConcatenater will return at least six lists
		// this is done by adding 1 to all six lists
		final LongList a = LongList.of(1, 2, 3);
		final LongList b = LongList.of(1, 3, 4);
		final LongList c = LongList.of(1, 4, 10,11);
		final LongList d = LongList.of(1, 6,9);
		final LongList e = LongList.of(1, 123, 144);
		final LongList f = LongList.of(1, 411, 1011,1111);  

		final LongList actual = LongList.union(a, b, c,d,e,f);
		Assertions.assertEquals(LongList.of(1,2,3,4,6,9,10,11,123,144,411,1011,1111), actual);
		Assertions.assertEquals(LongList.union(a, b, c,d,e,f), LongList.union(b, c, a,d,e,f));
		Assertions.assertEquals(LongList.union(a, b, c,d,e,f), LongList.union( b, d,f,a,e, c));
	}

	@Test
	public void testUnionSortedLists_ten_lists_fifteen_elements_random() {
		testUnionSortedLists(10, 15, true);
	}

	@Test
	public void testUnionSortedLists_ten_lists_fifteen_elements_equal() {
		testUnionSortedLists(10, 15, false);
	}

	private void testUnionSortedLists(int numLists, int values, boolean random) {
		ThreadLocalRandom rng = ThreadLocalRandom.current();
		List<LongList> longSorted = new ArrayList<>();
		for (int i = 0; i < numLists; i++) {
			LongList list = new LongList(values);
			if (random) {
				for (int j = 0; j < values; j++) {
					list.add(rng.nextLong());
				}
				list.sort();
			} else {
				LongStream.range(0, values).forEachOrdered(list::add);
			}
			longSorted.add(list);
		}

		final LongList actual = LongList.union(longSorted);

		final LongList concatenatedList = new LongList();
		concatenatedList.addAll(longSorted);
		final LongList expected = LongList.union(concatenatedList, LongList.of());
		Assertions.assertEquals(expected, actual);

		Collections.shuffle(longSorted);
		final LongList unionShuffled1 = LongList.union(longSorted);
		Collections.shuffle(longSorted);
		final LongList unionShuffled2 = LongList.union(longSorted);
		Assertions.assertEquals(unionShuffled1, unionShuffled2);
	}

	@Test
	public void testUnionUnsortedLists() {
		final LongList a = LongList.of(1, 0, 3, 4);
		final LongList b = LongList.of(2, 5, 4);

		final LongList actual = LongList.union(a, b);
		Assertions.assertEquals(LongList.of(0, 1, 2, 3, 4, 5), actual);
		Assertions.assertEquals(LongList.union(a, b), LongList.union(b, a));
	}

	@Test
	public void testUnionUnsortedLists_oneListIsSorted() {
		final LongList a = LongList.of(1, 2, 3);
		final LongList b = LongList.of(2, 5, 4);

		final LongList actual = LongList.union(a, b);
		Assertions.assertEquals(LongList.of(1, 2, 3, 4, 5), actual);
		Assertions.assertEquals(LongList.union(a, b), LongList.union(b, a));
	}

	@Test
	public void testUniq_sorted() {
		final LongList sorted = LongList.of(1, 1, 2, 3, 4, 4, 4);
		final LongList expected = LongList.of(1, 2, 3, 4);

		sorted.uniq();
		Assertions.assertEquals(expected, sorted);
	}

	@Test
	public void testUniq_empty() {
		final LongList empty = LongList.of();
		final LongList expected = LongList.of();

		empty.uniq();
		Assertions.assertEquals(expected, empty);
	}

	@Test
	public void testUniq_oneElement() {
		final LongList list = LongList.of(1);
		final LongList expected = LongList.of(1);

		list.uniq();
		Assertions.assertEquals(expected, list);
	}

	@Test
	public void testUniq_unsorted() {
		final LongList unsorted = LongList.of(1, 1, 2, 3, 4, 4, 4);
		unsorted.shuffle();
		final LongList expected = LongList.of(1, 2, 3, 4);

		unsorted.uniq();
		Assertions.assertEquals(expected, unsorted);
	}
}
