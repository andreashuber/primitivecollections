package org.lucares.collections;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Random;
import java.util.stream.LongStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LongLongHashMapTest {
	@Test
	public void testPutRemove() {
		putGetRemove(1);
	}

	@Test
	public void testNullValue() {
		putGetRemove(0);
	}
	
	@Test
	public void testRemovedValue() {
		putGetRemove(-1);
	}

	private void putGetRemove(final long key) {
		final LongLongHashMap map = new LongLongHashMap();

		final long valueA = 2L;
		final long valueB = 3L;

		// value does not exist
		Assertions.assertFalse(map.containsKey(key));
		Assertions.assertEquals(0, map.size());

		// add value and check it is in the map
		map.put(key, valueA);
		Assertions.assertTrue(map.containsKey(key));
		Assertions.assertEquals(valueA, map.get(key, Long.MIN_VALUE));
		Assertions.assertEquals(1, map.size());

		// overwrite value
		map.put(key, valueB);
		Assertions.assertEquals(valueB, map.get(key, Long.MIN_VALUE));
		Assertions.assertEquals(1, map.size());

		// remove value and check it is gone
		map.remove(key);
		Assertions.assertFalse(map.containsKey(key));
		Assertions.assertEquals(0, map.size());
	}

	@Test
	public void testRemoveNonExistingKey() {
		final LongLongHashMap map = new LongLongHashMap();
		final int values = 100;

		 // fill with keys 0...99
		for (int i = 0; i < values; i++) {
			map.put(i, i);
		}

		map.remove(values); // key does not exist
		Assertions.assertEquals(values, map.size(), "size after removing non existing key 100");

		// -1 is a sentinel key and has special handling
		map.remove(-1); // key does not exist
		Assertions.assertEquals(values, map.size(), "size after removing non existing key -1");
		
		map.put(-1, -1);
		Assertions.assertEquals(values+1, map.size(), "size after adding key -1");
		
		map.remove(-1); // key exists
		Assertions.assertEquals(values, map.size(), "size after removing key -1");

		// 0 is a sentinel key and has special handling
		map.remove(0); // key exists
		Assertions.assertEquals(values - 1, map.size(), "size after removing existing key 0");

		map.remove(0); // key does not exist
		Assertions.assertEquals(values - 1, map.size(), "size after removing non existing key 0 (2nd removal of 0)");

		for (int i = 1; i < 100; i++) {
			map.remove(i);
			Assertions.assertEquals(values-i-1, map.size(), "size after removing key "+i);
		}
		Assertions.assertEquals(0, map.size(), "size after removing all keys");
	}

	@Test
	public void testRemoveAllValuesOneByOne() {
		int values = 20;
		final LongLongHashMap map = new LongLongHashMap();
		Random r = new Random(123);

		LongList keys = new LongList();

		for (int round = 0; round < 5; round++) {
			values *= 2;
			keys.clear();
			keys.addAll(r.longs(values).toArray());
			keys.shuffle(r);
			keys.stream().forEach(l -> map.put(l, 2));

			for (int i = values - 1; i >= 0; i--) {
				long key = keys.get(i);

				map.compute(key, 2, (k,v) -> v * 2);
				Assertions.assertEquals(4, map.get(key, -1), "value for key " + key + "=4 - map=" + map);

				Assertions.assertTrue(map.containsKey(key), "map contains key " + key);
				map.remove(key);
				Assertions.assertEquals(i, map.size(), "size after removing key " + key);
			}
		}
	}


	@Test
	public void testCompute() {		
		final LongLongHashMap map = new LongLongHashMap();
		
		// initialize values
		map.compute(LongLongHashMap.REMOVED_KEY, 11L, (k,v) -> {
			Assertions.assertEquals(LongLongHashMap.REMOVED_KEY, k);
			Assertions.assertEquals(11, v);
			return 12L;
		});
		Assertions.assertEquals(12, map.get(LongLongHashMap.REMOVED_KEY, -111),
				"initialValueIfAbsent is used when there is no mapping for the key");
		
		map.compute(LongLongHashMap.NULL_KEY, 21L, (k,v) -> {
			Assertions.assertEquals(LongLongHashMap.NULL_KEY, k);
			Assertions.assertEquals(21, v);
			return 22L;
		});
		Assertions.assertEquals(22, map.get(LongLongHashMap.NULL_KEY, -111),
				"initialValueIfAbsent is used when there is no mapping for the key");
		
		map.compute(1, 31L, (k,v) -> {
			Assertions.assertEquals(1, k);
			Assertions.assertEquals(31, v);
			return 32L;
		});
		Assertions.assertEquals(32, map.get(1, -111),
				"initialValueIfAbsent is used when there is no mapping for the key");
		
		// update the value
		map.compute(LongLongHashMap.REMOVED_KEY, -123L, (k,v) -> {
			Assertions.assertEquals(LongLongHashMap.REMOVED_KEY, k);
			Assertions.assertEquals(12, v);
			return 13L;
		});
		Assertions.assertEquals(13, map.get(LongLongHashMap.REMOVED_KEY, -111), "update function is called when key is set");
		
		map.compute(LongLongHashMap.NULL_KEY, -123L, (k,v) -> {
			Assertions.assertEquals(LongLongHashMap.NULL_KEY, k);
			Assertions.assertEquals(22, v);
			return 23L;
		});
		Assertions.assertEquals(23, map.get(LongLongHashMap.NULL_KEY, -111), "update function is called when key is set");
		
		map.compute(1, -123L, (k,v) -> {
			Assertions.assertEquals(1, k);
			Assertions.assertEquals(32, v);
			return 33L;
		});
		Assertions.assertEquals(33, map.get(1, -111), "update function is called when key is set");
	}

	@Test
	public void testGrowMap() {
		final LongLongHashMap map = new LongLongHashMap(4, 0.75);

		final int numEntries = 12;
		final Random rand = new Random(12345);
		final LongList entries = LongList.of(LongStream.generate(rand::nextLong).limit(numEntries).toArray());

		entries.stream().forEachOrdered(l -> {
			map.put(l, l);
		});
		entries.stream().forEachOrdered(l -> {
			Assertions.assertEquals(l, map.get(l, Long.MIN_VALUE));
		});
		Assertions.assertEquals(16, map.getCapacity(), "capacity after adding 12 entries must be a the smallest number "
				+ "that satisfies initialCapacity * 2^n >= entries/fillFactor");
	}

	@Test
	public void testMultipleValuesOnSamePosition() {
		final int initialCapacity = 20;
		final LongLongHashMap map = new LongLongHashMap(initialCapacity, 0.75);
		// find to values that yield the same 'spread' (position in the table)
		final LongList keysWithSameSpread = findKeysWithSameSpread(map);
		Assertions.assertTrue(keysWithSameSpread.size() > 5);

		keysWithSameSpread.stream().forEach(l -> map.put(l, l));
		Assertions.assertEquals(keysWithSameSpread.size(), map.size());
		keysWithSameSpread.stream().forEach(l -> Assertions.assertEquals(l, map.get(l, Long.MIN_VALUE)));

		// remove the keys
		keysWithSameSpread.shuffle();
		keysWithSameSpread.stream().forEach(k -> {
			int sizeBefore = map.size();
			Assertions.assertTrue(map.containsKey(k));
			map.remove(k);
			Assertions.assertFalse(map.containsKey(k));
			Assertions.assertEquals(Long.MIN_VALUE, map.get(k, Long.MIN_VALUE));
			Assertions.assertEquals(sizeBefore - 1, map.size());
		});
	}
	
	@Test
	public void testMultipleValuesOnSamePosition2() {
		final LongLongHashMap map = new LongLongHashMap();
		// find to values that yield the same 'spread' (position in the table)
		final LongList keys = findKeysWithSameSpread(map);
		Assertions.assertTrue(keys.size() > 5);

		map.put(keys.get(0), 1);
		map.put(keys.get(1), 1);
		map.put(keys.get(2), 1);
		
		// creates a section of the array that looks like this: k0,-1,k2, where -1 marks a previously occupied slot
		map.remove(keys.get(1));
		
		// should overwrite the existing value which is after a slot that is marked as previously occupied
		map.put(keys.get(2), 2); 
		
		final LongList values=new LongList();
		map.forEach((k,v) -> values.add(v));
		Assertions.assertEquals(LongList.of(1,2), values);
	}
	

	@Test
	public void testForEach() {
		final LongLongHashMap map = new LongLongHashMap();
		final Random rand = new Random(6789);
		final LongList entries = LongList.of(LongStream.generate(rand::nextLong).limit(15).toArray());

		entries.stream().forEach(l -> {
			map.put(l, 2 * l);
		});

		map.forEach((k, v) -> {
			Assertions.assertEquals(k * 2, v, "value is key*2");
			Assertions.assertTrue(entries.indexOf(k) >= 0, "value " + k + " in entries: " + entries);
		});
	}

	@Test
	public void testForEachWithSpecialValues() {
		final LongLongHashMap map = new LongLongHashMap();
		final Random rand = new Random(6789);
		final LongList entries = LongList.of(LongStream.generate(rand::nextLong).limit(15).toArray());
		entries.add(0); // special key that is internally used to mark unset slots
		entries.add(-1);// special key that is internally used to mark slots with removed values
		entries.add(123); // value that will be removed later

		entries.stream().forEach(l -> {
			map.put(l, 2 * l);
		});
		map.remove(123);

		map.forEach((k, v) -> {
			Assertions.assertEquals(k * 2, v, "value is key*2");
			Assertions.assertTrue(entries.indexOf(k) >= 0, "value " + k + " in entries: " + entries);
		});
	}

	@Test
	public void testForEachOrdered() {
		final LongLongHashMap map = new LongLongHashMap();
		final LongList entries = LongList.of(-10, -9, -1, 0, 1, 2, 10);

		entries.stream().forEachOrdered(l -> {
			map.put(l, 2 * l);
		});

		final LongList actualOrderOfKeys = new LongList();
		map.forEachOrdered((k, v) -> {
			Assertions.assertEquals(k * 2, v, "value is key*2");
			Assertions.assertTrue(entries.indexOf(k) >= 0, "value " + k + " in entries: " + entries);
			actualOrderOfKeys.add(k);
		});

		Assertions.assertTrue(actualOrderOfKeys.isSorted(), "keys are sorted");
		Assertions.assertEquals(LongList.intersection(actualOrderOfKeys, entries).size(), entries.size(),
				"all keys were visited");
		final LongList additionalKeys = new LongList(actualOrderOfKeys);
		additionalKeys.removeAll(entries);
		Assertions.assertEquals(additionalKeys, LongList.of(), "no additional keys were visited");
	}

	@Test
	public void testForEachOrderedOnlyNegativeValues() {
		final LongLongHashMap map = new LongLongHashMap();
		final LongList entries = LongList.of(LongStream.range(-20, -5).toArray());

		entries.stream().forEachOrdered(l -> {
			map.put(l, 2 * l);
		});

		final LongList actualOrderOfKeys = new LongList();
		map.forEachOrdered((k, v) -> {
			Assertions.assertEquals(k * 2, v, "value is key*2");
			Assertions.assertTrue(entries.indexOf(k) >= 0, "value " + k + " in entries: " + entries);
			actualOrderOfKeys.add(k);
		});

		Assertions.assertTrue(actualOrderOfKeys.isSorted(), "keys are sorted");
		Assertions.assertEquals(LongList.intersection(actualOrderOfKeys, entries).size(), entries.size(),
				"all keys were visited");
		final LongList additionalKeys = new LongList(actualOrderOfKeys);
		additionalKeys.removeAll(entries);
		Assertions.assertEquals(additionalKeys, LongList.of(), "no additional keys were visited");
	}

	@Test
	public void testForEachOrderedOnlyNegativeValues2() {
		final LongLongHashMap map = new LongLongHashMap();
		final LongList entries = LongList.of(LongStream.range(-20, -5).toArray());

		entries.stream().forEachOrdered(l -> {
			map.put(l, 2 * l);
		});

		final LongList actualOrderOfKeys = new LongList();
		map.forEachOrdered((k, v) -> {
			Assertions.assertEquals(k * 2, v, "value is key*2");
			Assertions.assertTrue(entries.indexOf(k) >= 0, "value " + k + " in entries: " + entries);
			actualOrderOfKeys.add(k);
		});

		Assertions.assertTrue(actualOrderOfKeys.isSorted(), "keys are sorted");
	}

	@Test
	public void testFindPositionOfFirstPositiveKey() {

		Assertions.assertEquals(-1, LongLongHashMap.findPosOfFirstPositiveKey(new long[] {}));
		Assertions.assertEquals(-1, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0 }));
		Assertions.assertEquals(0, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 1 }));
		Assertions.assertEquals(1, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 1 }));
		Assertions.assertEquals(0, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 1, 1 }));
		Assertions.assertEquals(2, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, 0, 1 }));
		Assertions.assertEquals(0, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 }));
		Assertions.assertEquals(0, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 1, 1, 1, 1, 1, 1, 1, 1 }));
		Assertions.assertEquals(4,
				LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, -1, -1, -1, 1, 1, 1, 1, 1 }));
		Assertions.assertEquals(4,
				LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, -1, -1, -1, 1, 1, 1, 1 }));
		Assertions.assertEquals(3,
				LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, -1, -1, 1, 1, 1, 1, 1, 1 }));
		Assertions.assertEquals(3, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, -1, -1, 1, 1, 1, 1, 1 }));
		Assertions.assertEquals(-1, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 0 }));
		Assertions.assertEquals(-1, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 0, 0 }));
		Assertions.assertEquals(4, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 1, 1, 1, 1 }));
		Assertions.assertEquals(5, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 0, 1, 1, 1 }));
		Assertions.assertEquals(6, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 0, 0, 1, 1 }));
		Assertions.assertEquals(4, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 1, 1, 1 }));
		Assertions.assertEquals(5, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 0, 1, 1 }));
		Assertions.assertEquals(6, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { 0, 0, 0, 0, 0, 0, 1 }));
		Assertions.assertEquals(4, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, 0, 0, 0, 1, 1, 1 }));
		Assertions.assertEquals(5, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, 0, 0, 0, 0, 1, 1 }));
		Assertions.assertEquals(6, LongLongHashMap.findPosOfFirstPositiveKey(new long[] { -1, 0, 0, 0, 0, 0, 1 }));
	}
	
	@Test
	public void testMaxCapacity() {
		LongLongHashMap map = new LongLongHashMap(6,0.75);
		Assertions.assertEquals(6, map.getCapacity());
		
		// capacity is reduced to 5 - possible, because map is empty 
		map.setMaxCapacity(5);
		Assertions.assertEquals(5, map.getCapacity());
		
		map.put(1, 0); 
		map.put(2, 0);
		map.put(3, 0);
		map.put(4, 0);
		map.put(5, 0);
		Assertions.assertEquals(5, map.getCapacity());
		Assertions.assertEquals(5, map.size());
		
		// ensure we cannot add more values than the capacity allows
		// 0 and -1 are sentinels, we have to check them separately
		assertThrows(IllegalStateException.class, () -> map.put(0, 55));
		assertThrows(IllegalStateException.class, () -> map.put(-1, 55));
		assertThrows(IllegalStateException.class, () -> map.put(6, 55));// key is negative to ensure we actually could add it if the capacity restriction was not there
		
		Assertions.assertEquals(5, map.size()); // we still have only 5 keys in the map
		
		// check that we can increase the maxCapacity
		map.setMaxCapacity(map.getCapacity()+1);
		Assertions.assertEquals(5, map.getCapacity()); // capacity was not updated, because there was not need - you would have to manually call rehash()
		Assertions.assertEquals(5, map.size());
		map.put(6, 0); 
		assertThrows(IllegalStateException.class, () -> map.put(7, 55));
		
		// check we cannot make the capacity smaller than the current size
		assertThrows(IllegalArgumentException.class, ()->map.setMaxCapacity(map.size()-1));
	}

	private LongList findKeysWithSameSpread(final LongLongHashMap map) {
		final LongList result = new LongList();
		final int spread = map.spread(1);
		result.add(1);
		for (long l = 2; l < 10000; l++) {
			final int s = map.spread(l);
			if (s == spread) {
				result.add(l);
				if (result.size() > 10) {
					break;
				}
			}
		}

		return result;
	}
}
