package org.lucares.collections;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class IntListTest {

	@Test
	public void testEmptyList() {
		final IntList list = new IntList();

		Assertions.assertEquals(true, list.isEmpty());
	}

	@Test
	public void testCopyConstructor() {
		final IntList list = new IntList();
		list.addAll(1, 2, 3, 4, 5);

		final IntList copy = new IntList(list);

		Assertions.assertArrayEquals(copy.toArray(), list.toArray());
	}

	@Test
	public void testInvalidInitialCapacity() {
		try {
			new IntList(-1);
			Assertions.fail();
		} catch (final IllegalArgumentException e) {
			// expected
		}
	}

	@Test
	public void testRange() {
		final IntList expected = IntList.of(1, 2, 3, 4, 5);
		final IntList list = IntList.range(1, 6);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(5, list.getCapacity());
	}

	@Test
	public void testRangeWithNegativeValues() {
		final IntList expected = IntList.of(Integer.MIN_VALUE, Integer.MIN_VALUE + 1, Integer.MIN_VALUE + 2);
		final IntList list = IntList.range(Integer.MIN_VALUE, Integer.MIN_VALUE + 3);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(3, list.getCapacity());
	}

	@Test
	public void testEmptyRange() {
		final IntList expected = IntList.of();
		final IntList list = IntList.range(1, 1);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(0, list.getCapacity());
	}

	@Test
	public void testNegativeRange() {
		final IntList expected = IntList.of();
		final IntList list = IntList.range(1, 0);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(0, list.getCapacity());
	}

	@Test
	public void testRangeClosed() {
		final IntList expected = IntList.of(1, 2, 3, 4, 5);
		final IntList list = IntList.rangeClosed(1, 5);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(5, list.getCapacity());
	}

	@Test
	public void testRangeClosedWithNegativeValues() {
		final IntList expected = IntList.of(Integer.MIN_VALUE, Integer.MIN_VALUE + 1, Integer.MIN_VALUE + 2);
		final IntList list = IntList.rangeClosed(Integer.MIN_VALUE, Integer.MIN_VALUE + 2);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(3, list.getCapacity());
	}

	@Test
	public void testOneElementRangeClosed() {
		final IntList expected = IntList.of(1);
		final IntList list = IntList.rangeClosed(1, 1);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
		Assertions.assertEquals(1, list.getCapacity());
	}

	@Test
	public void testEmptyRangeClosed() {
		final IntList expected = IntList.of();
		final IntList list = IntList.rangeClosed(1, 0);
		Assertions.assertArrayEquals(expected.toArray(), list.toArray());
	}

	@Test
	public void testAdd() {
		// setting initial size to one, so that the first add does not need to resize,
		// but the second add must
		final IntList list = new IntList(1);

		list.add(1);

		Assertions.assertFalse(list.isEmpty());
		Assertions.assertEquals(1, list.size());

		list.add(2);
		Assertions.assertEquals(2, list.size());

		Assertions.assertEquals(1, list.get(0));
		Assertions.assertEquals(2, list.get(1));
	}

	@Test
	public void testInsert() {
		final IntList list = new IntList();

		list.insert(0);
		Assertions.assertArrayEquals(new int[] {}, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(0, 1);
		Assertions.assertArrayEquals(new int[] { 1 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, 2, 2, 2);
		Assertions.assertArrayEquals(new int[] { 1, 2, 2, 2 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, 3);
		Assertions.assertArrayEquals(new int[] { 1, 3, 2, 2, 2 }, list.toArray());
		Assertions.assertFalse(list.isSorted());

		list.insert(2, 4, 4, 4);
		Assertions.assertArrayEquals(new int[] { 1, 3, 4, 4, 4, 2, 2, 2 }, list.toArray());
		Assertions.assertFalse(list.isSorted());

		list.insert(2);
		Assertions.assertArrayEquals(new int[] { 1, 3, 4, 4, 4, 2, 2, 2 }, list.toArray());
		Assertions.assertFalse(list.isSorted());

		list.insert(8, 5, 5);
		Assertions.assertArrayEquals(new int[] { 1, 3, 4, 4, 4, 2, 2, 2, 5, 5 }, list.toArray());
		Assertions.assertFalse(list.isSorted());
	}

	@Test
	public void testInsertWithFlexibleApi() {
		final IntList list = new IntList();

		list.insert(0, new int[] {}, 0, 0);
		Assertions.assertArrayEquals(new int[] {}, list.toArray());

		list.insert(0, new int[] { 1, 3 }, 0, 2);
		Assertions.assertArrayEquals(new int[] { 1, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, new int[] { -1, 2, -1 }, 1, 1); // only the 2 will be inserted
		Assertions.assertArrayEquals(new int[] { 1, 2, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(2, new int[] { 7, 8 }, 0, 2);
		Assertions.assertArrayEquals(new int[] { 1, 2, 7, 8, 3 }, list.toArray());
		Assertions.assertFalse(list.isSorted());
	}

	@Test
	public void testInsertIntListWithFlexibleApi() {
		final IntList list = new IntList();

		list.insert(0, IntList.of(), 0, 0);
		Assertions.assertArrayEquals(new int[] {}, list.toArray());

		list.insert(0, IntList.of(1, 3), 0, 2);
		Assertions.assertArrayEquals(new int[] { 1, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, IntList.of(-1, 2, -1), 1, 1); // only the 2 will be inserted
		Assertions.assertArrayEquals(new int[] { 1, 2, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(2, IntList.of(7, 8), 0, 2);
		Assertions.assertArrayEquals(new int[] { 1, 2, 7, 8, 3 }, list.toArray());
		Assertions.assertFalse(list.isSorted());
	}

	@Test
	public void testInsertIntListWithSimpleApi() {
		final IntList list = new IntList();

		list.insert(0, IntList.of());
		Assertions.assertArrayEquals(new int[] {}, list.toArray());

		list.insert(0, IntList.of(1, 3));
		Assertions.assertArrayEquals(new int[] { 1, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(1, IntList.of(2));
		Assertions.assertArrayEquals(new int[] { 1, 2, 3 }, list.toArray());
		Assertions.assertTrue(list.isSorted());

		list.insert(3, IntList.of(6, 5));
		Assertions.assertArrayEquals(new int[] { 1, 2, 3, 6, 5 }, list.toArray());
		Assertions.assertFalse(list.isSorted());
	}

	@Test
	public void testInsertOutOfBounds() {
		final IntList list = new IntList();
		try {
			list.insert(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		try {
			list.insert(1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		try {
			list.add(1);
			list.insert(2, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			// sourcePos is out of range
			list.insert(1, IntList.of(1), 1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			// length is out of range
			list.insert(1, IntList.of(1), 0, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			// sourcePod+length is out of range
			list.insert(1, IntList.of(1, 2, 3), 2, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testInsertNullArray() {
		final IntList list = new IntList();
		try {
			list.insert(0, (int[]) null);
			Assertions.fail();
		} catch (final NullPointerException e) {
			// expected
		}
	}

	@Test
	public void testInsertNullList() {
		final IntList list = new IntList();
		try {
			list.insert(0, (IntList) null);
			Assertions.fail();
		} catch (final NullPointerException e) {
			// expected
		}
	}

	@Test
	public void testSet() {
		final IntList list = new IntList();
		list.addAll(0, 1, 2, 3, 4, 5);

		list.set(0, 10);
		Assertions.assertArrayEquals(new int[] { 10, 1, 2, 3, 4, 5 }, list.toArray());

		list.set(1, 11);
		Assertions.assertArrayEquals(new int[] { 10, 11, 2, 3, 4, 5 }, list.toArray());

		list.set(5, 55);
		Assertions.assertArrayEquals(new int[] { 10, 11, 2, 3, 4, 55 }, list.toArray());
	}

	@Test
	public void testSetOutOfBounds() {
		final IntList list = new IntList();
		try {
			list.set(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		try {
			list.set(1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}

		list.addAll(0, 1, 2, 3, 4, 5);

		try {
			list.set(6, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testGrow() {
		final IntList list = new IntList(1);

		final int size = 100;
		for (int i = 0; i < size; i++) {
			list.add(i);
		}
		Assertions.assertEquals(size, list.size());

		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(i, list.get(i));
		}
	}

	@Test
	public void testGrowIntListOfCapacityNull() {
		final IntList list = new IntList(0);

		final int size = 100;
		for (int i = 0; i < size; i++) {
			list.add(i);
		}
		Assertions.assertEquals(size, list.size());

		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(i, list.get(i));
		}
	}

	@Test
	public void testAddArray() {
		final IntList list = new IntList();

		list.addAll();
		Assertions.assertTrue(list.isEmpty());
		Assertions.assertEquals(0, list.size());

		final int size = 100;
		final int[] ints = ThreadLocalRandom.current().ints(size).toArray();

		list.addAll(ints);
		Assertions.assertEquals(size, list.size());

		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(ints[i], list.get(i));
		}

		final int[] anotherInts = ThreadLocalRandom.current().ints(size).toArray();
		list.addAll(anotherInts);
		Assertions.assertEquals(size * 2, list.size());
		for (int i = 0; i < size; i++) {
			Assertions.assertEquals(anotherInts[i], list.get(size + i));
		}
	}

	@Test
	public void testAddList() {
		final IntList list = new IntList();

		// adding empty list with no capacity
		list.addAll(IntList.of());
		Assertions.assertEquals(new IntList(), list);
		Assertions.assertTrue(list.isSorted());

		// adding empty list with capacity 2
		list.addAll(new IntList(2));
		Assertions.assertEquals(new IntList(), list);
		Assertions.assertTrue(list.isSorted());

		// adding sorted list to an empty list
		list.addAll(IntList.of(1, 2, 3));
		Assertions.assertEquals(IntList.of(1, 2, 3), list);
		Assertions.assertTrue(list.isSorted());

		// add empty list to a sorted list
		list.addAll(IntList.of());
		Assertions.assertEquals(IntList.of(1, 2, 3), list);
		Assertions.assertTrue(list.isSorted());

		// adding sorted list to a sorted list so that the list stays sorted
		list.addAll(IntList.of(3, 4, 5));
		Assertions.assertEquals(IntList.of(1, 2, 3, 3, 4, 5), list);
		Assertions.assertTrue(list.isSorted());

		// adding sorted list to a sorted list, but the new list is not sorted
		list.clear();
		list.addAll(IntList.of(1, 2, 3));
		list.addAll(IntList.of(0));
		Assertions.assertEquals(IntList.of(1, 2, 3, 0), list);
		Assertions.assertFalse(list.isSorted());

		// adding unsorted list to a sorted list
		list.clear();
		list.addAll(IntList.of(1, 2, 3));
		list.addAll(IntList.of(6, 5, 4));
		Assertions.assertEquals(IntList.of(1, 2, 3, 6, 5, 4), list);
		Assertions.assertFalse(list.isSorted());

		// adding unsorted list to an empty list
		list.clear();
		list.addAll(IntList.of(3, 2, 1));
		Assertions.assertEquals(IntList.of(3, 2, 1), list);
		Assertions.assertFalse(list.isSorted());

		// adding sorted list to an unsorted list
		list.clear();
		list.addAll(IntList.of(3, 2, 1));
		list.addAll(IntList.of(1, 2, 3));
		Assertions.assertEquals(IntList.of(3, 2, 1, 1, 2, 3), list);
		Assertions.assertFalse(list.isSorted());

	}

	@Test
	public void testGetArray() {
		final IntList list = new IntList();

		final int size = 100;
		final int[] ints = ThreadLocalRandom.current().ints(size).toArray();

		list.addAll(ints);

		final int length = 20;
		final int from = 10;
		final int[] actualInts = list.get(from, length);

		for (int i = 0; i < length; i++) {
			Assertions.assertEquals(actualInts[i], list.get(from + i));
			Assertions.assertEquals(actualInts[i], ints[from + i]);
		}
	}

	@Test
	public void testGetOutOfBounds() {
		final IntList list = new IntList();

		list.addAll(0, 1, 2, 3);

		try {
			list.get(-1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(4);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testGetArrayOutOfBounds() {
		final IntList list = new IntList();

		list.addAll(0, 1, 2, 3);

		try {
			list.get(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(1, -1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(3, 2);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.get(4, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testToArrayEmptyLIst() {
		final IntList list = new IntList();

		final int[] actual = list.toArray();
		Assertions.assertArrayEquals(actual, new int[0]);
	}

	@Test
	public void testToArray() {
		final IntList list = new IntList();
		list.addAll(1, 2, 3, 4, 5, 6);

		{
			final int[] input = new int[1];
			final int[] actual = list.toArray(input);
			// input is too short -> new array returned
			Assertions.assertNotSame(input, actual);
			Assertions.assertArrayEquals(list.toArray(), actual);
		}

		{
			final int[] input = new int[list.size()];
			final int[] actual = list.toArray(input);
			// input fits exactly -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(list.toArray(), actual);
		}

		{
			final int[] input = new int[list.size() + 1];
			final int[] expected = { 1, 2, 3, 4, 5, 6, 0 };
			final int[] actual = list.toArray(input);
			// input too big -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(expected, actual);
		}
	}

	@Test
	public void testToArrayWithEmptyList() {
		final IntList list = new IntList();

		{
			final int[] input = new int[0];
			final int[] actual = list.toArray(input);
			// input fits exactly -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(list.toArray(), actual);
		}

		{
			final int[] input = new int[list.size() + 1];
			final int[] expected = { 0 };
			final int[] actual = list.toArray(input);
			// input too big -> input returned
			Assertions.assertSame(input, actual);
			Assertions.assertArrayEquals(expected, actual);
		}
	}

	@Test
	public void testRemove() {
		final IntList list = new IntList();

		final int size = 10;
		final int[] ints = new int[size];
		for (int i = 0; i < size; i++) {
			ints[i] = i;
		}

		list.addAll(ints);

		// remove nothing
		list.remove(2, 2);
		Assertions.assertArrayEquals(ints, list.toArray());

		// remove the last element
		list.remove(9, 10);
		final int[] expectedA = removeElements(ints, 9);
		Assertions.assertArrayEquals(expectedA, list.toArray());

		// remove the first element
		list.remove(0, 1);
		final int[] expectedB = removeElements(expectedA, 0);
		Assertions.assertArrayEquals(expectedB, list.toArray());

		// remove single element in the middle
		list.remove(3, 4);
		final int[] expectedC = removeElements(expectedB, 3);
		Assertions.assertArrayEquals(expectedC, list.toArray());

		// remove several elements in the middle
		list.remove(2, 5);
		final int[] expectedD = removeElements(expectedC, 2, 3, 4);
		Assertions.assertArrayEquals(expectedD, list.toArray());

		// remove all elements
		list.remove(0, 4);
		final int[] expectedE = removeElements(expectedD, 0, 1, 2, 3);
		Assertions.assertArrayEquals(expectedE, list.toArray());
	}

	@Test
	public void testRemoveOutOfRange() {
		final IntList list = new IntList();

		list.addAll(0, 1, 2, 3);

		try {
			list.remove(-1, 1);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.remove(1, 0);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.remove(3, 5);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
		try {
			list.remove(4, 5);
			Assertions.fail();
		} catch (final IndexOutOfBoundsException e) {
			// expected
		}
	}

	@Test
	public void testTrim() {
		final IntList list = new IntList();

		list.addAll(0, 1, 2, 3);

		list.trim();
		Assertions.assertEquals(4, list.getCapacity());
	}

	@Test
	public void testTrimOnEmptyList() {
		final IntList list = new IntList();

		list.trim();
		Assertions.assertEquals(0, list.getCapacity());
	}

	@Test
	public void testClear() {
		final IntList list = IntList.of(2, 0, 1); // unsorted list
		final int capacityBeforeClear = list.getCapacity();
		list.clear();

		Assertions.assertEquals(0, list.size(), "the list is empty after clear");
		Assertions.assertEquals(capacityBeforeClear, capacityBeforeClear, "capacity does not change");
		Assertions.assertTrue(list.isSorted(), "empty lists are sorted");
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testHashCodeEquals() {
		final IntList a = new IntList();
		final IntList b = new IntList();

		// on empty lists
		Assertions.assertTrue(a.equals(b));
		Assertions.assertEquals(a.hashCode(), b.hashCode());

		// on equal lists
		a.addAll(1, 2, 3, 4);
		b.addAll(1, 2, 3, 4);
		Assertions.assertTrue(a.equals(b));
		Assertions.assertEquals(a.hashCode(), b.hashCode());

		// trim only one of them
		// we want to compare the actual content
		a.trim();
		Assertions.assertTrue(a.equals(b));
		Assertions.assertEquals(a.hashCode(), b.hashCode());

		// change one value
		a.remove(2, 3);
		a.insert(2, 99);
		Assertions.assertFalse(a.equals(b));
		Assertions.assertNotEquals(a.hashCode(), b.hashCode());

		// have different length
		a.add(66);
		Assertions.assertFalse(a.equals(b));
		Assertions.assertNotEquals(a.hashCode(), b.hashCode());

		// same object
		Assertions.assertTrue(a.equals(a));

		// other is null
		Assertions.assertFalse(a.equals(null));

		// equals with a different class
		Assertions.assertFalse(a.equals("not an IntList"));

		// sorted and unsorted lists with same length
		final IntList sorted = IntList.of(1, 2, 3, 4);
		final IntList unsorted = IntList.of(4, 3, 2, 1);
		Assertions.assertFalse(sorted.equals(unsorted));
		Assertions.assertNotEquals(sorted.hashCode(), unsorted.hashCode());
	}

	@Test
	public void testSort() {
		final IntList list = new IntList();

		list.addAll(4, 2, 3, 1);
		list.sort();

		final int[] expected = new int[] { 1, 2, 3, 4 };
		Assertions.assertArrayEquals(expected, list.toArray());
	}

	@Test
	public void testSortParallel() {
		final IntList list = new IntList();

		list.addAll(4, 2, 3, 1);
		list.parallelSort();

		final int[] expected = new int[] { 1, 2, 3, 4 };
		Assertions.assertArrayEquals(expected, list.toArray());
	}

	@Test
	public void testShuffle() {
		final IntList list = IntList.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
		final IntList original = list.clone();
		final IntList original2 = list.clone();

		final Random random = new Random(1);
		list.shuffle(random);

		Assertions.assertEquals(original.size(), list.size());

		original.removeAll(list);
		Assertions.assertTrue(original.isEmpty(), "original minus shuffled list: " + original);

		list.removeAll(original2);
		Assertions.assertTrue(list.isEmpty(), "shuffled list minus original: " + list);
	}

	@Test
	public void testShuffle_sortOfEmptyElementList() {
		final IntList list = IntList.of();
		list.shuffle();

		Assertions.assertTrue(list.isSorted(), "an empty list is sorted");
	}

	@Test
	public void testShuffle_sortOfOneElementList() {
		final IntList list = IntList.of(1);
		list.shuffle();

		Assertions.assertTrue(list.isSorted(), "a shuffled list of size 1 is sorted");
	}

	@Test
	public void testShuffle_sortOfListWithIdenticalElements() {
		final IntList list = IntList.of(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
		list.shuffle();

		Assertions.assertTrue(list.isSorted(), "a shuffled list with identical elements is sorted");
	}

	@Test
	public void testShuffle_sortOfThreeElementList() {
		final IntList list = IntList.of(2, 3, 1);

		int sorted = 0;
		int unsorted = 0;
		for (int i = 0; i < 20; i++) {
			final Random random = new Random(i);
			list.shuffle(random);

			sorted += IntList.of(1, 2, 3).equals(list) ? 1 : 0;
			unsorted += IntList.of(1, 2, 3).equals(list) ? 0 : 1;

			Assertions.assertEquals(IntList.of(1, 2, 3).equals(list), list.isSorted());
		}

		Assertions.assertTrue(sorted > 0);
		Assertions.assertTrue(unsorted > 0);
	}

	private int[] removeElements(final int[] data, final int... removedIndices) {
		final int[] result = new int[data.length - removedIndices.length];
		final List<Integer> blacklist = Arrays.stream(removedIndices).boxed().collect(Collectors.toList());

		int j = 0;
		for (int i = 0; i < data.length; i++) {
			if (!blacklist.contains(i)) {
				result[j] = data[i];
				j++;
			}
		}

		return result;
	}

	@Test
	public void testSerialize() throws IOException, ClassNotFoundException {
		final IntList emptyList = new IntList(0);
		internalSerializeTest(emptyList);

		final IntList emptyListWithCapacity = new IntList(10);
		internalSerializeTest(emptyListWithCapacity);

		final IntList threeElements = new IntList(10000);
		threeElements.addAll(1, 2, 3);
		internalSerializeTest(threeElements);

		final IntList trimmedList = new IntList(10000);
		trimmedList.addAll(1, 2, 3);
		trimmedList.trim();
		internalSerializeTest(trimmedList);
	}

	private void internalSerializeTest(final IntList list) throws IOException, ClassNotFoundException {
		final ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		try (final ObjectOutputStream out = new ObjectOutputStream(byteBuffer)) {
			out.writeObject(list);
		}

		try (ByteArrayInputStream byteInputStream = new ByteArrayInputStream(byteBuffer.toByteArray());
				ObjectInputStream in = new ObjectInputStream(byteInputStream)) {
			final IntList actual = (IntList) in.readObject();

			Assertions.assertEquals(list, actual);

			Assertions.assertEquals(list.size(), actual.getCapacity(),
					"The capacity of the deserialized list. Should be equal to the number of inserted "
							+ "values, because we don't want to serialize the unused part of the array");
		}
	}

	@Test
	public void testClone() {
		final IntList list = new IntList();
		list.addAll(1, 2);

		final IntList clone = list.clone();

		Assertions.assertEquals(list, clone);

		list.set(1, 0);
		Assertions.assertNotEquals(list, clone);
	}

	@Test
	public void testCloneEmptyList() {
		final IntList list = new IntList();

		final IntList clone = list.clone();
		Assertions.assertEquals(list, clone);
	}

	@Test
	public void testClonePreservesSortedFlagOnUnsortedList() {
		final IntList list = IntList.of(3, 2, 1);

		final IntList clone = list.clone();
		Assertions.assertEquals(list.isSorted(), clone.isSorted());
	}

	@Test
	public void testClonePreservesSortedFlagOnSortedList() {
		final IntList list = IntList.of(1, 2, 3);

		final IntList clone = list.clone();
		Assertions.assertEquals(list.isSorted(), clone.isSorted());
	}

	@Test
	public void testToString() {
		Assertions.assertEquals("[]", new IntList().toString());

		final IntList list = new IntList();
		list.addAll(-2, -1, 0, 1, 2);
		Assertions.assertEquals("[-2, -1, 0, 1, 2]", list.toString());
		Assertions.assertEquals(Arrays.toString(list.toArray()), list.toString(), "same result as Arrays.toString()");
	}

	@Test
	public void testSequentialStream() {

		{
			final IntList list = new IntList();
			list.addAll(0, 1, 2, 3, 4, 5, 6);
			final IntStream stream = list.stream();
			Assertions.assertEquals(list.size(), stream.count());
		}
		{
			final IntList list = new IntList();
			list.addAll(0, 1, 2, 3, 4, 5);
			final IntStream stream = list.stream();
			Assertions.assertEquals(15, stream.sum());
		}
		{
			final IntList emptyList = new IntList();
			Assertions.assertEquals(0, emptyList.stream().count());
		}
	}

	@Test
	public void testParallelStream() {
		final IntList list = new IntList();

		final int size = 1000;
		final int[] ints = new int[size];
		for (int i = 0; i < size; i++) {
			ints[i] = i;
		}

		list.addAll(ints);

		final ConcurrentLinkedQueue<Integer> processingOrder = new ConcurrentLinkedQueue<>();
		final List<Integer> actualList = list.parallelStream()//
				.peek(e -> processingOrder.add(e))//
				.boxed()//
				.collect(Collectors.toList());

		Assertions.assertEquals(actualList.toString(), list.toString(), "should be sequential, when using collect");
		Assertions.assertNotEquals(processingOrder.toString(), list.toString(),
				"should use parallelism during computation");
	}

	@Test
	public void testIndexOfOnSortedList() {
		final IntList list = new IntList();
		Assertions.assertEquals(-1, list.indexOf(0));

		list.add(1);
		Assertions.assertEquals(-1, list.indexOf(0));
		Assertions.assertEquals(0, list.indexOf(1));

		list.add(2);
		Assertions.assertEquals(-1, list.indexOf(0));
		Assertions.assertEquals(0, list.indexOf(1));
		Assertions.assertEquals(1, list.indexOf(2));
	}

	@Test
	public void testIndexOfOnSortedListReturnsFirstMatch() {
		final IntList list = IntList.of(0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4);
		Assertions.assertEquals(2, list.indexOf(2));
		Assertions.assertEquals(4, list.indexOf(2, 4));
	}

	@Test
	public void testIndexOfOnUnsortedList() {
		final IntList list = IntList.of(2, 0, 1);
		Assertions.assertEquals(1, list.indexOf(0));

		Assertions.assertEquals(0, list.indexOf(2));
		Assertions.assertEquals(2, list.indexOf(1));
		Assertions.assertEquals(-1, list.indexOf(3));
	}

	@Test
	public void testIndexOfWithOffsetOnSortedList() {
		final IntList list = new IntList();
		list.addAll(1, 1, 2, 3, 3);
		Assertions.assertEquals(0, list.indexOf(1, 0));
		Assertions.assertEquals(1, list.indexOf(1, 1));
		Assertions.assertEquals(2, list.indexOf(2, 2));
		Assertions.assertEquals(-1, list.indexOf(2, 3));
		Assertions.assertEquals(3, list.indexOf(3, 2));
	}

	@Test
	public void testIndexOfWithOffsetOnSortedListWithOffsetOutOfRange() {
		final IntList list = new IntList();
		list.addAll(1);
		Assertions.assertEquals(0, list.indexOf(1, -1));
		Assertions.assertEquals(-1, list.indexOf(1, list.size()));
	}

	@Test
	public void testIndexOfWithOffsetOnUnsortedList() {
		final IntList list = new IntList();
		list.addAll(0, 2, 0, 2);
		Assertions.assertEquals(1, list.indexOf(2, 0));
		Assertions.assertEquals(1, list.indexOf(2, 1));
		Assertions.assertEquals(3, list.indexOf(2, 2));
		Assertions.assertEquals(3, list.indexOf(2, 3));
		Assertions.assertEquals(-1, list.indexOf(2, 4));

		// indexed returned by indexOf() are consistent with get()
		Assertions.assertEquals(list.get(list.indexOf(2, 0)), 2);
		Assertions.assertEquals(list.get(list.indexOf(2, 1)), 2);
		Assertions.assertEquals(list.get(list.indexOf(2, 2)), 2);
		Assertions.assertEquals(list.get(list.indexOf(2, 3)), 2);
	}

	@Test
	public void testLastIndexOfWithOffsetOnEmptyList() {
		final IntList list = IntList.of();

		Assertions.assertEquals(-1, list.lastIndexOf(3, list.size()));
	}

	@Test
	public void testLastIndexOfWithOffsetOnSortedList() {
		final IntList list = IntList.of(1, 2, 2, 3, 4, 5, 6, 7, 8, 9);

		Assertions.assertEquals(3, list.lastIndexOf(3, list.size()));
		Assertions.assertEquals(2, list.lastIndexOf(2, list.size()));
		Assertions.assertEquals(0, list.lastIndexOf(1, list.size()));

		Assertions.assertEquals(2, list.lastIndexOf(2, 2)); // fromIndex == result
		Assertions.assertEquals(1, list.lastIndexOf(2, 1)); // fromIndex == result && the next element would be a hit

		Assertions.assertEquals(0, list.lastIndexOf(1, 0)); // fromIndex 0; found
		Assertions.assertEquals(-1, list.lastIndexOf(99, 0)); // fromIndex 0; not found
		Assertions.assertEquals(-1, list.lastIndexOf(99, list.size())); // fromIndex larger than list && not found
	}

	@Test
	public void testLastIndexOfOnSortedListReturnsFirstMatch() {
		final IntList list = IntList.of(0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4);
		Assertions.assertEquals(13, list.lastIndexOf(2));
		Assertions.assertEquals(10, list.lastIndexOf(2, 10));
	}

	@Test
	public void testLastIndexOfWithOffsetOnUnsortedList() {
		final IntList list = IntList.of(1, 2, 3, 1, 2, 3);

		Assertions.assertEquals(5, list.lastIndexOf(3, list.size()));
		Assertions.assertEquals(4, list.lastIndexOf(2, list.size()));
		Assertions.assertEquals(3, list.lastIndexOf(1, list.size()));
		Assertions.assertEquals(-1, list.lastIndexOf(99, list.size()));
		Assertions.assertEquals(-1, list.lastIndexOf(99, 0));

		Assertions.assertEquals(2, list.lastIndexOf(3, list.size() - 2));
		Assertions.assertEquals(4, list.lastIndexOf(2, list.size() - 1));
		Assertions.assertEquals(1, list.lastIndexOf(2, list.lastIndexOf(2) - 1));
	}

	@Test
	public void replaceAll() {
		final IntList list = new IntList();
		list.addAll(1, 2, 3);

		list.replaceAll(i -> i * 2);
		Assertions.assertArrayEquals(new int[] { 2, 4, 6 }, list.toArray());
	}

	@Test
	public void testReplaceAllOnEmptyList() {
		final IntList list = new IntList();

		list.replaceAll(i -> i * 3);
		Assertions.assertArrayEquals(new int[0], list.toArray());
	}

	@Test
	public void testRemoveAll() {
		final IntList list = IntList.of(-2, -1, 0, 1, 2, 3, 4, 5, 6);
		final IntList remove = IntList.of(-1, 2, 4, 5);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new int[] { -2, 0, 1, 3, 6 }, list.toArray());
		Assertions.assertArrayEquals(new int[] { -1, 2, 4, 5 }, remove.toArray());
	}

	@Test
	public void testRemoveEmptyList() {
		final IntList list = IntList.of(1, 2, 3, 4, 5, 6);
		final IntList remove = new IntList();

		list.removeAll(remove);
		Assertions.assertArrayEquals(new int[] { 1, 2, 3, 4, 5, 6 }, list.toArray());
		Assertions.assertArrayEquals(new int[] {}, remove.toArray());
	}

	@Test
	public void testRemoveAllFromEmptyList() {
		final IntList list = new IntList();
		final IntList remove = IntList.of(1);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new int[] {}, list.toArray());
		Assertions.assertEquals(0, list.size());
	}

	@Test
	public void testRemoveAllFromSortedList() {
		final IntList list = IntList.of(1, 2, 3, 4, 5);
		final IntList remove = IntList.of(1, 3);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new int[] { 2, 4, 5 }, list.toArray());
		Assertions.assertEquals(3, list.size());
		Assertions.assertTrue(list.isSorted());
	}

	@Test
	public void testRemoveAllFromSortedList2() {
		final IntList list = IntList.of(2, 4, 4, 6, 8);
		final IntList remove = IntList.of(1, 4, 9);

		list.removeAll(remove);
		Assertions.assertArrayEquals(new int[] { 2, 6, 8 }, list.toArray());
		Assertions.assertEquals(3, list.size());
		Assertions.assertTrue(list.isSorted());
	}

	@Test
	public void testRetainAll() {
		final IntList list = IntList.of(-2, -1, 0, 1, 2, 3, 4, 5, 6);
		final IntList retain = IntList.of(-1, 2, 4, 5, 99);

		list.retainAll(retain);
		Assertions.assertArrayEquals(new int[] { -1, 2, 4, 5 }, list.toArray());
		Assertions.assertArrayEquals(new int[] { -1, 2, 4, 5, 99 }, retain.toArray());
	}

	@Test
	public void testRetainAllOnEmptyList() {
		final IntList list = IntList.of();
		final IntList retain = IntList.of(-1, 2, 4, 5, 99);

		list.retainAll(retain);
		Assertions.assertArrayEquals(new int[] {}, list.toArray());
		Assertions.assertArrayEquals(new int[] { -1, 2, 4, 5, 99 }, retain.toArray());
	}

	@Test
	public void testRetainAllFromEmptyList() {
		final IntList list = IntList.of(-2, -1, 0, 1, 2);
		final IntList retain = IntList.of();

		list.retainAll(retain);
		Assertions.assertArrayEquals(new int[] {}, list.toArray());
		Assertions.assertArrayEquals(new int[] {}, retain.toArray());
	}

	@Test
	public void testRemoveIf() {
		final IntList list = IntList.of(1, 2, 3, 4, 5, 6);

		list.removeIf((value, index) -> value % 2 == 0);
		Assertions.assertArrayEquals(new int[] { 1, 3, 5 }, list.toArray());
	}

	@Test
	public void testRemoveIfNegationOfPredicate() {
		final IntList list = IntList.of(1, 2, 3, 4, 5, 6);

		final IntPredicate predicate = (value, index) -> value % 2 == 0;
		list.removeIf(predicate.negate());
		Assertions.assertArrayEquals(new int[] { 2, 4, 6 }, list.toArray());
	}

	@Test
	public void testRemoveIfWithAndCombinedPredicates() {
		final IntList list = IntList.of(1, 2, 3, 4, 5, 6);

		final IntPredicate predicateA = (value, index) -> value % 2 == 0;
		final IntPredicate predicateB = (value, index) -> value == 3 || value == 4;
		list.removeIf(predicateA.and(predicateB));
		Assertions.assertArrayEquals(new int[] { 1, 2, 3, 5, 6 }, list.toArray());
	}

	@Test
	public void testRemoveIfWithOrCombinedPredicates() {
		final IntList list = IntList.of(1, 2, 3, 4, 5, 6);

		final IntPredicate predicateA = (value, index) -> value % 2 == 0;
		final IntPredicate predicateB = (value, index) -> value == 3 || value == 4;
		list.removeIf(predicateA.or(predicateB));
		Assertions.assertArrayEquals(new int[] { 1, 5 }, list.toArray());
	}

	@Test
	public void testRemoveIfOnEmptyList() {
		final IntList list = IntList.of();

		list.removeIf((value, index) -> false);
		Assertions.assertArrayEquals(new int[] {}, list.toArray());
	}

	@Test
	public void testSortedFlagSort() {

		Assertions.assertTrue(new IntList().isSorted(), "empty list is sorted");

		final IntList list = new IntList();
		list.addAll(2, 0, 1);
		list.sort();
		Assertions.assertTrue(list.isSorted(), "is sorted after calling sort");
	}

	@Test
	public void testSortedFlagEmptyList() {

		Assertions.assertTrue(new IntList().isSorted(), "empty list is sorted");

		final IntList list = new IntList();
		list.addAll(2, 0, 1);
		list.remove(0, list.size());
		Assertions.assertTrue(list.isSorted(), "unsorted list initialized by addAll");
	}

	@Test
	public void testSortedFlagAdd() {

		final IntList list = new IntList();
		list.add(1);
		Assertions.assertTrue(list.isSorted(), "[1]");
		list.add(2);
		Assertions.assertTrue(list.isSorted(), "[1,2]");
		list.add(2);
		Assertions.assertTrue(list.isSorted(), "[1,2,2]");
		list.add(1);
		Assertions.assertFalse(list.isSorted(), "[1,2,2,1]");
		list.add(1);
		Assertions.assertFalse(list.isSorted(), "[1,2,2,1,1]");
	}

	@Test
	public void testSortedFlagAddAll() {

		{
			final IntList list = new IntList();
			list.addAll(2, 0);
			Assertions.assertFalse(list.isSorted(), "unsorted list initialized by addAll");
		}

		{
			final IntList list = new IntList();
			list.addAll(1);
			Assertions.assertTrue(list.isSorted(), "list with one element is sorted");
		}

		{
			final IntList list = new IntList();
			list.addAll(1, 1);
			Assertions.assertTrue(list.isSorted(), "list with all the same elements is sorted");
		}
	}

	@Test
	public void testSortedFlagInsert() {

		/*
		 * tests that result in a sorted list
		 */
		{
			final IntList list = new IntList();
			list.insert(0, 1, 2, 3);
			Assertions.assertTrue(list.isSorted(), "insert sorted values into empty list");
		}

		{
			final IntList list = IntList.of(1, 1);
			list.insert(0, 2, 2); // -> [2,2,1,1]
			Assertions.assertFalse(list.isSorted(), "insert before: unsorted");
		}
		{
			final IntList list = IntList.of(3, 3);
			list.insert(0, 2, 2); // -> [2,2,3,3]
			Assertions.assertTrue(list.isSorted(), "insert sorted values before: sorted");
		}

		{
			final IntList list = IntList.of(4, 4);
			list.insert(2, 2, 2); // -> [4,4,2,2]
			Assertions.assertFalse(list.isSorted(), "insert sorted values at end: unsorted");
		}
		{
			final IntList list = IntList.of(1, 1);
			list.insert(2, 2, 2); // -> [1,1,2,2]
			Assertions.assertTrue(list.isSorted(), "insert sorted values at end: sorted");
		}

		{
			final IntList list = IntList.of(1, 4);
			list.insert(1, 2, 2); // -> [1,2,2,4]
			Assertions.assertTrue(list.isSorted(), "insert sorted values in middle: sorted");
		}

		/*
		 * tests that result in an unsorted list
		 */
		{
			final IntList list = IntList.of(1, 4);
			list.insert(1, 5); // -> [1,5,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}
		{
			final IntList list = IntList.of(1, 4);
			list.insert(1, 6, 6); // -> [1,6,6,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}

		{
			final IntList list = IntList.of(1, 4);
			list.insert(1, 0); // -> [1,0,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}
		{
			final IntList list = IntList.of(1, 4);
			list.insert(1, 0, 0); // -> [1,0,0,4]
			Assertions.assertFalse(list.isSorted(), "insert sorted values in middle: unsorted");
		}

		{
			final IntList list = IntList.of(3, 4);
			list.insert(0, 2, 1); // -> [2,1,3,4]
			Assertions.assertFalse(list.isSorted(), "insert unsorted at begin: unsorted");
		}
		{
			final IntList list = IntList.of(1, 4);
			list.insert(1, 2, 1); // -> [1,2,1,4]
			Assertions.assertFalse(list.isSorted(), "insert unsorted middle: unsorted");
		}
		{
			final IntList list = IntList.of(3, 4);
			list.insert(2, 6, 5); // -> [13,4,6,5]
			Assertions.assertFalse(list.isSorted(), "insert unsorted at end: unsorted");
		}

	}

	@Test
	public void testSortedFlagSetByIndex() {

		{
			final IntList list = IntList.of(0, 1, 2);
			list.set(0, -1);
			Assertions.assertTrue(list.isSorted(), "set first element: [-1,1,2] sorted");
			list.set(0, 1);
			Assertions.assertTrue(list.isSorted(), "set first element: [1,1,2] sorted");
			list.set(0, 2);
			Assertions.assertFalse(list.isSorted(), "set first element: [2,1,2] not sorted");
		}

		{
			final IntList sortedList = IntList.of(0, 2, 4);
			sortedList.set(1, 3);
			Assertions.assertTrue(sortedList.isSorted(), "set middle element: [0,3,4] sorteed");
			sortedList.set(1, 4);
			Assertions.assertTrue(sortedList.isSorted(), "set middle element: [0,4,4] sorteed");
			sortedList.set(1, 0);
			Assertions.assertTrue(sortedList.isSorted(), "set middle element: [0,0,4] sorteed");
			sortedList.set(1, 5);
			Assertions.assertFalse(sortedList.isSorted(), "set middle element: [0,5,4] not sorteed");
		}

		{
			final IntList sortedList = IntList.of(0, 1, 2);
			sortedList.set(2, 3);
			Assertions.assertTrue(sortedList.isSorted(), "set last element: [0,1,3] sorteed");
			sortedList.set(2, 1);
			Assertions.assertTrue(sortedList.isSorted(), "set last element: [0,1,1] sorteed");
			sortedList.set(2, 0);
			Assertions.assertFalse(sortedList.isSorted(), "set last element: [0,1,0] not sorteed");
		}

		{
			final IntList sortedList = IntList.of(0, 1, 2);
			sortedList.set(2, 0);
			sortedList.set(2, 2);
			Assertions.assertFalse(sortedList.isSorted(), "unsorted lists stay unsorted");
		}
	}

	@Test
	public void testSortedFlagRemove() {

		final IntList list = IntList.of(4, 3, 2, 1);
		list.remove(0, 2); // removes 4,3
		Assertions.assertFalse(list.isSorted(), "unsorted list with two elements is not sorted");

		list.remove(0, 1);
		Assertions.assertTrue(list.isSorted(), "unsorted list with one element becomes sorted");

		list.add(-1); // make list unsorted again
		list.remove(0, 2); // remove both elements
		Assertions.assertTrue(list.isSorted(), "unsorted list with no elements becomes sorted");
	}

	@Test
	public void testSortedFlagRemove_unsortedBecomesSorted_emptyList() {

		final IntList list = IntList.of(4, 3, 2, 1);
		list.remove(0, 4); // removes all
		Assertions.assertTrue(list.isSorted(), "empty list is sorted");
	}

	@Test
	public void testSortedFlagRemove_unsortedBecomesSorted_oneElement() {

		final IntList list = IntList.of(4, 3, 2, 1);
		list.remove(1, 4); // removes 3,2,1
		Assertions.assertTrue(list.isSorted(), "list with one element is");
	}

	@Test
	public void testSortedFlagRemove_unsortedBecomesSorted() {

		final IntList list = IntList.of(1, 2, 777, 4, 5);
		list.remove(2, 3); // removes 777
		Assertions.assertTrue(list.isSorted(), "list is sorted after remove");
	}

	@Test
	public void testSortedFlagRemoveAll() {

		final IntList list = IntList.of(4, 3, 2, 1);
		list.removeAll(IntList.of(4, 3));
		Assertions.assertFalse(list.isSorted(), "unsorted list with two elements is not sorted");

		list.removeAll(IntList.of(2));
		Assertions.assertTrue(list.isSorted(), "unsorted list with one element becomes sorted");

		list.add(-1); // make list unsorted again
		list.removeAll(IntList.of(-1, 2)); // remove both elements
		Assertions.assertTrue(list.isSorted(), "unsorted list with no elements becomes sorted");
	}

	@Test
	public void testSortedFlagRemoveIf() {

		final IntList list = IntList.of(4, 3, 2, 1);
		list.removeIf((value, index) -> value >= 3); // removes 3 and 4
		Assertions.assertFalse(list.isSorted(), "unsorted list with two elements is not sorted");

		list.removeIf((value, index) -> value >= 2); // removes 2
		Assertions.assertTrue(list.isSorted(), "unsorted list with one element becomes sorted");

		list.add(-1); // make list unsorted again
		list.removeIf((value, index) -> true); // remove both elements
		Assertions.assertTrue(list.isSorted(), "unsorted list with no elements becomes sorted");
	}

	@Test
	public void testSortedFlagRemoveIf_unsortedBecomesSorted_emptyAfterRemove() {

		final IntList list = IntList.of(1, 3, 2);
		list.removeIf((value, index) -> true); // makes the list sorted
		Assertions.assertTrue(list.isEmpty(), "list is empty");
		Assertions.assertTrue(list.isSorted(), "empty list is sorted");
	}

	@Test
	public void testSortedFlagRemoveIf_unsortedBecomesSorted_oneElementAfterRemove() {

		final IntList list = IntList.of(1, 3, 2);
		list.removeIf((value, index) -> value > 1); // makes the list sorted
		Assertions.assertTrue(list.isSorted(), "list with one element is sorted");
	}

	@Test
	public void testSortedFlagRemoveIf_unsortedBecomesSorted() {

		final IntList list = IntList.of(1, 2, 3, 777, 4, 5);
		list.removeIf((value, index) -> value == 777); // makes the list sorted
		Assertions.assertTrue(list.isSorted(), "unsorted list becomes sorted");
	}

	@Test
	public void testSortedFlagReplace() {

		final IntList list = IntList.of(1, 2, 3, 4);
		list.replaceAll(v -> v >= 3 ? 2 : v); // replace 3 and 4 with 2 -> [1,2,2,2]
		Assertions.assertTrue(list.isSorted(), "sorted list still sorted after replace");

		list.replaceAll(v -> v == 1 ? 3 : v); // replace 1 with 3 -> [3,2,2,2]
		Assertions.assertFalse(list.isSorted(), "sorted list becomes unsorted after replace");

		list.replaceAll(v -> 2); // replace all with 2 -> [2,2,2,2]
		Assertions.assertFalse(list.isSorted(), "unsorted list stays unsorted");
	}

	@Test
	public void testIntersectionSortedLists() {
		{
			final IntList a = IntList.of(0, 1, 2, 3, 4);
			final IntList b = IntList.of(2, 4, 5);
			final IntList actual = IntList.intersection(a, b);
			Assertions.assertEquals(IntList.of(2, 4), actual);
		}

		{
			final IntList a = IntList.of(0, 2, 4, 6);
			final IntList b = IntList.of(3, 5);
			final IntList actual = IntList.intersection(a, b);
			Assertions.assertEquals(IntList.of(), actual);
		}

		/*
		 * duplicate elements are removed in the result
		 */
		{
			final IntList a = IntList.of(3, 3, 3);
			final IntList b = IntList.of(3, 3);
			final IntList actual = IntList.intersection(a, b);
			Assertions.assertEquals(IntList.of(3), actual);
		}
		{
			final IntList a = IntList.of(4, 4);
			final IntList b = IntList.of(4, 4, 4);
			final IntList actual = IntList.intersection(a, b);
			Assertions.assertEquals(IntList.of(4), actual);
		}
	}

	@Test
	public void testIntersectionUnsortedLists() {
		{
			final IntList a = IntList.of(0, 1, 2, 3, 4);
			final IntList b = IntList.of(2, 4, 5);
			a.shuffle();
			b.shuffle();
			final IntList actual = IntList.intersection(a, b);
			actual.sort();
			Assertions.assertEquals(IntList.of(2, 4), actual);
		}

		/*
		 * duplicate elements are removed in the result
		 */
		{
			final IntList a = IntList.of(3, 5, 3, 3, 1);
			final IntList b = IntList.of(2, 3, 3);
			final IntList actual = IntList.intersection(a, b);
			Assertions.assertEquals(IntList.of(3), actual);
		}
		{
			final IntList a = IntList.of(1, 4);
			final IntList b = IntList.of(4, 3, 4, 4, 2);
			final IntList actual = IntList.intersection(a, b);
			Assertions.assertEquals(IntList.of(4), actual);
		}

		{
			final IntList a = IntList.of(1, 4, 3, 2, 4);
			final IntList b = IntList.of(4, 3, 4, 4, 2);
			final IntList actual = IntList.intersection(a, b);
			actual.sort();
			Assertions.assertEquals(IntList.of(2, 3, 4), actual);
		}
	}

	@Test
	public void testUnionSortedLists_emptyLists() {
		final IntList a = IntList.of();
		final IntList b = IntList.of();

		Assertions.assertEquals(IntList.of(), IntList.union(a, b));
		Assertions.assertEquals(IntList.union(a, b), IntList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_uniqueValues() {
		final IntList a = IntList.of(0, 1, 3, 4);
		final IntList b = IntList.of(2, 4, 5);

		final IntList actual = IntList.union(a, b);
		Assertions.assertEquals(IntList.of(0, 1, 2, 3, 4, 5), actual);
		Assertions.assertEquals(IntList.union(a, b), IntList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_duplicateValues_inMiddleOfListA() {
		final IntList a = IntList.of(1, 2, 2, 3);
		final IntList b = IntList.of(1, 3);

		final IntList actual = IntList.union(a, b);
		Assertions.assertEquals(IntList.of(1, 2, 3), actual);
		Assertions.assertEquals(IntList.union(a, b), IntList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_duplicateValues_inMiddleOfBothLists() {
		final IntList a = IntList.of(1, 2, 2, 3);
		final IntList b = IntList.of(1, 2, 2, 4);

		final IntList actual = IntList.union(a, b);
		Assertions.assertEquals(IntList.of(1, 2, 3, 4), actual);
		Assertions.assertEquals(IntList.union(a, b), IntList.union(b, a));
	}

	@Test
	public void testUnionSortedLists_duplicateValues_atEndOfListA_whenHighestValueInBIsSmaller() {
		final IntList a = IntList.of();
		final IntList b = IntList.of(2, 2);

		final IntList actual = IntList.union(a, b);
		Assertions.assertEquals(IntList.of(2), actual);
		Assertions.assertEquals(IntList.union(a, b), IntList.union(b, a));
	}

	@Test
	public void testUnionUnsortedLists() {
		final IntList a = IntList.of(1, 0, 3, 4);
		final IntList b = IntList.of(2, 5, 4);

		final IntList actual = IntList.union(a, b);
		Assertions.assertEquals(IntList.of(0, 1, 2, 3, 4, 5), actual);
		Assertions.assertEquals(IntList.union(a, b), IntList.union(b, a));
	}

	@Test
	public void testUnionUnsortedLists_oneListIsSorted() {
		final IntList a = IntList.of(1, 2, 3);
		final IntList b = IntList.of(2, 5, 4);

		final IntList actual = IntList.union(a, b);
		Assertions.assertEquals(IntList.of(1, 2, 3, 4, 5), actual);
		Assertions.assertEquals(IntList.union(a, b), IntList.union(b, a));
	}

	@Test
	public void testUniq_sorted() {
		final IntList sorted = IntList.of(1, 1, 2, 3, 4, 4, 4);
		final IntList expected = IntList.of(1, 2, 3, 4);

		sorted.uniq();
		Assertions.assertEquals(expected, sorted);
	}

	@Test
	public void testUniq_empty() {
		final IntList empty = IntList.of();
		final IntList expected = IntList.of();

		empty.uniq();
		Assertions.assertEquals(expected, empty);
	}

	@Test
	public void testUniq_oneElement() {
		final IntList list = IntList.of(1);
		final IntList expected = IntList.of(1);

		list.uniq();
		Assertions.assertEquals(expected, list);
	}

	@Test
	public void testUniq_unsorted() {
		final IntList unsorted = IntList.of(1, 1, 2, 3, 4, 4, 4);
		unsorted.shuffle();
		final IntList expected = IntList.of(1, 2, 3, 4);

		unsorted.uniq();
		Assertions.assertEquals(expected, unsorted);
	}
}
