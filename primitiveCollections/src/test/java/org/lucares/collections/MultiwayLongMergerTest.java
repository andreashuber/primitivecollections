package org.lucares.collections;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MultiwayLongMergerTest {
	
	@Test
	public void testMergeTwoLists() {
		
		LongList a = LongList.of(1,2,3);
		LongList b = LongList.of(1,3,5);
		LongList expected = LongList.of(1,2,3,5);
		
		
		LongList union = MultiwayLongMerger.unionSorted(Arrays.asList(a,b));
		Assertions.assertEquals(expected, union);
	}
	
	@Test
	public void testMergeThreeLists() {
		
		LongList a = LongList.of(1,2,3);
		LongList b = LongList.of(1,3,5);
		LongList c = LongList.of(2,3,5);
		LongList expected = LongList.of(1,2,3,5);
		
		
		LongList union = MultiwayLongMerger.unionSorted(Arrays.asList(a,b,c));
		Assertions.assertEquals(expected, union);
	}
	
	@Test
	public void testMergeListsWithLongMin() {
		
		LongList a = LongList.of(Long.MIN_VALUE,2,3);
		LongList b = LongList.of(1,3,5);
		LongList c = LongList.of(Long.MIN_VALUE,Long.MIN_VALUE);
		LongList expected = LongList.of(Long.MIN_VALUE,1,2,3,5);
		
		
		LongList union = MultiwayLongMerger.unionSorted(Arrays.asList(a,b,c));
		Assertions.assertEquals(expected, union);
	}
	
	@Test
	public void testMergeEmptyLists() {
		
		LongList a = LongList.of();
		LongList b = LongList.of();
		LongList expected = LongList.of();
		
		
		LongList union = MultiwayLongMerger.unionSorted(Arrays.asList(a,b));
		Assertions.assertEquals(expected, union);
	}
}
