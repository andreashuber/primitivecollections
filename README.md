Java collections for primitives (currently only int) released under [MIT](https://opensource.org/licenses/MIT) license.

*IntList* and *LongList* are implementations of lists that store primitive integers/longs. The ints/longs are stored in an int/long array and the class takes care of growing the array as needed. You can trim the list to reduce the memory overhead. No dependencies.

The lists support the following operations: 

* add/remove elements via single and bulk operations
* replace elements
* sort and shuffle
* the lists know whether or not they are sorted and can leverage that knowledge for searching (binary search), union and intersection
* search for elements
* union, intersection, retainIf, retainAll removeIf, removeAll
* clear and trim are separate methods, so that the list can be re-used without having to re-allocate memory
* stream support
* the lists are serializable and cloneable
* the lists are **not** thread-safe

# How to use
The library is still considered beta. There no pre-build artifacts on Maven Central or JCenter, but you can download them from repo.lucares.org.

Example for Gradle:

```groovy
apply plugin: 'maven'
repositories {
	maven { url 'https://repo.lucares.org/' }
}
dependencies {
	compile 'org.lucares:primitiveCollections:0.1.20181120195412'
}
```

# Examples

```java
import org.lucares.collections.IntList;

public class Example {
	public static void main(final String[] args) {
		final IntList list = IntList.of(1, 3, 5);
		System.out.println(list + " is sorted: " + list.isSorted());

		list.insert(2, 7);
		System.out.println(list + " is sorted: " + list.isSorted());

		list.sort();
		System.out.println(list + " is sorted: " + list.isSorted());
	}
}

```
Running this program gives the following output:

```
[1, 3, 5] is sorted: true
[1, 3, 7, 5] is sorted: false
[1, 3, 5, 7] is sorted: true
```
