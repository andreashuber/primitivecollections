#!/bin/bash

mkdir -p tmp
cd tmp

if ! test -d "gradle-2.0"; then
	wget https://services.gradle.org/distributions/gradle-2.0-bin.zip
	unzip gradle-2.0-bin.zip
fi


#echo setting environment

export GRADLE_HOME=`pwd`/gradle-2.0
#echo GRADLE_HOME=$GRADLE_HOME

export PATH=$PATH:$GRADLE_HOME/bin
#echo $PATH


cd ..
gradle --daemon $1 $2 $3 $4 $5 $6 $7
